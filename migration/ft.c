#include "qemu/osdep.h"
#include "qemu/units.h"
//#include "sysemu/sysemu.h"
#include "qapi/error.h"
#include "migration.h"
#include "migration/ft.h"
#include "qemu-file-channel.h"
#include "qemu-file.h"
#include "savevm.h"
#include "ram.h"
#include "block.h"
#include "io/channel-buffer.h"
#include "trace.h"
#include "qemu/error-report.h"
#include "monitor/hmp.h"
#include "qemu/thread.h"
#include "block/block.h"
#include "qapi/qmp/qdict.h"

#include "sysemu/runstate.h"
#include "qemu/main-loop.h"

#include "block.h"
#include "qemu/units.h"

//#include "io/channel-file.h"

#include "qapi/error.h"

#ifdef FT_DEBUG_RECORD
static QemuThread ft_record_save_thread;
#endif

#define FT_GOTO_ERROROUT_ON(condition)  if (unlikely(condition)) \
        { ret = (ret == 0)? -ENOSYS : ret;  goto error_out; }
#define FT_UPDATE_MAX_SENT_SIZE(prev_max, size) \
        if (size > prev_max) { prev_max = size; }

#define VMSD_BUF_BASE_SIZE (4 * MiB)
#define BLOCK_BUF_BASE_SIZE (512 * MiB)
// FT_DEFAULT_SOCKET_TIMEOUT is in millisecond(s)
#define FT_DEFAULT_SOCKET_TIMEOUT 250
#define FT_DEFAULT_WATCHDOG_STAGES 3

static QemuMutex sync_shutdown_mutex;

static inline int ft_sleep_monotonic(struct timespec *ts);
static inline struct timespec ft_generate_timespec(int64_t milliseconds);

// -------------------------- start for primary side --------------------------
//

//thread and sync sem for save vmsd (almost device state)
static QemuThread ft_save_vmsd_thread;
static QemuSemaphore ft_save_vmsd_notify;
static QemuSemaphore ft_save_vmsd_done;
static bool ft_save_vmsd_thread_running = false;

//thread and sync sem for block replication
static QemuThread ft_save_block_thread;
static QemuSemaphore ft_save_block_notify;
static QemuSemaphore ft_save_block_done;
static bool ft_save_block_thread_running = false;

static QemuThread ft_load_ram_thread;
static QemuSemaphore ft_load_ram_notify;
static QemuSemaphore ft_load_ram_done;
static bool ft_load_ram_thread_running = false;

static QemuThread ft_load_block_thread;
static QemuSemaphore ft_load_block_notify;
static QemuSemaphore ft_load_block_done;
static bool ft_load_block_thread_running = false;
static bool ft_guest_powering_off = false;

static int64_t ft_checkpoint_delay_ms;

static void ft_checkpoint_loop(MigrationState *);

static int ft_do_checkpoint_transaction_once(MigrationState *,
                                             CheckpointingState *);
static void *ft_save_block_thread_func(void *opaque);
static void *ft_load_ram_thread_func(void *opaque);
static void *ft_load_block_thread_func(void *opaque);
static void *ft_save_vmsd_thread_func(void *opaque);
static void ft_primary_do_failover(void);
static void ft_auto_start_vm_after_failover(void *);

static int64_t ft_autocd_last_skipped_ms = 0;
static QemuThread ft_watchdog_thread;
static void *ft_watchdog_thread_func(void *opaque);

bool migration_in_ft_state(void)
{
    MigrationState *s = migrate_get_current();
    return s->state == MIGRATION_STATUS_FT;
}

void migrate_start_ft(MigrationState *s)
{
    Error *err = NULL;
    qemu_mutex_init(&sync_shutdown_mutex);

    if (!ft_net_init(&err) || !savevm_ft_check_unsupported_device(&err))
    {
        if (err)
        {
            error_report_err(err);
        }
        /* Failure FTState has been set in the previous condition functions */
        migrate_set_state(&s->state, MIGRATION_STATUS_ACTIVE, MIGRATION_STATUS_FAILED);
        error_report("FT failed to start because VM devices has been"
                         " changed during FT initialization");
        qemu_mutex_unlock_iothread();
        ft_primary_do_failover();
        qemu_mutex_lock_iothread();
        ft_set_status(FT_STATUS_VM_CHANGED_DURING_INIT,
         "FT failed to start because VM devices has been changed during FT initialization");
        /* Prevent VM autostart BH from putting addtional status to shared storage */
        ft_disable_shared_export();
        return;
    }
    qemu_mutex_unlock_iothread();
    ft_guest_powering_off = false;
    qemu_sem_init(&s->ft_checkpoint_sem, 0);

    qemu_file_qio_socket_set_nodelay(s->to_dst_file);

    int64_t wdog_stage_intvl = migrate_get_current()->parameters.ft_watchdog_stage_interval;
    if (wdog_stage_intvl > 0)
    {
        qemu_file_qio_socket_set_timeout(s->to_dst_file,
                (wdog_stage_intvl < (INT_MAX / FT_DEFAULT_WATCHDOG_STAGES))?
                  wdog_stage_intvl * FT_DEFAULT_WATCHDOG_STAGES : FT_DEFAULT_SOCKET_TIMEOUT );
    }

    s->ft_checkpoint_delay_timer = timer_new_ms(QEMU_CLOCK_HOST,
                                                ft_checkpoint_notify, s);
    ft_checkpoint_loop(s);
    qemu_mutex_lock_iothread();
}

static void ft_checkpoint_loop(MigrationState *s)
{
    CheckpointingState *cs = checkpointing_get_current();
    QEMUFile *vmsd_file = NULL, *block_file = NULL;
    
    Error *local_err = NULL;
    int64_t current_time = qemu_clock_get_ms(QEMU_CLOCK_HOST);
    int64_t checkpoint_requested_time = 0;
    struct timespec extra_stepping_delay = ft_generate_timespec(1);
    int ret = -ENOTSUP;
    int sem_timeout;
    cs->ram_buffer = cs->vmsd_buffer = cs->block_buffer = NULL;
    cs->ram_file = NULL;
    cs->max_block_buffer_sent = cs->max_ram_buffer_sent = cs->max_vmsd_buffer_sent = 0;
    ft_checkpoint_delay_ms = atomic_read(&s->parameters.ft_checkpoint_delay);

    ft_init_local_file();

    //Malloc of buffer ,if buffer is NULL
    cs->ram_local_buffer = qio_channel_buffer_new(4 * KiB);
    if (cs->ram_local_buffer->data)
    {
        g_free(cs->ram_local_buffer->data);
    }
    cs->ram_local_buffer->data = g_try_malloc(ram_size / 4);
    if (!cs->ram_local_buffer->data)
    {
        error_report("[Checkpointing] OOM during initialization");
        goto error_out;
    }

    s->rp_state.from_dst_file = qemu_file_get_return_path(s->to_dst_file);//設定return path(QF)跟backup做溝通

    if (!s->rp_state.from_dst_file)
    {
        ft_set_status(FT_STATUS_INTERNAL_ERROR, "backup messaging failed");
        error_report("[Checkpointing] Open QEMUFile from_dst_file failed");
        goto error_out;
    }
    ret = ft_message_init(s->to_dst_file, s->rp_state.from_dst_file, cs);
    FT_GOTO_ERROROUT_ON(ret != 0);

    /* pre-allocate 1/4 ram_size of the VM to save dirty pages
     * qio_channel_buffer_new() uses g_new0, which might abort the VM. */
    cs->ram_buffer = qio_channel_buffer_new(4 * KiB);
    if (cs->ram_buffer->data)
    {
        g_free(cs->ram_buffer->data);
    }
    cs->ram_buffer->data = g_try_malloc(ram_size / 4);
    if (!cs->ram_buffer->data)
    {
        error_report("[Checkpointing] OOM during initialization");
        goto error_out;
    }
    
    if (!cs->ram_buffer->data)
    {
        error_report("[Checkpointing] OOM during initialization");
        goto error_out;
    }
    //(orig)set (channel buffer) on (QMEUFile)
    cs->ram_file = qemu_fopen_channel_output(QIO_CHANNEL(cs->ram_buffer));
    cs->vmsd_buffer = qio_channel_buffer_new(VMSD_BUF_BASE_SIZE);
    vmsd_file = qemu_fopen_channel_output(QIO_CHANNEL(cs->vmsd_buffer));

    qemu_sem_init(&ft_save_vmsd_notify, 0);
    qemu_sem_init(&ft_save_vmsd_done, 0);
    cs->vmsd_notify = &ft_save_vmsd_notify;


    qemu_thread_create(&ft_save_vmsd_thread, "ft_save_vmsd",
                       ft_save_vmsd_thread_func, vmsd_file,
                       QEMU_THREAD_JOINABLE);

    ft_save_vmsd_thread_running = true;

    if (ft_block_replication_enabled())
    {
        block_update_block_size((s->parameters.ft_block_granularity * KiB), true);
        cs->block_buffer = qio_channel_buffer_new(BLOCK_BUF_BASE_SIZE);
        block_file = qemu_fopen_channel_output(QIO_CHANNEL(cs->block_buffer)); //這邊把block buffer channel 開在block file(QF)
        qemu_sem_init(&ft_save_block_notify, 0);
        cs->block_notify = &ft_save_block_notify;
        qemu_sem_init(&ft_save_block_done, 0);
        qemu_thread_create(&ft_save_block_thread, "ft_save_block",
                        ft_save_block_thread_func, block_file,
                        QEMU_THREAD_JOINABLE);
        ft_save_block_thread_running = true;
    }
    //malloc block buufer , if it is NULL
    cs->block_local_buffer = qio_channel_buffer_new(BLOCK_BUF_BASE_SIZE);
    cs->vmsd_local_buffer = qio_channel_buffer_new(VMSD_BUF_BASE_SIZE);

    /* will need to wait if FT_DEBUG_RECORD is enabled */
    if (!ft_autopilot_init(s))
    {
        error_report("[Checkpointing] can not initialize autopilot; terminating.");
        goto error_out;
    }

    if (!ft_compression_init(cs->compressor))
    {
        info_report("[Checkpointing] will continue without compression.");
    }

    migrate_set_state(&s->state, MIGRATION_STATUS_ACTIVE, MIGRATION_STATUS_FT);
    cs->active_role = ft_get_active_role();

    ft_export_start_locals_polling();

    char start_ft_str[64] = "";
    sprintf(start_ft_str, "Started FT - Primary Type %s", CheckpointingType_str(cs->type));
    ft_set_status(FT_STATUS_NORMAL, start_ft_str);

#ifdef FT_DIRTY_TRACK
    printf("Checkpoint counter,Dirty pages,Overlap pages,Overlap ratio\n");
#endif

    ft_vm_start();

    timer_mod(s->ft_checkpoint_delay_timer,
              current_time + s->parameters.ft_checkpoint_delay);

    /* Wait for backup finish loading VM states, mallocs and enter FT restore.
     * send message back to make sure watchdog thread creation timing is
     * almost the same on the both hosts. */
    ft_receive_and_check_message(FT_MESSAGE_CHECKPOINT_READY, &local_err);
    FT_GOTO_ERROROUT_ON(local_err);

    ret = ft_send_message(FT_MESSAGE_CHECKPOINT_READY, &local_err);
    FT_GOTO_ERROROUT_ON(local_err || ret != 0);

    current_time = qemu_clock_get_ms(QEMU_CLOCK_HOST);

    qemu_thread_create(&ft_watchdog_thread, "ft_watchdog",
                       ft_watchdog_thread_func, NULL,
                       QEMU_THREAD_JOINABLE);

    while (likely(s->state == MIGRATION_STATUS_FT))
    {
        /* use timed wait to keep main loop and takeover from being blocked;
         * wait twice the delay_ms to ensure the semaphore is properly posted and waited */
        sem_timeout = (ft_checkpoint_delay_ms > (INT_MAX - FT_CHECKPOINT_SEM_TIMEOUT_MS)) ?
                        (int)ft_checkpoint_delay_ms : (INT_MAX - FT_CHECKPOINT_SEM_TIMEOUT_MS);
        ret = qemu_sem_timedwait(&s->ft_checkpoint_sem,
                                 sem_timeout + FT_CHECKPOINT_SEM_TIMEOUT_MS);
        if (ret != 0)
        {
            ft_set_status(FT_STATUS_INTERNAL_ERROR, "internal semaphore timed-out");
            error_report("[Checkpointing] %s: internal sem timeout (%d ms) reached, forcing "
                         "takeover", __func__, sem_timeout + FT_CHECKPOINT_SEM_TIMEOUT_MS);
            break;
        }

        checkpoint_requested_time = qemu_clock_get_ms(QEMU_CLOCK_HOST);

        /* update checkpoint delay from HMP or autopilot */
        ft_checkpoint_delay_ms = atomic_read(&s->parameters.ft_checkpoint_delay);

        /* delay checkpoint if tx queue is empty; using sleep function instead of timer_mod()
         * because it also works outside the main_loop(), where the timer will not works. */
        while (s->parameters.ft_auto_cd && ft_tx_queue_is_empty() &&
               ft_autocd_last_skipped_ms < cs->max_allowed_delay)
        {
            ret = ft_sleep_monotonic(&extra_stepping_delay);
            if (ret != 0)
            {
                error_report("%s: ft_sleep_monotonic failed %d:%s, ignoring",
                             __func__, ret, strerror(ret));
            }
            current_time = qemu_clock_get_ms(QEMU_CLOCK_HOST);
            ft_autocd_last_skipped_ms = current_time - checkpoint_requested_time;
        }
        ft_autopilot_record_int64(FT_RECORD_EXTRA_DELAY, ft_autocd_last_skipped_ms);

        ret = ft_do_checkpoint_transaction_once(s, cs);
        if (unlikely(ret != 0))
        {
            error_report("%s: exiting because %s", __func__, strerror(-ret));
            break;
        }

        cs->last_checkpointed_host_clock = qemu_clock_get_ms(QEMU_CLOCK_HOST);
        ft_autocd_last_skipped_ms = 0;
    }

error_out:
    object_unref(OBJECT(cs->ram_buffer));
    object_unref(OBJECT(cs->vmsd_buffer));
    object_unref(OBJECT(cs->block_buffer));
    
    //(new)
    object_unref(OBJECT(cs->ram_local_buffer));
    object_unref(OBJECT(cs->vmsd_local_buffer));
    object_unref(OBJECT(cs->block_local_buffer));
    /* Throw the unreported error message after exited from loop */
    if (local_err)
    {
        error_report_err(local_err);
    }

    if (cs->ram_file)
    {
        qemu_fclose(cs->ram_file);
    }
    if (vmsd_file)
    {
        qemu_fclose(vmsd_file);
    }
    if (block_file)
    {
        qemu_fclose(block_file);
    }

    //(new)
    if (cs->ram_local_file)
    {
        qemu_fclose(cs->ram_local_file);
    }
    if (cs->vmsd_local_file)
    {
        qemu_fclose(cs->vmsd_local_file);
    }
    if (cs->block_local_file)
    {
        qemu_fclose(cs->block_local_file);
    }
    /* set powering_off to unblock main thread,
     * sometimes guest-powered-off FT message sent might fail .*/
    atomic_set(&ft_guest_powering_off, false);
    timer_del(s->ft_checkpoint_delay_timer);
    qemu_thread_join(&ft_watchdog_thread);

    /* Hope this not to be too long to wait here */
    // qemu_sem_wait(&s->ft_exit_sem);
    // qemu_sem_destroy(&s->ft_exit_sem);
    /*
     * Must be called after failover BH is completed,
     * Or the failover BH may shutdown the wrong fd that
     * re-used by other threads after we release here.
     */
    // if (s->rp_state.from_dst_file) {
    //     qemu_fclose(s->rp_state.from_dst_file);
    // }
    ft_primary_do_failover();

    qemu_sem_destroy(&s->ft_checkpoint_sem);
    qemu_sem_destroy(&ft_save_vmsd_done);
    qemu_sem_destroy(&ft_save_vmsd_notify);
    if (ft_block_replication_enabled())
    {
        qemu_sem_destroy(&ft_save_block_notify);
        qemu_sem_destroy(&ft_save_block_done);
    }
}

int ft_do_checkpoint_transaction_once(MigrationState * migs,
                                      CheckpointingState *cs)
{

    QIOChannelBuffer *ram_buffer = cs->ram_buffer;
    QIOChannelBuffer *vmsd_buffer = cs->vmsd_buffer;
    QIOChannelBuffer *block_buffer = cs->block_buffer;
    QEMUFile *ram_file = cs->ram_file;

    //(new) local
    QIOChannelBuffer *ram_local_buffer = cs->ram_local_buffer;
    QIOChannelBuffer *block_local_buffer = cs->block_local_buffer;
    QIOChannelBuffer *vmsd_local_buffer = cs->vmsd_local_buffer;
    //QEMUFile *ram_local_file = cs->ram_local_file;

    Error *local_err = NULL;
    /* Initialize ret to -ENOTSUP to make sure unknown errors always stop
     * at ft_checkpoint_loop() */
    int ret = -ENOTSUP;

    ft_autopilot_record_host_clock(FT_RECORD_EPOCH_BEGIN);
    ft_message_lock();

    // send control header (checkpoint, guest_poweroff, ...)
    //---------------------------------------------------------------------
    FTMessage ctrl_hdr = FT_MESSAGE_CHECKPOINT_REQUEST;

    if (unlikely(atomic_read(&ft_guest_powering_off) == true))
    {
        ctrl_hdr = FT_MESSAGE_GUEST_POWERED_OFF;
    }

    ret = ft_send_message(ctrl_hdr, &local_err);

    FT_GOTO_ERROROUT_ON(local_err || ret != 0);
    /* no more checkpoints should be made after guest powered off */
    if (atomic_read(&ft_guest_powering_off) == true)
    {
        ret = -EHOSTDOWN;
        goto error_out;
    }
    ft_message_unlock();

    // reset channel-buffer directly
    //---------------------------------------------------------------------
    qio_channel_io_seek(QIO_CHANNEL(ram_buffer), 0, 0, NULL);
    ram_buffer->usage = 0;
    qio_channel_io_seek(QIO_CHANNEL(vmsd_buffer), 0, 0, NULL);
    vmsd_buffer->usage = 0;
    if (ft_block_replication_enabled())
    {
        qio_channel_io_seek(QIO_CHANNEL(block_buffer), 0, 0, NULL);
        block_buffer->usage = 0;
    }

    qio_channel_io_seek(QIO_CHANNEL(cs->ram_local_buffer), 0, 0, NULL);
    ram_local_buffer->usage = 0;
    qio_channel_io_seek(QIO_CHANNEL(cs->vmsd_local_buffer), 0, 0, NULL);
    vmsd_local_buffer->usage = 0;
    if (ft_block_replication_enabled())
    {
        qio_channel_io_seek(QIO_CHANNEL(block_local_buffer), 0, 0, NULL);
        block_local_buffer->usage = 0;
    }
    // stop VM to do checkpoint
    //---------------------------------------------------------------------
    ft_debug_record_host_clock(FT_RECORD_VM_STOP_BEGIN);
    ft_vm_stop_still_lock(RUN_STATE_FINISH_MIGRATE, true);
    ft_debug_record_host_clock(FT_RECORD_VM_STOP_END);

    // save VM to QIOChannel buffer (QIOChannelBuffer)
    //---------------------------------------------------------------------
    ft_debug_record_host_clock(FT_RECORD_RAM_SAVE_BEGIN);
    ret = ram_save_complete_ft(ram_file, (void *)cs, (void *)ram_buffer);
    FT_GOTO_ERROROUT_ON(ret != 0);
    ft_debug_record_host_clock(FT_RECORD_RAM_SAVE_END);
    //(mulfi CP)ret = ram_save_complete_file(ram_local_file, (void *)cs, (void *)ram_local_buffer);
    ret = ram_save_complete_file(cs->ram_local_file, (void *)cs, (void *)cs->ram_local_buffer);
    FT_GOTO_ERROROUT_ON(ret != 0);
    qemu_sem_wait(&ft_save_vmsd_done);
    if (ft_block_replication_enabled())
    {
        qemu_sem_wait(&ft_save_block_done);
    }
    qemu_mutex_unlock_iothread();

    ssize_t tx_queue_size = ft_net_post_savevm();

    // start VM
    //---------------------------------------------------------------------
    ft_debug_record_host_clock(FT_RECORD_VM_START_BEGIN);
    ft_vm_start();
    ft_debug_record_host_clock(FT_RECORD_VM_START_END);

    ft_autopilot_record_host_clock(FT_RECORD_SEND_BEGIN);
    ft_message_lock();

    // send block data
    //---------------------------------------------------------------------
    if (ft_block_replication_enabled())
    {
        /* Send Block device data buffer to Backup.
         * We need the size of the Block device data in Backup side,
         * With which we can decide how much data should be read.
         * Wait for backup malloc if the buffer is bigger than ever. */
        ret = ft_send_size_and_wait_malloc(FT_MESSAGE_BLOCK_FULL_SIZE,
                    block_buffer->usage, cs->max_block_buffer_sent, &local_err);
        FT_GOTO_ERROROUT_ON(local_err || ret != 0);
        FT_UPDATE_MAX_SENT_SIZE(cs->max_block_buffer_sent, block_buffer->usage);

        ret = ft_send_buffer_adapt(block_buffer->data, block_buffer->usage,
                                   FT_RECORD_SEND_TYPE_BLOCK);
        FT_GOTO_ERROROUT_ON(ret != 0);
        ft_autopilot_record_uint64(FT_RECORD_BLOCK_SIZE, block_buffer->usage);

                ret = ft_send_size_and_wait_malloc(FT_MESSAGE_BLOCK_FILE_SIZE,
                            cs->block_local_buffer->usage, cs->max_block_local_buffer_sent, &local_err);
                FT_GOTO_ERROROUT_ON(local_err || ret != 0);
                FT_UPDATE_MAX_SENT_SIZE(cs->max_block_local_buffer_sent, cs->block_local_buffer->usage);

                ret = ft_send_buffer_adapt(cs->block_local_buffer->data, cs->block_local_buffer->usage,
                                        FT_RECORD_SEND_TYPE_BLOCK);
                FT_GOTO_ERROROUT_ON(ret != 0);
    }

    // send ram data
    // Note: We need the size of the VM ram data in Backup side,
    //       With which we can decide how much data should be read.
    //---------------------------------------------------------------------
    ft_autopilot_record_uint64(FT_RECORD_RAM_SIZE, ram_buffer->usage);
    ret = ft_send_size_and_wait_malloc(FT_MESSAGE_RAM_FULL_SIZE,
                ram_buffer->usage, cs->max_ram_buffer_sent, &local_err);
    FT_GOTO_ERROROUT_ON(local_err || ret != 0);
    FT_UPDATE_MAX_SENT_SIZE(cs->max_ram_buffer_sent, ram_buffer->usage);

    ret = ft_send_buffer_adapt(ram_buffer->data, ram_buffer->usage,
                               FT_RECORD_SEND_TYPE_RAM);
    FT_GOTO_ERROROUT_ON(ret != 0);
    info_report("send ram  size=%ld ",ram_buffer->usage);
    // send ram file
        ret = ft_send_size_and_wait_malloc(FT_MESSAGE_RAM_FILE_SIZE,
                cs->ram_local_buffer->usage , cs->max_ram_local_buffer_sent, &local_err);
        info_report("send ram file size=%ld ",cs->ram_local_buffer->usage);
        FT_GOTO_ERROROUT_ON(local_err || ret != 0);
        FT_UPDATE_MAX_SENT_SIZE(cs->max_ram_local_buffer_sent, ram_local_buffer->usage);
        // 已經在save對block_local_buffer->data做初始化了
        // 改tpye怕會出錯
        ret = ft_send_buffer_adapt(cs->ram_local_buffer->data, cs->ram_local_buffer->usage,
                                   FT_RECORD_SEND_TYPE_RAM);
        FT_GOTO_ERROROUT_ON(ret != 0);
    //

    // send other VM device data
    //---------------------------------------------------------------------
    ft_autopilot_record_uint64(FT_RECORD_VMSD_SIZE, vmsd_buffer->usage);
    ret = ft_send_message_and_value
                (FT_MESSAGE_VMSD_FULL_SIZE, vmsd_buffer->usage, &local_err);
    FT_GOTO_ERROROUT_ON(local_err || ret != 0);

    ret = ft_send_buffer_adapt(vmsd_buffer->data, vmsd_buffer->usage,
                               FT_RECORD_SEND_TYPE_VMSD);
    FT_GOTO_ERROROUT_ON(ret != 0);
    /*
        //send vmsd file
        ret = ft_send_size_and_wait_malloc(FT_MESSAGE_VMSD_FILE_SIZE,
                    cs->vmsd_local_buffer->usage, cs->max_vmsd_buffer_sent, &local_err);
        FT_GOTO_ERROROUT_ON(local_err || ret != 0);
        FT_UPDATE_MAX_SENT_SIZE(cs->max_vmsd_buffer_sent, cs->vmsd_local_buffer->usage);

        ret = ft_send_buffer_adapt(cs->vmsd_local_buffer->data, cs->vmsd_local_buffer->usage,
                                   FT_RECORD_SEND_TYPE_VMSD);
        FT_GOTO_ERROROUT_ON(ret != 0);
        */
    // wait for backup acked and loaded block, RAM and all other vmsds;
    // then flush the buffered TX of VM.
    //---------------------------------------------------------------------
    ret = ft_receive_and_check_message(FT_MESSAGE_VMSTATE_RECEIVED, &local_err);
    FT_GOTO_ERROROUT_ON(local_err || ret != 0);
    ft_autopilot_record_host_clock(FT_RECORD_SEND_END);

    ft_net_post_sendvm();
    ft_net_flush_tx();
    ft_debug_record_host_clock(FT_RECORD_LOADED_ACK_BEGIN);
    ret = ft_receive_and_check_message(FT_MESSAGE_VMSTATE_LOADED, &local_err);
    FT_GOTO_ERROROUT_ON(local_err || ret != 0);
    ft_debug_record_host_clock(FT_RECORD_LOADED_ACK_END);

    ft_message_unlock();
    cs->last_checkpointed_host_clock = qemu_clock_get_ms(QEMU_CLOCK_HOST);
    ft_autopilot_record_host_clock(FT_RECORD_EPOCH_END);
    ft_autopilot_record_uint64(FT_RECORD_TX_QUEUE_SIZE, tx_queue_size);
    ft_autopilot_record_int64(FT_RECORD_CHECKPOINT_DELAY,
                          migs->parameters.ft_checkpoint_delay);

    // decide and set: when should the next checkpointing start
    //---------------------------------------------------------------------
    if (migs->parameters.ft_auto_cd)
    {
        ft_autopilot_update_checkpoint_delay(migs);
    }

    /* Mod timer to the time of next checkpoint. see include/qemu/timer.h 
    timer_mod_anticipate(migs->ft_checkpoint_delay_timer,
        ft_autopilot_record_get_uint64(FT_RECORD_EPOCH_BEGIN) + ft_checkpoint_delay_ms);*/
    timer_mod_anticipate(migs->ft_checkpoint_delay_timer,
        ft_autopilot_record_get_uint64(FT_RECORD_EPOCH_BEGIN) + 5000);
    if (migs->parameters.ft_show_msg)
    {
        ft_autopilot_print_stat();
    }
    ft_autopilot_record_epoch_end();

    ret = 0;

error_out:
    if (unlikely(local_err || ret != 0))
    {
        ft_message_unlock();
    }
    if (unlikely(local_err))
    {
        error_report_err(local_err);
    }
    if (ret != 0)
    {
        error_report("%s: ret=%d - %s", __func__, ret, strerror(-ret));
    }
    return ret;
}

static void *ft_save_vmsd_thread_func(void *opaque)
{
    MigrationState *s = migrate_get_current();
    CheckpointingState *cs = checkpointing_get_current();
    QEMUFile *vmsd_file = opaque;

    qemu_sem_wait(&ft_save_vmsd_notify);

    while (s->state == MIGRATION_STATUS_FT)
    {
        ft_debug_record_host_clock(FT_RECORD_VMSD_SAVE_BEGIN);
        qemu_savevm_state_vmsd_only(vmsd_file);
        if (qemu_file_get_error(vmsd_file) < 0)
        {
            ft_set_status(FT_STATUS_INTERNAL_ERROR, "Save vmsd to local buffer failed");
            error_report("Save vmsd to local buffer failed");
            break;
        }
        ft_debug_record_host_clock(FT_RECORD_VMSD_SAVE_END);
        //channel file
        qemu_savevm_state_vmsd_only_file(cs->vmsd_local_file);
        if (qemu_file_get_error(cs->block_local_file) < 0)
        {
            ft_set_status(FT_STATUS_INTERNAL_ERROR, "Save vmsd file failed");
            error_report("Save vmsd file failed");
            break;
        }
        qemu_sem_post(&ft_save_vmsd_done);
        qemu_sem_wait(&ft_save_vmsd_notify);
    }

    qemu_sem_post(&ft_save_vmsd_done);

    return NULL;
}

static void *ft_save_block_thread_func(void *opaque)
{
    if (!ft_block_replication_enabled())
    {
        return NULL;
    }
    MigrationState *s = migrate_get_current();
    QEMUFile *block_file = opaque;
    CheckpointingState *cs = checkpointing_get_current();

    int ret = 0;
    qemu_sem_wait(&ft_save_block_notify);

    while (s->state == MIGRATION_STATUS_FT)
    {
        ft_debug_record_host_clock(FT_RECORD_BLOCK_SAVE_BEGIN);
        ret = block_save_complete_ft(block_file);
        if (qemu_file_get_error(block_file) < 0 || ret < 0)
        {
            ft_set_status(FT_STATUS_INTERNAL_ERROR, "Save Block to local buffer failed");
            error_report("Save Block to local buffer failed");
            break;
        }
        ft_debug_record_host_clock(FT_RECORD_BLOCK_SAVE_END);
        //channel file
        ret = block_save_complete_file(cs->block_local_file);
        if (qemu_file_get_error(cs->block_local_file) < 0 || ret < 0)
        {
            ft_set_status(FT_STATUS_INTERNAL_ERROR, "Save Block file failed");
            error_report("Save Block file failed");
            break;
        }
        qemu_sem_post(&ft_save_block_done);
        qemu_sem_wait(&ft_save_block_notify);
    }

    qemu_sem_post(&ft_save_block_done);

    return NULL;
}

void ft_checkpoint_notify(void *opaque)
{
    MigrationState *s = opaque;
    if (migration_in_ft_state())
    {
        qemu_sem_post(&s->ft_checkpoint_sem);
    }
}

static void ft_primary_do_failover(void)
{
    MigrationState *s = migrate_get_current();
    CheckpointingState *cs = checkpointing_get_current();

    ft_vm_stop(RUN_STATE_PAUSED);
    ft_set_status(FT_STATUS_DURING_TAKEOVER,
                  "VM will be paused until takeover succeed.");
    migrate_set_state(&s->state, MIGRATION_STATUS_FT,
                      MIGRATION_STATUS_CANCELLED);
    cs->active_role = ft_get_active_role();

    if (ft_save_vmsd_thread_running)
    {
        qemu_sem_post(&ft_save_vmsd_notify);
        qemu_thread_join(&ft_save_vmsd_thread);
        ft_save_vmsd_thread_running = false;
    }

    if (ft_block_replication_enabled() && ft_save_block_thread_running)
    {
        qemu_sem_post(&ft_save_block_notify);
        qemu_thread_join(&ft_save_block_thread);
        ft_save_block_thread_running = false;
        /* reset the BLOCK_SIZE to 1MB */
        block_update_block_size(1 * MiB, false);
    }

    ft_sync_shutdown("socket error");
    /* ensure takeover process validity using FT Export */
    if (!ft_export_is_disabled() && ft_export_exists_takeover_lock())
    {
        error_report("Backup has already tookover the VM. "
                "Now shutting down this QEMU instance.\n");
        exit(EXIT_FAILURE);
    }
    if (!ft_export_is_disabled() && !ft_export_put_takeover_lock())
    {
        ft_set_status(FT_STATUS_STORAGE_DISCONNECTED,
                "Can not lock storage for takeover. Manual resume VM is required.");
        ft_export_append_alert();
        //FIXME check the timing of qemu_tap_queue flushes, or delay to vm_start()
        // ft_net_failover_flush_tx();
        ft_export_cleanup_local();
        ft_export_cleanup_shared();
        return;
    }

    s->ft_start_vm_bh = qemu_bh_new(ft_auto_start_vm_after_failover, s);

    qemu_mutex_destroy(&sync_shutdown_mutex);
    ft_compression_cleanup(true);
    //printf("ft_primary_do_failover\n");
}

static void ft_auto_start_vm_after_failover(void *opaque)
{
    MigrationState *s = opaque;
    qemu_bh_delete(s->ft_start_vm_bh);
    s->ft_start_vm_bh = NULL;
    ft_net_failover_flush_tx();
    if (!runstate_is_running())
    {
        vm_start();
        //printf("started VM\n\n");
    }
    ft_set_status(FT_STATUS_PRIMARY_TAKEOVER, "Primary Takeover");
    ft_export_cleanup_shared();
    ft_export_cleanup_local();
}

void hmp_ft_record_start(Monitor *mon, const QDict *qdict)
{
#ifdef FT_DEBUG_RECORD
    ft_autopilot_record_start();
#else
    error_report("Configure with `./configure "
                 "--extra-cflags=\"-DFT_DEBUG_RECORD\"` , then build QEMU "
                 "to enable full record and save debugging feature.");
#endif
}

void hmp_ft_record_stop(Monitor *mon, const QDict *qdict)
{
#ifdef FT_DEBUG_RECORD
    static char *record_csv = NULL;
    record_csv = strdup(qdict_get_str(qdict, "record-csv"));
    info_report("stopping record and save to %s", record_csv);
    qemu_thread_create(&ft_record_save_thread, "ft_record_save",
                       ft_autopilot_record_stop_and_save_csv_thread,
                       (void *)record_csv, QEMU_THREAD_DETACHED);
#else
    error_report("Configure with `./configure "
                 "--extra-cflags=\"-DFT_DEBUG_RECORD\"` , then build QEMU "
                 "to enable full record and save debugging feature.");
#endif
}

/* notify checkpoint_loop to send powered_off message,
 * and wait until the message being sent by checkpoint_loop */
void ft_notify_guest_powered_off_pre(void)
{
    if (!migration_in_ft_state())
    {
        return;
    }
    atomic_set(&ft_guest_powering_off, true);
    ft_checkpoint_notify(migrate_get_current());
}

void ft_notify_guest_powered_off_post(void)
{
    if (!migration_in_ft_state())
    {
        return;
    }
    qemu_mutex_unlock_iothread();
    /* block main thread to prevent QEMU exit before message sent */
    int i = 0;
    while (atomic_read(&ft_guest_powering_off) == true)
    {
        g_usleep(100000); // 100 msec
        /* sem_post to make sure the checkpoint_loop keeps going */
        if (i < 5)
        {
            ft_checkpoint_notify(migrate_get_current());
            ++i;
        }
    }
    qemu_mutex_lock_iothread();
}

void hmp_checkpoint_now(Monitor *mon, const QDict *qdict)
{
    info_report("[Checkpointing] checkpoint triggered from HMP");
    ft_checkpoint_notify(migrate_get_current());
}

//
// --------------------------  end for primary side  --------------------------

// --------------------------  start for backup side --------------------------
//
static inline int ft_handle_message_request(FTMessage msg);
static void ft_backup_do_failover(void *opaque);

bool migration_incoming_in_ft_state(void)
{
    MigrationIncomingState *s = migration_incoming_get_current();
    return s->state == MIGRATION_STATUS_FT;
}

void *migrate_ft_incoming_thread(void *opaque)
{
    MigrationIncomingState *migs = opaque;
    CheckpointingState *cs = checkpointing_get_current();
    QEMUFile *vmsd_file = NULL, *block_file = NULL;
    uint8_t *vmsd_recv_buffer = NULL, *vmsd_tmp_buffer = NULL;
    uint64_t value;
    int ret = -ENOTSUP;
    FTMessage msg;
    bool vm_reset_needed = false;
    Error *local_err = NULL;
    cs->ram_buffer = cs->vmsd_buffer = cs->block_buffer = NULL;
    cs->ram_file = NULL;
    cs->max_block_buffer_sent = cs->max_ram_buffer_sent = 0;
    ft_guest_powering_off = false;

    /* For demo: Reset the console of backup VM to uninitialized */
    qemu_mutex_lock_iothread();
    qemu_system_reset(SHUTDOWN_CAUSE_NONE);
    qemu_mutex_unlock_iothread();

    qemu_mutex_init(&sync_shutdown_mutex);
    qemu_sem_init(&migs->ft_incoming_sem, 0);

    migrate_set_state(&migs->state, MIGRATION_STATUS_ACTIVE,
                      MIGRATION_STATUS_FT);
    cs->active_role = ft_get_active_role();

    migs->to_src_file = qemu_file_get_return_path(migs->from_src_file);
    if (!migs->to_src_file)
    {
        ft_set_status(FT_STATUS_INTERNAL_ERROR, "FT incoming thread: Open QEMUFile to_src_file failed");
        error_report("FT incoming thread: Open QEMUFile to_src_file failed");
        goto error_out;
    }
    ret = ft_message_init(migs->to_src_file, migs->from_src_file, cs);
    FT_GOTO_ERROROUT_ON(ret != 0);

    /*
     * Note: the communication between Primary side and Backup side
     * should be sequential, we set the fd to unblocked in migration incoming
     * coroutine, and here we are in the FT incoming thread, so it is ok to
     * set the fd back to blocked.
     */
    qemu_file_set_blocking(migs->from_src_file, true);

    qemu_file_qio_socket_set_nodelay(migs->to_src_file);

    int64_t wdog_stage_intvl = migrate_get_current()->parameters.ft_watchdog_stage_interval;
    if (wdog_stage_intvl > 0)
    {
        qemu_file_qio_socket_set_timeout(migs->to_src_file,
                (wdog_stage_intvl < (INT_MAX / FT_DEFAULT_WATCHDOG_STAGES))?
                  wdog_stage_intvl * FT_DEFAULT_WATCHDOG_STAGES : FT_DEFAULT_SOCKET_TIMEOUT );
    }
    //配置backup buffer大小
    /* buffer_new->g_new0 directly on bakcup, since the VM is not available here */
    cs->ram_buffer = qio_channel_buffer_new(ram_size / 4);
    cs->ram_file = qemu_fopen_channel_input(QIO_CHANNEL(cs->ram_buffer));
    cs->vmsd_buffer = qio_channel_buffer_new(VMSD_BUF_BASE_SIZE);
    vmsd_file = qemu_fopen_channel_input(QIO_CHANNEL(cs->vmsd_buffer));
    vmsd_recv_buffer = g_malloc(VMSD_BUF_BASE_SIZE);

    QIOChannelBuffer *ram_buffer = cs->ram_buffer;//到這邊只配置大小
    QIOChannelBuffer *vmsd_buffer = cs->vmsd_buffer;
    QEMUFile *ram_file = cs->ram_file;

    if (ft_block_replication_enabled())
    {
        block_update_block_size(
            (migrate_get_current()->parameters.ft_block_granularity * KiB) ,false);
        cs->block_buffer = qio_channel_buffer_new(BLOCK_BUF_BASE_SIZE);
        block_file = qemu_fopen_channel_input(QIO_CHANNEL(cs->block_buffer));
        qemu_sem_init(&ft_load_ram_notify, 0);
        qemu_sem_init(&ft_load_ram_done, 0);
        qemu_sem_init(&ft_load_block_notify, 0);
        qemu_sem_init(&ft_load_block_done, 0);
        //load
        qemu_thread_create(&ft_load_ram_thread, "ft_load_ram",
                           ft_load_ram_thread_func, ram_file,
                           QEMU_THREAD_JOINABLE);
        ft_load_ram_thread_running = true;

        qemu_thread_create(&ft_load_block_thread, "ft_load_block",
                           ft_load_block_thread_func, block_file,
                           QEMU_THREAD_JOINABLE);
        ft_load_block_thread_running = true;
    }
    QIOChannelBuffer *block_buffer = cs->block_buffer;

    //object_unref(OBJECT(ram_buffer));

    if (!ft_compression_init(cs->compressor))
    {
        error_report("[Checkpointing] can not setup equivalent decompression"
            "resources as primary on backup side, terminating.");
        goto error_out;
    }

    /* Wait for primary finish mallocs and enter FT checkpointing
     * make sure watchdog thread creation timing is
     * almost the same on the both hosts. */
    ft_message_lock();
    ret = ft_send_message(FT_MESSAGE_CHECKPOINT_READY, &local_err);
    FT_GOTO_ERROROUT_ON(local_err || ret != 0);

    ret = ft_receive_and_check_message(FT_MESSAGE_CHECKPOINT_READY, &local_err);
    FT_GOTO_ERROROUT_ON(local_err || ret != 0);

    qemu_thread_create(&ft_watchdog_thread, "ft_watchdog",
                       ft_watchdog_thread_func, NULL,
                       QEMU_THREAD_JOINABLE);
    ft_export_start_locals_polling();

    ft_set_status(FT_STATUS_NORMAL, "Started FT - Backup");
    while (migs->state == MIGRATION_STATUS_FT)
    {
        // receive control header (checkpoint, guest_poweroff, ...)
        //---------------------------------------------------------------------
        ret = ft_receive_message(&msg, &local_err);
        FT_GOTO_ERROROUT_ON(local_err || ret != 0);
        ret = ft_handle_message_request(msg);
        if (ret == -EINVAL)
        {
            error_report("%s: invalid request received, stopping FT.", __func__);
            exit(EXIT_FAILURE);
        }
        FT_GOTO_ERROROUT_ON(ret != 0);

        // receive block data
        //---------------------------------------------------------------------
        if (ft_block_replication_enabled())
        {
            ret = ft_receive_size_and_malloc(&value, FT_MESSAGE_BLOCK_FULL_SIZE,
                            cs->max_block_buffer_sent, block_buffer, &local_err);
            FT_GOTO_ERROROUT_ON(local_err || ret != 0);
            FT_UPDATE_MAX_SENT_SIZE(cs->max_block_buffer_sent, value);

            ret = ft_receive_buffer_adapt(block_buffer->data, value);
            FT_GOTO_ERROROUT_ON(ret != 0);
            block_buffer->usage = value;
        }

        // receive ram data
        //---------------------------------------------------------------------
        ret = ft_receive_size_and_malloc(&value, FT_MESSAGE_RAM_FULL_SIZE,
                        cs->max_ram_buffer_sent, ram_buffer, &local_err);
        FT_GOTO_ERROROUT_ON(local_err || ret != 0);
        FT_UPDATE_MAX_SENT_SIZE(cs->max_ram_buffer_sent, value);

        ret = ft_receive_buffer_adapt(ram_buffer->data, value);
        FT_GOTO_ERROROUT_ON(ret != 0);
        ram_buffer->usage = value; //在bcakup更新上次的ram size

        // receive device data
        // Note: need two buffer save last two epoch device state,
        //       guarantee have last device state when failover.
        //---------------------------------------------------------------------
        ret = ft_receive_message_and_value
                    (FT_MESSAGE_VMSD_FULL_SIZE, &value, &local_err);
        FT_GOTO_ERROROUT_ON(local_err || ret != 0);
        if (value > vmsd_buffer->capacity)
        {
            g_free(vmsd_recv_buffer);
            vmsd_recv_buffer = g_try_malloc(value);
            FT_GOTO_ERROROUT_ON(!vmsd_recv_buffer);
        }

        ret = ft_receive_buffer_adapt(vmsd_recv_buffer, value);
        FT_GOTO_ERROROUT_ON(ret != 0);

        // send ack to Primary, and start reverse keepalive
        //---------------------------------------------------------------------
        ret = ft_send_message(FT_MESSAGE_VMSTATE_RECEIVED, &local_err);
        FT_GOTO_ERROROUT_ON(local_err || ret != 0);
        ft_reverse_keepalive_start();

        // prepare device data : make sure a valid vmsd_buffer is always
        // available even re-allocation failed.
        //---------------------------------------------------------------------
        if (value > vmsd_buffer->capacity)
        {
            vmsd_tmp_buffer = g_try_malloc(value);
            if (!vmsd_tmp_buffer)
            {
                goto error_out;
            }
            g_free(vmsd_buffer->data);
            vmsd_buffer->data = vmsd_tmp_buffer;
            vmsd_buffer->capacity = value;
        }
        vmsd_buffer->usage = value;
        memcpy(vmsd_buffer->data, vmsd_recv_buffer, vmsd_buffer->usage);

        // reset channel
        //---------------------------------------------------------------------
        if (ft_block_replication_enabled())
        {
            qio_channel_io_seek(QIO_CHANNEL(block_buffer), 0, 0, NULL);
        }
        qio_channel_io_seek(QIO_CHANNEL(ram_buffer), 0, 0, NULL);

        qemu_mutex_lock_iothread();

        // load block and RAM data, stop reverse keep-alive and notify Primary
        //---------------------------------------------------------------------
        if (ft_block_replication_enabled())
        {
            qemu_sem_post(&ft_load_ram_notify);
            qemu_sem_wait(&ft_load_ram_done);

            /* VM need to be reset only if block/ram not correctly loaded.
             * In the case of messaging failed, just load the last state. */
            vm_reset_needed = (cs->status == FT_STATUS_INTERNAL_ERROR);

        }
        else if (ram_load_ft(ram_file) < 0)
        {
            error_report("FT: load ram data failed");
            vm_reset_needed = true;
            qemu_mutex_unlock_iothread();
            goto error_out;
        }
        qemu_mutex_unlock_iothread();

        if (ft_reverse_keepalive_finish() != 0 ||
            cs->status == FT_STATUS_INTERNAL_ERROR)
        {
            qemu_mutex_unlock_iothread();
            goto error_out;
        }

        ret = ft_send_message(FT_MESSAGE_VMSTATE_LOADED, &local_err);
        FT_GOTO_ERROROUT_ON(local_err || ret != 0);

        cs->last_checkpointed_host_clock = qemu_clock_get_ms(QEMU_CLOCK_HOST);
    }

error_out:
    object_unref(OBJECT(cs->ram_buffer));
    object_unref(OBJECT(cs->block_buffer));

    //(new)
    object_unref(OBJECT(cs->ram_local_buffer));
    object_unref(OBJECT(cs->block_local_buffer));

    /* Throw the unreported error message after exited from loop */
    if (local_err)
    {
        error_report_err(local_err);
    }

    switch (ret)
    {
    case 0:
        break;
    case -EINVAL:
        error_report("%s: message from primary host malformed or tampered,"
                     "exiting. ", __func__);
        exit(EXIT_FAILURE);
    case -ENOMEM:
        error_report("%s: RAM of backup host has been exhausted, exiting.",
                     __func__);
        exit(EXIT_FAILURE);
    default:
        error_report("%s: ret=%d - %s", __func__, ret, strerror(-ret));
    }

    if (block_file)
    {
        qemu_fclose(block_file);
    }
    if (cs->ram_file)
    {
        qemu_fclose(cs->ram_file);
    }

    /* reset VM, and load vmstate if possible.  When QEMU failed to load
     * the vmsd, the VM should have be rebooted by QEMU, forgive it. */
    qemu_mutex_lock_iothread();
    if (vm_reset_needed)
    {
        error_report("[Checkpointing] VM reset required due to "
                     "incomplete VM state.");
        qemu_system_reset(SHUTDOWN_CAUSE_NONE);
    }
    if (vmsd_file)
    {
        if (!vm_reset_needed && cs->vmsd_buffer->usage > 0)
        {
            qemu_system_reset(SHUTDOWN_CAUSE_NONE);
            qemu_loadvm_state_vmsd_only(vmsd_file); //vmsd只在這邊做load
        }
        qemu_fclose(vmsd_file);
    }
    qemu_mutex_unlock_iothread();

    ft_message_unlock();
    qemu_thread_join(&ft_watchdog_thread);

    object_unref(OBJECT(cs->vmsd_buffer));
    
    //(new)
    object_unref(OBJECT(cs->vmsd_local_buffer));

    /* Hope this not to be too long to loop here */
    //qemu_sem_wait(&migs->ft_incoming_sem);
    //qemu_sem_destroy(&migs->ft_incoming_sem);
    /* Must be called after failover BH is completed */
    if (migs->to_src_file)
    {
        qemu_fclose(migs->to_src_file);
        migs->to_src_file = NULL;
    }
    ft_vmstate_clear_info();

    migs->ft_failover_bh = qemu_bh_new(ft_backup_do_failover, migs);
    qemu_bh_schedule(migs->ft_failover_bh);

    return NULL;
}

static void *ft_load_ram_thread_func(void *opaque)
{
    MigrationIncomingState *s = migration_incoming_get_current();
    CheckpointingState *cs = checkpointing_get_current();

    QEMUFile *ram_file = opaque;

    qemu_sem_wait(&ft_load_ram_notify);

    while (s->state == MIGRATION_STATUS_FT)
    {
        qemu_sem_post(&ft_load_block_notify);
        if (ram_load_ft(ram_file) < 0)
        {
            ft_set_status(FT_STATUS_INTERNAL_ERROR, "Load ram data failed");
            error_report("FT: load ram data failed");
            break;
        }
        if (ram_load_ft(cs->ram_local_file) < 0)
        {
            ft_set_status(FT_STATUS_INTERNAL_ERROR, "Load ram file failed");
            error_report("FT: load ram file failed");
            break;
        }
        qemu_sem_wait(&ft_load_block_done); //load順序:這裡為平行
        qemu_sem_post(&ft_load_ram_done);
        qemu_sem_wait(&ft_load_ram_notify);
    }

    qemu_sem_wait(&ft_load_block_done);
    qemu_sem_post(&ft_load_ram_done);

    return NULL;
}

static void *ft_load_block_thread_func(void *opaque)
{
    MigrationIncomingState *s = migration_incoming_get_current();
    QEMUFile *block_file = opaque;

    qemu_sem_wait(&ft_load_block_notify);

    while (s->state == MIGRATION_STATUS_FT)
    {
        if (block_load_ft(block_file) < 0)
        {
            ft_set_status(FT_STATUS_INTERNAL_ERROR, "Load block data failed");
            error_report("FT: load block data failed");
            break;
        }
        qemu_sem_post(&ft_load_block_done);
        qemu_sem_wait(&ft_load_block_notify);
    }

    qemu_sem_post(&ft_load_block_done);

    return NULL;
}

static int ft_handle_message_request(FTMessage msg)
{
    if (msg >= FT_MESSAGE__MAX)
    {
        error_report("%s: Got unknown FT message: %d, it's likely because "
                     "Primary M-FTVM is newer than Backup or network corruption.",
                     __func__, msg);
        return -EINVAL;
    }

    switch (msg)
    {
    case FT_MESSAGE_CHECKPOINT_REQUEST:
        return 0;
    case FT_MESSAGE_GUEST_POWERED_OFF:
        error_report("%s: Guest has been turned off on Primary, exiting... ",
                     __func__);
        exit(EXIT_SUCCESS);
    default:
        error_report("%s: Got invalid FT message: %d - %s",
                     __func__, msg, FTMessage_str(msg));
        return -EINVAL;
    }
}

static void ft_backup_do_failover(void *opaque)
{
    MigrationIncomingState *migs = opaque;
    CheckpointingState *cs = checkpointing_get_current();
    qemu_bh_delete(migs->ft_failover_bh);
    if (migs != migration_incoming_get_current())
    {
        fprintf(stderr, "Fatal Error !\nMigrationIncomingState not consistent !\n");
        abort();
    }
    ft_set_status(FT_STATUS_DURING_TAKEOVER, "trying to lock the storage...");
    migrate_set_state(&migs->state, MIGRATION_STATUS_FT,
                      MIGRATION_STATUS_COMPLETED);
    cs->active_role = ft_get_active_role();
    // if (!autostart) {
    //     error_report("\"-S\" qemu option will be ignored in backup side");
    //     /* recover runstate to normal migration finish state */
    //     autostart = true;
    // }

    if (ft_block_replication_enabled() &&
        (ft_load_ram_thread_running || ft_load_block_thread_running))
    {
        qemu_sem_post(&ft_load_ram_notify);
        qemu_sem_post(&ft_load_block_notify);
        qemu_thread_join(&ft_load_block_thread);
        qemu_thread_join(&ft_load_ram_thread);
        ft_load_ram_thread_running = false;
        ft_load_block_thread_running = false;
        /* reset the BLOCK_SIZE to 1MB */
        block_update_block_size((1 << 20), false);
    }

    ft_sync_shutdown("socket error");
    if (!ft_export_is_disabled() && ft_export_timed_wait_takeover_lock())
    {
        FTStatus last_exported_stat = ft_export_get_status();
        if (last_exported_stat == FT_STATUS_VM_POWERED_OFF)
        {
            info_report("%s: VM has been powered off by guest.", __func__);
            exit(EXIT_SUCCESS);
        }
        else if (last_exported_stat == FT_STATUS_VM_CHANGED_DURING_INIT)
        {
            error_report("FT failed to start because VM devices has been"
                         " changed during FT initialization");
            exit(EXIT_FAILURE);
        }
        error_report("Primary has already tookover the VM. "
                     "Now killing this QEMU instance.\n");
        exit(EXIT_FAILURE);
    }
    if (!ft_export_is_disabled() && !ft_export_put_takeover_lock())
    {
        error_report("Failed to put backup lock, "
                "Now killing this QEMU instance.\n");
        exit(EXIT_FAILURE);
    }

    /* For Backup VM, jump to incoming coroutine */
    if (migs->migration_incoming_co)
    {
        qemu_coroutine_enter(migs->migration_incoming_co);
    }

    ft_compression_cleanup(true);

    ft_export_append_migrated();

    ft_set_status(FT_STATUS_BACKUP_TAKEOVER, "Backup Takeover");

    ft_export_cleanup_shared();
    ft_export_cleanup_local();
    qemu_mutex_destroy(&sync_shutdown_mutex);
    if (ft_block_replication_enabled())
    {
        qemu_sem_destroy(&ft_load_ram_notify);
        qemu_sem_destroy(&ft_load_ram_done);
        qemu_sem_destroy(&ft_load_block_notify);
        qemu_sem_destroy(&ft_load_block_done);
    }
}

//
// --------------------------  end for backup side  ---------------------------

void hmp_checkpointing_takeover(Monitor *mon, const QDict *qdict)
{
    char shutdown_reason[2048] = "";
    if (!migration_in_ft_state() && !migration_incoming_in_ft_state())
    {
        info_report("No ongoing checkpointing process");
        return;
    }
    sprintf(shutdown_reason, "HMP Request(%s)", qdict_get_str(qdict, "reason"));
    ft_sync_shutdown(shutdown_reason);
}

void ft_vm_start(void)
{
    qemu_mutex_lock_iothread();
    vm_start();
    qemu_mutex_unlock_iothread();
}

void ft_vm_stop(RunState state)
{
    qemu_mutex_lock_iothread();
    vm_stop_force_state(state);
    qemu_mutex_unlock_iothread();
}

void ft_vm_stop_still_lock(RunState state, bool still_lock)
{
    qemu_mutex_lock_iothread();
    vm_stop_force_state(state);
    if (!still_lock)
    {
        qemu_mutex_unlock_iothread();
    }
}

FTRole ft_get_active_role(void)
{
    if (migration_in_ft_state())
    {
        return FT_ROLE_PRIMARY;
    }
    else if (migration_incoming_in_ft_state())
    {
        return FT_ROLE_BACKUP;
    }
    return FT_ROLE_UNKNOWN;
}

FTRole ft_get_assigned_role(void)
{
    CheckpointingState *cs = checkpointing_get_current();
    return cs->assigned_role;
}

void ft_set_assigned_role(FTRole role)
{
    CheckpointingState *cs = checkpointing_get_current();
    cs->assigned_role = role;
}

// Make ft support info available before migration starts
bool ft_is_supported(void)
{
    return (ft_net_is_supported() && savevm_ft_check_unsupported_device(NULL));
}

/* FT watchdog has 3 stages: 0/1 for keep-alive, and 2 for kill connection and force takeover.
 * If FT messaging is alive, the watchdog will be set to first stage(0),
 * otherwise it will send a keep-alive(primary) or keep waiting(backup) then enter the last stage.
 * Use a new thread instead of QEMUTimer to avoid main thread blocking watchdog functionality.
 */
static void *ft_watchdog_thread_func(void *opaque)
{
    Error *local_err = NULL;
    short stage = 0;
    int ret = 0;
    CheckpointingState *cs = checkpointing_get_current();
    cs->watchdog_stage_interval = migrate_get_current()->parameters.ft_watchdog_stage_interval;

    if (cs->watchdog_stage_interval == 0)
    {
        warn_report("[Checkpointing] Watchdog has been disabled; manual takeover will be required");
        return NULL;
    }
    info_report("%s: Watchdog total timeout is set to %ldms",
                 __func__, cs->watchdog_stage_interval * FT_DEFAULT_WATCHDOG_STAGES);

    struct timespec ts = ft_generate_timespec(cs->watchdog_stage_interval);

    /* Perform watchdog checks periodically when migration is in FT state */
    while (migration_in_ft_state() || migration_incoming_in_ft_state())
    {
        ret = ft_sleep_monotonic(&ts);
        if (ret != 0)
        {
            error_report("%s: clock_nanosleep() failed with %d - %s, stopping FT",
                         __func__, ret, strerror(ret));
            goto kill;
        }

        /* Wait for next watchdog check and reset stage if connection alive */
        if (ft_messaging_is_active())
        {
            stage = 0;
            continue;
        }
        /* Keep-alive stages */
        if (stage < (FT_DEFAULT_WATCHDOG_STAGES - 1))
        {
            /* If is primary and messaging is lockable, try keep-alive and reset stage */
            if (migration_in_ft_state() && ft_message_trylock() == 0)
            {
                ft_send_message(FT_MESSAGE_KEEP_ALIVE, &local_err);
                ft_message_unlock();
                if (local_err)
                {
                    goto kill;
                }
                stage = 0;
                continue;
            }
            /* otherwise just wait (maybe sending ram/vmsd) and enter next stage. */
            ++stage;
        }
        /* Kill stage */
        else
        {
            goto kill;
        }
    }
    /* Not in FT state, stop. */
    return NULL;
kill:
    /* do not kill if guest OS powered down */
    if (atomic_read(&ft_guest_powering_off) == true)
    {
        return NULL;
    }
/*
#ifdef FT_DEBUG_RECORD
    if (migration_in_ft_state())
    {
        error_report("[Checkpointing] Last known recorded item: %s",
                     FTRecord_str(ft_get_last_recorded_item()));
    }

#endif
*/
    /* show 'issuing socket error' in log to make following log more readable */
    ft_sync_shutdown("watchdog timeout (issuing socket error)");
    return NULL;
}

/* message will not need a newline  */
void ft_set_status(FTStatus type, const char *message)
{
    /* Print to screen/log. Maybe able to use QEMU monitor instead. */
    info_report("[FT] %s: %s", FTStatus_str(type), message);

    /* Set migration info */
    checkpointing_get_current()->status = type;

    /* Export to shared storage when assigned as Primary or Backup Takeover */
    if (ft_get_assigned_role() == FT_ROLE_PRIMARY ||
        (ft_get_assigned_role() == FT_ROLE_BACKUP &&
            migration_incoming_get_current()->state == MIGRATION_STATUS_COMPLETED))
    {
        ft_export_put_status(type);
    }

    ft_export_fill_locals();
}

/* Shutdown FT Sync socket(s) immediately and print reason to log. */
void ft_sync_shutdown(const char *reason)
{
    qemu_mutex_lock(&sync_shutdown_mutex);
    info_report("[Checkpointing] Stop syncing due to %s", (reason)? reason : "unknown reason");
    /* get current available qemu_files from migration/migrate_incoming state */
    QEMUFile *from_file = NULL, *to_file = NULL;
    if (migration_in_ft_state())
    {
        from_file = migrate_get_current()->rp_state.from_dst_file;
        to_file = migrate_get_current()->to_dst_file;
    }
    else if (migration_incoming_in_ft_state())
    {
        from_file = migration_incoming_get_current()->from_src_file;
        to_file = migration_incoming_get_current()->to_src_file;
    }
    /* This will wake up FT thread(s) which is blocked in recv() or send(),
     * The from_file and to_file may use the same fd, but we still shutdown the fd for twice,
     * it is harmless although it will return -1. */
    if (from_file)
    {
        qemu_file_shutdown(from_file);
    }
    if (to_file)
    {
        qemu_file_shutdown(to_file);
    }
    /* Waking up main FT thread which is blocked by sem_wait(ft_checkpoint_sem).
     * Backup host does not wait for such kind of semaphore. */
    if (migration_in_ft_state())
    {
        ft_checkpoint_notify(migrate_get_current());
    }
    qemu_mutex_unlock(&sync_shutdown_mutex);
}

bool ft_net_tx_buffering_enabled(void)
{
    return checkpointing_get_current()->
            enabled_capabilities[CHECKPOINTING_CAPABILITY_TX_BUFFERING];
}

bool ft_block_replication_enabled(void)
{
    return checkpointing_get_current()->
            enabled_capabilities[CHECKPOINTING_CAPABILITY_BLOCK_REPLICATE];
}

/* free then malloc buf->data with new_size, without aborting he program
 * (using g_try_malloc). Will allocate slightly bigger than requested.
 * returns: allocate the memory successfully or not */
int ft_discard_resize_qio_channel_buffer(size_t new_size, void *opaque)
{
    QIOChannelBuffer *buf = opaque;
    size_t new_alloc_size;

    if (!buf)
    {
        error_report("%s: called without valid qioc_buf", __func__);
        return -EINVAL;
    }
    if (new_size <= buf->capacity && buf->data)
    {
        return 0;
    }

    new_alloc_size = (size_t)(new_size * 1.05);
    buf->capacity = 0;
    if (buf->data)
    {
        g_free(buf->data);
    }
    buf->data = g_try_malloc(new_alloc_size);
    if (!buf->data)
    {
        return -ENOMEM;
    }
    buf->capacity = new_alloc_size;

    return 0;
}

int ft_sleep_monotonic(struct timespec *ts)
{
    struct timespec remain_ts = {0ll, 0l};
    int ret = clock_nanosleep(CLOCK_MONOTONIC, 0, ts, &remain_ts);
    while (ret == EINTR)
    {
        ret = clock_nanosleep(CLOCK_MONOTONIC, 0, &remain_ts, &remain_ts);
    }
    return ret;
}

struct timespec ft_generate_timespec(int64_t milliseconds)
{
    struct timespec ts = { (long long)(milliseconds / 1000) * 1ll,
                           (long)(milliseconds % 1000) * 1000000l };
    return ts;
}
void ft_init_local_file(void)
{
    CheckpointingState *cs = checkpointing_get_current();
    //init QEMUFile and QIOChannelBuffer success
    cs->vmsd_local_file = cs->block_local_file = NULL;
    cs->ram_local_file = NULL;
    cs->vmsd_local_buffer = cs->block_local_buffer = NULL;
    cs->ram_local_buffer = NULL;
    Error *errp = NULL;
    cs->max_block_local_buffer_sent = cs->max_ram_local_buffer_sent = 0;
    info_report("init QEMUFile and QIOChannelBuffer success");

    //open file on channel
    const char *block_filename="/var/block_file";
    const char *ram_filename="/var/ram_file";
    const char *vmsd_filename="/var/vmsd_file";
    //block
    cs->block_local_buffer = qio_channel_buffer_new_path(block_filename,
                                    O_WRONLY | O_CREAT | O_TRUNC | O_BINARY,
                                    (mode_t) 0660,
                                    &errp);
    if(!cs->block_local_buffer)
    {
        error_report_err(errp);
        info_report("create block channel fail");
        return ;
    }

    //ram
    cs->ram_local_buffer = qio_channel_buffer_new_path(ram_filename,
                                    O_WRONLY | O_CREAT | O_TRUNC | O_BINARY,
                                    (mode_t) 0660,
                                    &errp);                                 
    if(!cs->ram_local_buffer)
    {
        error_report_err(errp);
        info_report("create ram channel fail");
        return ;
    }

    //vmsd
    cs->vmsd_local_buffer = qio_channel_buffer_new_path(vmsd_filename,
                                    O_WRONLY | O_CREAT | O_TRUNC | O_BINARY,
                                    (mode_t) 0660,
                                    &errp);
    if(!cs->vmsd_local_buffer)
    {
        error_report_err(errp);
        info_report("create vmsd channel fail");
        return ;
    }
    info_report("crate file on channel success");

    //set (channel file) on (QMEUFile)
    cs->ram_local_file = qemu_fopen_channel_output(QIO_CHANNEL(cs->block_local_buffer));
    cs->vmsd_local_file = qemu_fopen_channel_output(QIO_CHANNEL(cs->block_local_buffer));
    cs->block_local_file = qemu_fopen_channel_output(QIO_CHANNEL(cs->vmsd_local_buffer));
    info_report("set channel on QMEUFile success");
}
/*
//func
int ft_create_channel_file(QIOChannelFile block_channel,QIOChannelFile ram_channel,QIOChannelFile vmsd_channel);
{
    const char *block_filename="block_file";
    const char *ram_filename="ram_file";
    const char *vmsd_filename="vmsd_file";
    Error *errp = NULL;

    block_channel = qio_channel_file_new_path(block_filename,
                                    O_WRONLY | O_CREAT | O_TRUNC | O_BINARY,
                                    (mode_t) 0660,
                                    &errp);
    ram_channel = qio_channel_file_new_path(ram_filename,
                                    O_WRONLY | O_CREAT | O_TRUNC | O_BINARY,
                                    (mode_t) 0660,
                                    &errp);
    vmsd_channel = qio_channel_file_new_path(vmsd_filename,
                                    O_WRONLY | O_CREAT | O_TRUNC | O_BINARY,
                                    (mode_t) 0660,
                                    &errp);
    return ret;
}
*/