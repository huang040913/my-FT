/*
 * M-FTVM has two type of exports: shared, and local.
 *
 * The shared export requires a shared storage, can be used as
 * preventing accidentially takeover and as second-channel
 * communication.
 * The shared storage path _MUST_ be mounted without file metadata
 * cache, delayed write, and anything that may delay synchronization
 * of the files metadata and content.
 * Mount a NFS client with `rw,lookupcache=none,sync,noac,cto` is
 * tested and recommended.
 *
 * The local export can be utilized as an API to M-FTVM system. It
 * reads the watched files periodically and write responds to files.
 * When an external app want to get the current status, it should
 * write an UUID(A NULL-terminated string which less than 8 bytes) to
 * a predefined file (see migration.json). Local export worker will
 * detect changes of the UUID file, then fill in the information
 * available, and repeat the input UUID to another file to let the
 * external app know the request is done.
 * Due to the frequent read (or write) to the directory by local export,
 * a tmpfs(5) mount is strongly recommended for acceptable performance.
 */

#include "qemu/osdep.h"
#include "qemu/uuid.h"
#include "qemu/error-report.h"
#include "sysemu/sysemu.h"
#include "qapi/qapi-commands-run-state.h"
#include "migration/migration.h"
#include "migration/ft.h"
/* required for checking whether a path is on tmpfs */
#include <linux/magic.h>
#include <sys/statfs.h>

#define FT_EXPORT_MAX_WAIT_TIME_MS 500
#define FT_EXPORT_CHECK_INTERVAL_US 1000
#define FT_EXPORT_MAX_FILE_STR_LENGTH 255
#define FT_EXPORT_TMPFS_POLLING_MS 100
#define FT_EXPORT_NON_TMPFS_POLLING_MS 1000

/* root path of the shared export */
static const char *shared_rootpath = "/var/mftvm";
/* root path of the local export */
static const char *local_rootpath = "/var/mftvm-local";
/* local migrated path: inotifywait(1) on this will get notified
 * when this backup VM has done takeover operation. */
static const char *migrated_path = "/var/mftvm-migrated";
/* local alert path: inotifywait(1) on this will get notified
 * when a takeover operation failed. */
static const char *alert_path = "/var/mftvm-alert";

static const char *takeover_lock_filename = "takeover-lock";
static const char *backup_init_hint_filename = "backup-init";
static const char *shared_status_filename = "last-status";

static char *shared_vmdirpath = NULL;
static char *takeover_lock_path = NULL;
static char *backup_init_hint_path = NULL;
static char *shared_status_path = NULL;
static char *local_vmdirpath = NULL;
static char *locals_path[FT_EXPORT_LOCAL__MAX] = { NULL };

static char *ft_vm_uuid = NULL;
static bool shared_has_error_after_init = false;
static bool shared_export_disabled = false;
static bool local_export_disabled = false;
static bool local_input_table_initialized = false;
static uint64_t local_polling_ms = FT_EXPORT_NON_TMPFS_POLLING_MS;
static QemuMutex fill_locals_mutex;
static QemuThread ft_export_local_thread;

static const FTExportLocal local_input_files[] = {
    FT_EXPORT_LOCAL_SESSION_INPUT, FT_EXPORT_LOCAL_TAKEOVER,
    FT_EXPORT_LOCAL_DESTROY, FT_EXPORT_LOCAL_RPO_REQUEST,
    FT_EXPORT_LOCAL_CHECKPOINT_NOW, FT_EXPORT_LOCAL_DR_MODE_REQUEST
};
static bool local_input_table[FT_EXPORT_LOCAL__MAX] = {false};

/* Note: functions here return 1 (true) when succeed, 0 when failed.
 * POSIX file/dir functions often return 0 when succeed, -1 and errno is provieded when failed.
 */
static bool ft_export_prepare_shared_vmdir(void);
static __attribute__((always_inline)) inline
        void ft_put_str_to_file(const char *path, const char *str, bool append, bool lock);
static __attribute__((always_inline)) inline char *ft_get_str_from_file(const char *path);
/* opendir then closedir, error_report and return successful or not */
static bool ft_try_opendir(const char *dir_path);
static bool ft_export_timed_wait_for_file(char *path);

/* Should only be called during export init */
static bool ft_export_rm_peer_takeover_lock(void);

static void *ft_export_locals_polling_worker(void *opaque);
static __attribute__((always_inline)) inline
        bool is_input_local_file(FTExportLocal enum_item);

static bool ft_export_internal_init(void)
{
    /* Get VM UUID */
    if (!ft_vm_uuid)
    {
        if (qemu_uuid_is_null(&qemu_uuid))
        {
            ft_set_status(FT_STATUS_NO_UUID, "[Checkpointing] Export "
                          "components will not start since VM UUID is NULL.");
            return false;
        }
        ft_vm_uuid = qemu_uuid_unparse_strdup(&qemu_uuid);
    }
    return true;
}

static bool ft_export_shared_init_common(void)
{
    shared_export_disabled = !migrate_get_current()->parameters.ft_shared_export;
    if (shared_export_disabled)
    {
        error_report("[Checkpointing] %s: Shared export is disabled by migrate "
          "parameter. Set the param to on and restart FT to enable Export.", __func__);
        return true;
    }

    if (!ft_export_internal_init())
    {
        error_report("[Checkpointing] %s: ft_export_internal_init() failed", __func__);
        return false;
    }

    /* Check for existence of export rootpath and its permissions(drwx***r*x) */
    struct stat dir_stat;
    if (!ft_try_opendir(shared_rootpath))
    {
        return false;
    }
    if (stat(shared_rootpath, &dir_stat) < 0)
    {
        error_report("%s: Cannot stat %s because %s",
                     __func__, shared_rootpath, strerror(errno));
        return false;
    }
    if (!(dir_stat.st_mode & (S_IRWXU | S_IROTH | S_IXOTH)))
    {
        error_report("%s: %s folder requires drwx***r*x mode",
                     __func__, shared_rootpath);
        return false;
    }

    /* Setup VM paths */
    shared_vmdirpath = g_build_filename(shared_rootpath, ft_vm_uuid, NULL);
    takeover_lock_path =
            g_build_filename(shared_vmdirpath, takeover_lock_filename, NULL);
    backup_init_hint_path =
            g_build_filename(shared_vmdirpath, backup_init_hint_filename, NULL);
    shared_status_path =
            g_build_filename(shared_vmdirpath, shared_status_filename, NULL);

    shared_has_error_after_init = false;
    return true;
}

/* Initialize primary part of FT Export during sending migration header.
 * This function should be called before saving vm state to the QEMUFile;
 * if anything went wrong, migration can be cancelled immediately.
 */
bool ft_export_init_primary(void)
{
    if (!ft_export_shared_init_common())
    {
        return false;
    }
    if (shared_export_disabled)
    {
        return true;
    }

    /* Create a shared folder named after UUID and ensure permissions */
    if (mkdir(shared_vmdirpath, S_IRWXU | S_IRGRP | S_IROTH | S_IXOTH) != 0)
    {
        int mkdir_err = errno;
        if (mkdir_err == EEXIST)
        {
            info_report("FT export: %s already exists, files that conflicts will be removed",
                        shared_vmdirpath);
            ft_export_prepare_shared_vmdir();
        }
        else
        {
            error_report("export folder %s create failed: %s",
                         shared_vmdirpath, strerror(mkdir_err));
            return false;
        }
    }

    if (!ft_try_opendir(shared_vmdirpath))
    {
        return false;
    }

    /* Put primary lock to validate export functionality of backup  */
    return ft_export_put_takeover_lock();
}

/* Initialize backup part of FT Export during loading vmstate
 *  (right after QEMU FT magic is received. Primary has already created the vmdir)
 */
bool ft_export_init_backup(void)
{
    if (!ft_export_shared_init_common())
    {
        return false;
    }
    if (shared_export_disabled)
    {
        return true;
    }

    /* Check if the shared folder of this VM has been created */
    if (!ft_try_opendir(shared_vmdirpath))
    {
        return false;
    }

    /* Check primary lock, remove the lock, and put the lock with backup info */
    if (!ft_export_timed_wait_takeover_lock() || !ft_export_rm_peer_takeover_lock() ||
        !ft_export_put_takeover_lock())
    {
        error_report("backup export init validation failed.");
        return false;
    }

    /* Notify primary that backup has put the lock by storage*/
    int backup_hint = open(backup_init_hint_path, O_CREAT | O_EXCL | O_SYNC,
                           S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (backup_hint < 0)
    {
        error_report("put backup hint failed: %s", strerror(errno));
        return false;
    }
    return true;
}

bool ft_export_init_primary_validate(void)
{
    if (shared_export_disabled)
    {
        return true;
    }
    /* Backup will put a hint file before put the lock. */
    if (!ft_export_timed_wait_for_file(backup_init_hint_path) ||
        !ft_export_timed_wait_takeover_lock() || !ft_export_rm_peer_takeover_lock())
    {
        error_report("primary export init validation failed.");
        return false;
    }
    return !unlink(backup_init_hint_path);
}

bool ft_export_init_backup_validate(void)
{
    return !ft_export_exists_takeover_lock();
}

bool ft_export_put_takeover_lock(void)
{
    if (shared_export_disabled)
    {
        return true;
    }
    /* Make sure lock is "created", not "truncating" to prevent conflicts */
    int lock_fd = open(takeover_lock_path, O_CREAT | O_EXCL | O_SYNC | O_WRONLY,
                                              S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (lock_fd == -1)
    {
        error_report("Failed to put lock: %s", strerror(errno));
        shared_has_error_after_init = true;
        return false;
    }

    FILE *lock = fdopen(lock_fd, "w");
    if (fprintf(lock, "%d\n", ft_get_assigned_role()) < 0)
    {
        error_report("Writing lock information failed");
        /* safe to continue because init check will handle this failure */
    }
    return fclose(lock) == 0;
}

bool ft_export_rm_peer_takeover_lock(void)
{
    if (!ft_export_timed_wait_takeover_lock())
    {
        error_report("Lock did not exist!");
        return false;
    }

    int lock_role = FT_ROLE_UNKNOWN;
    FILE *lock = fopen(takeover_lock_path, "r");
    if (!lock)
    {
        error_report("Lock content open failed: %s", strerror(errno));
        return false;
    }
    if (fscanf(lock, "%d", &lock_role) == EOF ||
        lock_role == FT_ROLE_UNKNOWN || lock_role == ft_get_assigned_role())
    {
        error_report("Storage handshake failed, role=%d", lock_role);
        return false;
    }

    return fclose(lock) == 0 && unlink(takeover_lock_path) == 0;
}

/* Remove existing known files to make sure shared export works properly. */
bool ft_export_prepare_shared_vmdir(void)
{
    bool ret = false;
    /* unlink() returns !0 if failed, so any failure will mark `ret` as true;
     * but returning false if any file unlinking failed to follow other FT func. */
    ret |= unlink(takeover_lock_path);
    ret |= unlink(shared_status_path);
    ret |= unlink(backup_init_hint_path);
    return !ret;
}

bool ft_export_timed_wait_takeover_lock(void)
{
    return ft_export_timed_wait_for_file(takeover_lock_path);
}

static bool ft_export_timed_wait_for_file(char *path)
{
    int64_t start_time = qemu_clock_get_ms(QEMU_CLOCK_HOST);
    while ((qemu_clock_get_ms(QEMU_CLOCK_HOST) - start_time) <= FT_EXPORT_MAX_WAIT_TIME_MS)
    {
        if (access(path, F_OK) == 0)
        {
            return true;
        }
        /* Lower check frequency to prevent CPU/IO congestion */
        g_usleep(FT_EXPORT_CHECK_INTERVAL_US);
    }
    return false;
}

bool ft_export_exists_takeover_lock(void)
{
    return access(takeover_lock_filename, F_OK) == 0;
}

/* Put a string to a file (path).  Appending to the end of file or do POSIX
 * locking can be set by arguments. */
void ft_put_str_to_file(const char *path, const char *str, bool append, bool lock)
{
    if (!path || !str)
    {
        return;
    }

    /* Initialize file locking structs. pid will be set at runtime. */
    static struct flock write_lock = {F_WRLCK, SEEK_SET, 0, 0, 0};
    static struct flock write_unlock = {F_UNLCK, SEEK_SET, 0, 0, 0};

    FILE *file = fopen(path, append ? "a" : "w");
    if (!file)
    {
        error_report("%s: cannot put %s to %s: %s",
                     __func__, str, path, strerror(errno));
        return;
    }

    /* take the lock to prevent file corruption. pid should always > 0. */
    if (lock && (write_lock.l_pid = getpid()) &&
        fcntl(fileno(file), F_SETLKW, &write_lock) == -1)
    {
        warn_report("%s: (negligible) %s lock failed: %s",
                    __func__, path, strerror(errno));
    }

    int fprintf_ret = fprintf(file, "%s\n", str);
    if (fprintf_ret < 0)
    {
        warn_report("%s: write %s to %s failed, returned %d",
                    __func__, str, path, fprintf_ret);
    }

    if (lock && (write_unlock.l_pid = getpid()) &&
        fcntl(fileno(file), F_SETLK, &write_unlock) == -1)
    {
        warn_report("%s: (negligible) %s unlock failed: %s",
                    __func__, path, strerror(errno));
    }
    fclose(file);
}

static char *ft_get_str_from_file(const char *path)
{
    if (!path)
    {
        return NULL;
    }

    FILE *file = fopen(path, "r");
    if (!file)
    {
        error_report("cannot open %s: %s", path, strerror(errno));
        return NULL;
    }

    char *str_from_file = g_malloc0(FT_EXPORT_MAX_FILE_STR_LENGTH + 1);
    int ret =
        fscanf(file, "%" stringify(FT_EXPORT_MAX_FILE_STR_LENGTH) "s", str_from_file);
    fclose(file);

    if (ret == 0 || ret == EOF)
    {
        g_free(str_from_file);
        return NULL;
    }
    return str_from_file;
}

bool ft_try_opendir(const char *dir_path)
{
    DIR *dir = opendir(dir_path);
    if (dir == NULL)
    {
        error_report("%s: Cannot open %s because %s",
                     __func__, dir_path, strerror(errno));
        return false;
    }
    if (closedir(dir) == -1)
    {
        error_report("%s: Cannot close %s because %s",
                     __func__, dir_path, strerror(errno));
        return false;
    }
    return true;
}

void ft_export_put_status(FTStatus status)
{
    if (shared_export_disabled || g_strcmp0(shared_status_path, "") == 0)
    {
        return;
    }
    ft_put_str_to_file(shared_status_path, FTStatus_str(status), false, false);
}

FTStatus ft_export_get_status(void)
{
    if (shared_export_disabled)
    {
        return FT_STATUS__MAX;
    }
    char *str_from_file = ft_get_str_from_file(shared_status_path);
    if (!str_from_file)
    {
        return FT_STATUS__MAX;
    }
    int error_val = qapi_enum_parse(&FTStatus_lookup, str_from_file, -1, NULL);

    g_free(str_from_file);
    if (error_val < 0)
    {
        return FT_STATUS__MAX;
    }
    return error_val;
}

void ft_export_append_migrated(void)
{
    if (local_export_disabled)
    {
        return;
    }
    ft_put_str_to_file(migrated_path, ft_vm_uuid, true, true);
}

void ft_export_append_alert(void)
{
    if (local_export_disabled)
    {
        return;
    }
    ft_put_str_to_file(alert_path, ft_vm_uuid, true, true);
}

bool ft_export_is_disabled(void)
{
    return shared_export_disabled;
}

void ft_export_cleanup_shared(void)
{
    g_free(shared_vmdirpath);
    shared_vmdirpath = NULL;
    g_free(takeover_lock_path);
    takeover_lock_path = NULL;
    g_free(backup_init_hint_path);
    backup_init_hint_path = NULL;
    g_free(shared_status_path);
    shared_status_path = NULL;
}

/* Disable shared FT Export until next migration */
void ft_disable_shared_export(void)
{
    shared_export_disabled = true;
}

bool ft_export_init_local(bool export_local_enabled)
{
    local_export_disabled = !export_local_enabled;

    /* init mutex even if disabled to let fill_local works properly */
    qemu_mutex_init(&fill_locals_mutex);

    if (local_export_disabled)
    {
        warn_report("[Checkpointing] %s: local export disabled.", __func__);
        return true;
    }

    if (!ft_export_internal_init())
    {
        error_report("[Checkpointing] %s: ft_export_internal_init() failed", __func__);
        return false;
    }

    /* initialize paths */
    g_free(local_vmdirpath);
    local_vmdirpath = g_build_filename(local_rootpath, ft_vm_uuid, NULL);
    for (int i = 0; i < FT_EXPORT_LOCAL__MAX; ++i)
    {
        g_free(locals_path[i]);
        locals_path[i] =
                g_build_filename(local_vmdirpath, FTExportLocal_str(i), NULL);
    }

    /* check local_rootpath exists, have right permissions, and is on a tmpfs;
     * slow down polling freq if is not on a tmpfs */
    struct stat dir_stat;
    struct statfs dir_statfs;
    if (!ft_try_opendir(local_rootpath))
    {
        goto error_out;
    }

    if (stat(local_rootpath, &dir_stat) < 0)
    {
        error_report("%s: Cannot stat %s because %s",
                     __func__, local_rootpath, strerror(errno));
        goto error_out;
    }
    if (!(dir_stat.st_mode & (S_IRWXU | S_IRWXG)))
    {
        error_report("%s: %s folder requires drwxrwx*** mode",
                     __func__, local_rootpath);
        goto error_out;
    }

    statfs(local_rootpath, &dir_statfs);
    if (dir_statfs.f_type != TMPFS_MAGIC)
    {
        warn_report("%s: local export dir %s is not a tmpfs. Mount it with "
                    "tmpfs or the input polling frequency will be limited.",
                    __func__, local_rootpath);
        local_polling_ms = FT_EXPORT_NON_TMPFS_POLLING_MS;
    }
    else
    {
        local_polling_ms = FT_EXPORT_TMPFS_POLLING_MS;
    }

    /* create local vmdir if not exists */
    if (mkdir(local_vmdirpath, S_IRWXU | S_IRWXG) != 0)
    {
        int mkdir_err = errno;
        if (mkdir_err == EEXIST)
        {
            if (chmod(local_vmdirpath, S_IRWXU | S_IRWXG) != 0)
            {
                error_report("%s: chmod %s failed because %s",
                             __func__, local_vmdirpath, strerror(mkdir_err));
                goto error_out;
            }
        }
        else
        {
            error_report("%s: local export vmdir %s create failed because %s",
                         __func__, shared_vmdirpath, strerror(mkdir_err));
            goto error_out;
        }
    }
    if (!ft_try_opendir(local_vmdirpath))
    {
        goto error_out;
    }

    /* create local export files (truncating existing files) */
    FILE *file = NULL;
    for (int i = 0; i < FT_EXPORT_LOCAL__MAX; ++i)
    {
        file = fopen(locals_path[i], "w");
        if (!file)
        {
            error_report("%s: fopen %s failed because %s",
                         __func__, locals_path[i], strerror(errno));
            goto error_out;
        }

        if (fclose(file) == EOF)
        {
            error_report("%s: fclose %s failed because %s",
                         __func__, locals_path[i], strerror(errno));
            goto error_out;
        }
        file = NULL;

        if (chmod(locals_path[i], S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP) != 0)
        {
            error_report("%s: export local %s chmod failed because %s",
                         __func__, locals_path[i], strerror(errno));
            goto error_out;
        }
    }

    ft_export_fill_locals();

    /* Check for existence and permission of mftvm-migrated file */
    struct stat file_stat;
    if (stat(migrated_path, &file_stat) < 0)
    {
        error_report("Cannot stat %s: %s", migrated_path, strerror(errno));
        return false;
    }
    if (S_ISDIR(file_stat.st_mode) ||
        !(file_stat.st_mode & (S_IRUSR | S_IWUSR | S_IROTH)))
    {
        error_report("%s is not -rw****r**", migrated_path);
        return false;
    }

    /* Check for existence and permission of mftvm-alert file */
    if (stat(alert_path, &file_stat) < 0)
    {
        error_report("Cannot stat %s: %s", alert_path, strerror(errno));
        return false;
    }
    if (S_ISDIR(file_stat.st_mode) ||
        !(file_stat.st_mode & (S_IRUSR | S_IWUSR | S_IROTH)))
    {
        error_report("%s is not -rw****r**", alert_path);
        return false;
    }

    return true;

error_out:
    error_report("[Checkpointing] %s: failed to init local export", __func__);
    return false;
}

void ft_export_cleanup_local(void)
{
    if (atomic_read(&local_export_disabled) == true)
    {
        return;
    }
    qemu_mutex_lock(&fill_locals_mutex);
    atomic_set(&local_export_disabled, true);
    g_free(local_vmdirpath);
    local_vmdirpath = NULL;

    for (int i = 0; i < FT_EXPORT_LOCAL__MAX; ++i)
    {
        g_free(locals_path[i]);
        locals_path[i] = NULL;
    }
    qemu_mutex_unlock(&fill_locals_mutex);
}

bool is_input_local_file(FTExportLocal enum_item)
{
    /* create table for lookup */
    if (!local_input_table_initialized)
    {
        for (int i = 0; i < FT_EXPORT_LOCAL__MAX; ++i)
        {
            bool is_input = false;
            for (int j = 0;
                 j < (sizeof(local_input_files) / sizeof(local_input_files[0])); ++j)
            {
                if (i == local_input_files[j])
                {
                    is_input = true;
                    break;
                }
            }
            local_input_table[i] = is_input;
        }
        local_input_table_initialized = true;
    }
    return local_input_table[enum_item];
}

void ft_export_fill_locals(void)
{
    qemu_mutex_lock(&fill_locals_mutex);
    /* prevent race conditions between takeover status fill and cleanup */
    if (local_export_disabled)
    {
        qemu_mutex_unlock(&fill_locals_mutex);
        return;
    }

    MigrationState *s = migrate_get_current();
    CheckpointingState *cs = checkpointing_get_current();
    char rpo_str[32] = "";
    char last_synced_clock[32] = "";

    /* hmp.c: hmp_info_status() */
    StatusInfo *info = qmp_query_status(NULL);

    /* checkpoint_delay is not avail on Backup */
    sprintf(rpo_str, "%lu", (migration_in_ft_state()) ?
            atomic_read(&s->parameters.ft_checkpoint_delay) : 0lu);

    sprintf(last_synced_clock, "%ld", cs->last_checkpointed_host_clock);

    ft_put_str_to_file(locals_path[FT_EXPORT_LOCAL_STATUS],
            FTStatus_str(cs->status), false, false);
    ft_put_str_to_file(locals_path[FT_EXPORT_LOCAL_ROLE],
            FTRole_str(ft_get_assigned_role()), false, false);
    ft_put_str_to_file(locals_path[FT_EXPORT_LOCAL_RUNSTATE],
            RunState_str(info->status), false, false);
    ft_put_str_to_file(locals_path[FT_EXPORT_LOCAL_RPO], rpo_str,
                       false, false);
    ft_put_str_to_file(locals_path[FT_EXPORT_LOCAL_LAST_SYNCED],
                       last_synced_clock, false, false);
    ft_put_str_to_file(locals_path[FT_EXPORT_LOCAL_CHECKPOINTING_TYPE],
            CheckpointingType_str(cs->type), false, false);

    //TODO impl DR Mode Switch and stat

    qapi_free_StatusInfo(info);
    qemu_mutex_unlock(&fill_locals_mutex);
}

void ft_export_start_locals_polling(void)
{
    if (local_export_disabled)
    {
        return;
    }
    qemu_thread_create(&ft_export_local_thread,
                       "ft_export_local_worker",
                       ft_export_locals_polling_worker,
                       NULL, QEMU_THREAD_DETACHED);
}

void *ft_export_locals_polling_worker(void *opaque)
{
    MigrationState *s = migrate_get_current();

    info_report("[Checkpointing] Local Export thread started");

    while ((migration_in_ft_state() || migration_incoming_in_ft_state()) &&
           !local_export_disabled)
    {
        g_usleep(local_polling_ms * 1000);

        char *input_str = NULL;
        char *input[FT_EXPORT_LOCAL__MAX] = { NULL };

        /* poll all inputs */
        for (int i = 0; i < FT_EXPORT_LOCAL__MAX; ++i)
        {
            if(!is_input_local_file(i))
            {
                continue;
            }

            input_str = ft_get_str_from_file(locals_path[i]);
            if (input_str == NULL || strlen(input_str) == 0)
            {
                g_free(input_str);
                input_str = NULL;
                continue;
            }
            input[i] = input_str;
            input_str = NULL;
        }

        /* respond to modifed input file(s) */
        if (input[FT_EXPORT_LOCAL_TAKEOVER])
        {
            if (g_strcmp0(input[FT_EXPORT_LOCAL_TAKEOVER], "1") == 0)
            {
                ft_sync_shutdown("local export required takeover");
                break;
            }
            else
            {
                warn_report("%s: Write 1 for takeover triggering", __func__);
            }
        }
        if (input[FT_EXPORT_LOCAL_DESTROY])
        {
            if (g_strcmp0(input[FT_EXPORT_LOCAL_DESTROY], "1") == 0)
            {
                info_report("%s: self-destroying...", __func__);
                exit(EXIT_SUCCESS);
            }
            else
            {
                warn_report("%s: Write 1 for destory triggering", __func__);
            }
        }
        if (input[FT_EXPORT_LOCAL_RPO_REQUEST] && migration_in_ft_state())
        {
            uint64_t new_checkpoint_delay =
                        strtoull(input[FT_EXPORT_LOCAL_RPO_REQUEST], NULL, 10);
            if (errno == ERANGE || errno == EINVAL)
            {
                error_report("%s: invalid RPO %s because %s, "
                             "only digit is accepted", __func__,
                             input[FT_EXPORT_LOCAL_RPO_REQUEST],  strerror(errno));
            }
            else if (new_checkpoint_delay > 0)
            {
                info_report("%s: Setting desired RPO to %lu. Issue checkpoint-now "
                            "if the last RPO set is too large.",
                            __func__, new_checkpoint_delay);
                atomic_set(&s->parameters.ft_checkpoint_delay, new_checkpoint_delay);
            }
            else
            {
                error_report("%s: RPO must be >= 1", __func__);
            }
        }
        else if (input[FT_EXPORT_LOCAL_RPO_REQUEST])
        {
            error_report("RPO request is only available on Primary host.");
        }

        if (input[FT_EXPORT_LOCAL_CHECKPOINT_NOW] && migration_in_ft_state())
        {
            if (g_strcmp0(input[FT_EXPORT_LOCAL_CHECKPOINT_NOW], "1") == 0)
            {
                info_report("%s: checkpoint triggered", __func__);
                ft_checkpoint_notify(migrate_get_current());
            }
            else
            {
                warn_report("%s: Write 1 for destory triggering", __func__);
            }
        }
        else if (input[FT_EXPORT_LOCAL_CHECKPOINT_NOW])
        {
            warn_report("%s: checkpoint-now is only available on "
                        "Primary Host", __func__);
        }

        //TODO dr-mode-request requires DRMode impl

        if (input[FT_EXPORT_LOCAL_SESSION_INPUT])
        {
            ft_export_fill_locals();
            /* copy the session ID to new file at the last step, let
             * external apps know that the outputs are ready to read */
            ft_put_str_to_file(locals_path[FT_EXPORT_LOCAL_SESSION_CURRENT],
                               input[FT_EXPORT_LOCAL_SESSION_INPUT], false, false);
        }

        /* reset input files and buffer */
        for (int i = 0; i < FT_EXPORT_LOCAL__MAX; ++i)
        {
            if (!is_input_local_file(i))
            {
                continue;
            }
            if (input[i])
            {
                ft_put_str_to_file(locals_path[i], "", false, false);
            }

            g_free(input[i]);
        }
    }

    info_report("[Checkpointing] Local Export thread exited");
    return NULL;
}
