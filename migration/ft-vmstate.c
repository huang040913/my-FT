/*
 * ft-vmstate tell backup need enable ft
 */

#include "qemu/osdep.h"
#include "migration.h"
#include "migration/vmstate.h"
#include "migration/ft.h"

static int ft_info_pre_save(void *opaque);
static bool ft_info_need(void *opaque);

struct FTInfo {
    bool ft_requested;
} ;
typedef struct FTInfo FTInfo ;

static FTInfo ft_info = {false};

bool ft_vmstate_get_info(void) {
    return ft_info.ft_requested;
}

void ft_vmstate_clear_info(void) {
    ft_info.ft_requested = false;
}

static int ft_info_pre_save(void *opaque) {
    FTInfo *s = opaque;
    s->ft_requested = migrate_ft_enabled();
    return 0;
}

static bool ft_info_need(void *opaque) {
    return migrate_ft_enabled();
}

static const VMStateDescription ft_vmstate = {
    .name = "FTState",
    .version_id = 1,
    .minimum_version_id = 1,
    .pre_save = ft_info_pre_save,
    .needed = ft_info_need,
    .fields = (VMStateField[]) {
        VMSTATE_BOOL(ft_requested, FTInfo),
        VMSTATE_END_OF_LIST()
    },
};

void ft_info_init(void) {
    vmstate_register(NULL, 0, &ft_vmstate, &ft_info);
}
