#include "qemu/osdep.h"
//#include "sysemu/sysemu.h"
#include "qapi/error.h"
#include "migration.h"
#include "migration/ft.h"
#include "qemu-file-channel.h"
#include "qemu-file.h"
#include "savevm.h"
#include "io/channel-buffer.h"
#include "trace.h"
#include "qemu/error-report.h"
#include "qemu/iov.h"
#include "net/net.h"
#include "qemu/typedefs.h"
#include "qemu/thread.h"
#include "net/tap.h"
#include "net/vhost_net.h"
#include "hw/virtio/virtio-net.h"

#include "qemu/main-loop.h"

/*
 *  Copy from /net/queue.c
 *  must be compatible with QEMU native struct
 *  ================/net/queue.c==================
 */
struct NetPacket {
    QTAILQ_ENTRY(NetPacket) entry;
    NetClientState *sender;
    unsigned flags;
    int size;
    NetPacketSent *sent_cb;
    uint8_t data[0];
};

struct NetQueue {
    void *opaque;
    uint32_t nq_maxlen;
    uint32_t nq_count;
    NetQueueDeliverFunc *deliver;

    QTAILQ_HEAD(packets, NetPacket) packets;

    unsigned delivering : 1;
};
/*  =============================================
 */

/*  ================/net/queue.c==================
 */

typedef QTAILQ_HEAD(, NetPacket) NetPacketHead;

typedef QTAILQ_HEAD(, NetClientState) NetClientStateHead;

/*
 *  for multi NICs, record NIC pair
 */
struct NICsQueue {
    QTAILQ_ENTRY(NICsQueue) entry;
    NetQueue *backend_nq; /* guest side, like virtio-net-pci */
    NetPacketHead *backend;  /* host side, like TAP */
    NetPacketHead *ft_tx_queue;
    NetPacketHead *ft_tx_out_queue;
};

typedef QTAILQ_HEAD(, NICsQueue) NICsQueueHead;

static NetPacket *ft_net_new_net_packet(NetClientState *,
                                        const struct iovec *const,
                                        const int,
                                        const ssize_t);

static void ft_net_packet_queue_concat(NetPacketHead *const,
                                       NetPacketHead *const);

static void qemu_net_queue_flush_bh(void *);

static NICsQueueHead _nics = QTAILQ_HEAD_INITIALIZER(_nics);
static NICsQueueHead *const nics = &_nics;

static QEMUBH *ft_net_flush_tx_bh = NULL;

static ssize_t tx_packets_total_length = 0;

static CheckpointingState *cs = NULL;

/* only call from virtio_net_flush_tx() - main thread
 * append to tx_queue
 */
ssize_t ft_net_tx_queue_append(void *_NetClientState,
                               const void *const _IOVEC,
                               const int iovcnt) {
    /* If TX buffering is disabled, this function will not be called in virtio-net.c */
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_TX_BUFFERING] == false)
    {
        return 0;
    }

    NetClientState *sender = _NetClientState;
    const struct iovec *const iov = _IOVEC;
    ssize_t iovs_len = iov_size(iov, iovcnt);

    if(unlikely(sender->link_down || !sender->peer)) {
        return iovs_len;
    }

    NICsQueue *nicq = sender->ft_net;

    NetPacket *packet = ft_net_new_net_packet(sender, iov, iovcnt, iovs_len);

    QTAILQ_INSERT_TAIL(nicq->ft_tx_queue, packet, entry);
    atomic_add(&tx_packets_total_length, iovs_len);

    nicq->backend_nq->nq_count++;
    return iovs_len;
}

/*
 * Migration(FT) thread: called after save vm
 * move ft_tx_queue to ft_tx_out_queue
 * ft_tx_queue can add new VM tx
 * flush ft_tx_out_queue after send VM
*/
ssize_t ft_net_post_savevm(void) {
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_TX_BUFFERING] == false)
    {
        return 0;
    }

    ssize_t total = tx_packets_total_length;

    NICsQueue *nicq;
    QTAILQ_FOREACH(nicq, nics, entry) {
        ft_net_packet_queue_concat(nicq->ft_tx_out_queue, nicq->ft_tx_queue);
    }

    tx_packets_total_length = 0;

    return total;
}

//only use on Primary side, for flush tx after primary send vm to backup
void ft_net_post_sendvm(void) {
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_TX_BUFFERING] == false)
    {
        return ;
    }

    qemu_mutex_lock_iothread();

    NICsQueue *nicq;
    QTAILQ_FOREACH(nicq, nics, entry) {
        ft_net_packet_queue_concat(nicq->backend, nicq->ft_tx_out_queue);
    }

    qemu_mutex_unlock_iothread();
}

//only use in migration(FT) thread
void ft_net_flush_tx(void) {
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_TX_BUFFERING] == false)
    {
        return ;
    }

    qemu_bh_schedule(ft_net_flush_tx_bh);
}

//only use in main thread (IO thread) when failover
ssize_t ft_net_failover_flush_tx(void) {
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_TX_BUFFERING] == false)
    {
        return 0;
    }

    NICsQueue *nicq;
    ssize_t ret = tx_packets_total_length;

    QTAILQ_FOREACH(nicq, nics, entry) {
        ft_net_packet_queue_concat(nicq->backend, nicq->ft_tx_out_queue);
        ft_net_packet_queue_concat(nicq->backend, nicq->ft_tx_queue);
    }

    tx_packets_total_length = 0;

    return ret;
}

// concat source to dest tail then clear source header
static void ft_net_packet_queue_concat(NetPacketHead *const dest,
                                       NetPacketHead *const source) {
    /* NetPacket *np; */
    if( QTAILQ_EMPTY(source) ) {
        return;
    }
    // not compatible above version 4.x
    /*
    *(dest->tqh_last) = source->tqh_first;
    source->tqh_first->entry.tqe_prev = dest->tqh_last;
    dest->tqh_last = source->tqh_last;
    */
    /*
    QTAILQ_FOREACH(np, source, entry) {
        QTAILQ_INSERT_TAIL(dest, np, entry);
    }
    */
    dest->tqh_circ.tql_prev->tql_next = source->tqh_first;
    source->tqh_first->entry.tqe_circ.tql_prev = dest->tqh_circ.tql_prev;
    dest->tqh_circ.tql_prev = source->tqh_circ.tql_prev;
    QTAILQ_INIT(source);
}

static NetPacket *ft_net_new_net_packet(NetClientState *sender,
                                        const struct iovec *const iov,
                                        const int iovcnt,
                                        const ssize_t iovs_len) {
    NetPacket *packet;

    packet = g_malloc(sizeof(NetPacket) + iovs_len);
    packet->sender = sender;
    packet->sent_cb = NULL;
    packet->flags = QEMU_NET_PACKET_FLAG_NONE;
    packet->size = 0;

    for(int i = 0; i < iovcnt; i++) {
        memcpy(packet->data + packet->size, iov[i].iov_base, iov[i].iov_len);
        packet->size += iov[i].iov_len;
    }

    return packet;
}

//only use on Primary side, real flush tx via QEMU
static void qemu_net_queue_flush_bh(void *opaque) {
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_TX_BUFFERING] == false)
    {
        return ;
    }

    NICsQueue *nicq;
    QTAILQ_FOREACH(nicq, nics, entry) {
        qemu_net_queue_flush(nicq->backend_nq);
    }
}

/* Check the NCS passed is compatible or not
 *  and set proper FTStatus if incompatible NCS found.
 * Return true if ncs is "TAP" or "virtio NIC with TAP backend". */
static bool ft_net_is_ncs_supported(NetClientState *ncs)
{
    if(ncs->info->type == NET_CLIENT_DRIVER_TAP) {
        return true;
    }
    if(ncs->info->type != NET_CLIENT_DRIVER_NIC) {
        ft_set_status(FT_STATUS_NET_BACKEND_NOT_SUPPORTED,
                "Some of the NetClientDrivers(maybe VHOST, SLIRP/USER, ...)"
                "is not supported to start FT.");
        goto fail;
    }
    if(ncs->model && strcmp(ncs->model, "virtio-net-pci") != 0) {
        ft_set_status(FT_STATUS_NET_FRONTEND_NOT_SUPPORTED,
                "The device model of NIC(s) is not supported to start FT. "
                "Only virtio NIC is supported.");
        goto fail;
    }
    if(!ncs->peer || ncs->peer->info->type != NET_CLIENT_DRIVER_TAP) {
        ft_set_status(FT_STATUS_NET_BACKEND_NOT_SUPPORTED,
                "The backend of NIC(s) is not supported to start FT. "
                "Only TAP is supported.");
        goto fail;
    }
    return true;

fail:
    error_report("FT only support virtio net guest and TAP qemu backend"
        " network option !\n"
        "Example QEMU argv: ...... -netdev tap,ifname=tap0,id=tapnet0,"
        "script=no,downscript=/etc/qemu-ifdown ......\n"
        "         ...... -device virtio-net-pci,netdev=tapnet0,"
        "id=nic0,mac=10:11:22:33:44:55 ......\n"
        "Notice: Don't use -net option.\nNotice: For more detail visit :"
        "\"https://qemu.weilnetz.de/doc/qemu-doc.html#Network-options\"\n"
        "        \"https://wiki.qemu.org/Documentation/Networking\"\n");
    return false;
}

/* vhost-net use in-kernel packet processing to make virtio more efficiently.
 * However the packet queue can not be intercept without modifying kernel module;
 * will try disabling it if vhost-net is detected.
 * See net_init_tap_one() for possible re-enabling flow.
 *
 * For more details:
 * https://www.redhat.com/en/blog/introduction-virtio-networking-and-vhost-net
 * https://www.redhat.com/en/blog/deep-dive-virtio-networking-and-vhost-net
 * https://www.redhat.com/en/blog/hands-vhost-net-do-or-do-not-there-no-try
 */
static void ft_disable_ncs_vhost(NetClientState *ncs)
{
    ft_tap_stop_vhost(ncs->peer);
    VirtIONet *n = qemu_get_nic_opaque(ncs);
    n->vhost_started = false;
}

bool ft_net_init(Error **errp) {
    cs = checkpointing_get_current();
    /* Any NIC/network(even vhost) looks good if no tx buffering is required */
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_TX_BUFFERING] == false)
    {
        return true;
    }

    NetClientStateHead *net_clients = get_net_clients_list_head();
    NetClientState *ncs;
    NICsQueue *nicq;

    QTAILQ_FOREACH(ncs, net_clients, next) {
        if (ft_net_is_ncs_supported(ncs)){
            if (ncs->info->type == NET_CLIENT_DRIVER_TAP) {
                /* wait for nic, just continue */
                continue;
            }
            /* disable vhost, see comment of ft_disable_ncs_vhost() */
            if (get_vhost_net(ncs->peer)) {
                warn_report("vhost-net detected, and it is not compatible with FT. "
                    "Stopping vhost-net and use qemu virtio backend instead on this VM.");
                ft_disable_ncs_vhost(ncs);
            }

            /* create new NICsQueue node */
            nicq = g_malloc(sizeof(NICsQueue));
            nicq->backend_nq = ncs->peer->incoming_queue;
            nicq->backend = (NetPacketHead*)(&nicq->backend_nq->packets);
            /* create queue head for each VM NIC */
            nicq->ft_tx_queue = g_malloc(sizeof(NetPacketHead));
            QTAILQ_INIT(nicq->ft_tx_queue);
            nicq->ft_tx_out_queue = g_malloc(sizeof(NetPacketHead));
            QTAILQ_INIT(nicq->ft_tx_out_queue);

            /* add new node to NICsQueue list */
            QTAILQ_INSERT_TAIL(nics, nicq, entry);

            /* quickly pointed to NICsQueue when filted net packet */
            ncs->ft_net = nicq;
        }
        else {
            /* not supported: not TAP or NIC */
            goto error;
        }
    }
    if (QTAILQ_EMPTY(nics)) {
        /* No NIC is installed */
        return false;
    }

    /* let QEMU Main thread (IOThread) to flush net tx packet */
    ft_net_flush_tx_bh = qemu_bh_new(qemu_net_queue_flush_bh, NULL);

    return true;

error:
    ft_set_status(FT_STATUS_INTERNAL_ERROR,
                "VM config changed during FT initialization, "
                "please review VM config for compability and start FT again.");
    return false;
}

// Make FT NetClientStates support info avaiable before live migration
bool ft_net_is_supported(void) {
    cs = checkpointing_get_current();
    /* Any NIC/network looks good if no tx buffering is required */
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_TX_BUFFERING] == false)
    {
        return true;
    }

    NetClientStateHead *net_clients = get_net_clients_list_head();
    NetClientState *ncs;
    uint32_t support_ncs_count = 0;
    QTAILQ_FOREACH(ncs, net_clients, next) {
        if (!ft_net_is_ncs_supported(ncs))
        {
            /* error msg has been handled in ft_net_is_ncs_supported() */
            return false;
        }
        ++support_ncs_count;
    }
    if (support_ncs_count == 0) {
        ft_set_status(FT_STATUS_INTERNAL_ERROR,
                "No NIC(s) is installed in this VM, FT will not able to start.");
        return false;
    }
    return true;
}

_Bool ft_tx_queue_is_empty(void) {
    return atomic_read(&tx_packets_total_length) == 0;
}
