#include "qemu/osdep.h"
#include "qemu/units.h"
#include "sysemu/sysemu.h"
#include "qapi/error.h"
#include "migration.h"
#include "migration/ft.h"
#include "qemu-file-channel.h"
#include "qemu-file.h"
#include "savevm.h"
#include "io/channel-buffer.h"
#include "trace.h"
#include "qemu/error-report.h"
#include "migration/lz4.h"
#include "migration/zstd.h"

#define FT_MSG_GOTO_ERROROUT_ON(condition) \
        if (unlikely(condition)) { goto error_out; }
#define FT_RETURN_ON_QEMUFILE_INVALID() \
        if (unlikely(!to_peer_file || !from_peer_file)) \
        { error_report("%s: !QEMUFile", __func__); return -EINVAL; } \
        if (unlikely(!qemu_file_is_writable(to_peer_file)) || \
            unlikely(qemu_file_is_writable(from_peer_file)) ) \
        { error_report("%s: (X)writable", __func__); return -EINVAL; }

static QEMUFile *to_peer_file = NULL;
static QEMUFile *from_peer_file = NULL;
static CheckpointingState *cs = NULL;

/* connectivity liveness */
static bool messaging_active = false;
static inline void ft_mark_messaging_active(void);
static QemuMutex ft_message_mutex;
static QemuSemaphore reverse_keepalive_request;
static QemuSemaphore reverse_keepalive_finish_request;
static QemuSemaphore reverse_keepalive_finished;
static QemuThread ft_reverse_keepalive_thread;
static int ft_reverse_keepalive_thread_ret = 0;
static bool ft_reverse_keepalive_thread_started = false;
static bool ft_reverse_keepalive_thread_should_stop = false;

/* ft_send_buffer will split the buffer to be sent, and the size will be used for
 * watchdog to determine aliveness. */
static size_t buffer_segment_bytes = 1 * MiB;

static int ft_send_buffer_compressed(uint8_t *buf, size_t size, FTRecordSendType type);

static size_t ft_receive_buffer_compressed(uint8_t *buf, size_t size);
static void ft_free_segments(void);

enum FTCompressImpl {
    FT_NO_COMPRESS = 0,
    FT_LZ4_FAST_COMPRESS = 1, FT_LZ4_SAFE_DECOMPRESS = 2,
    FT_ZSTD_FAST_COMPRESS = 3, FT_ZSTD_DECOMPRESS = 4,
    FT_COMPRESS_IMPL__MAX
};
typedef struct {
    size_t current_buffer_size;
    size_t prev_buffer_size;

    /* Allocated segment count */
    unsigned int alloc_segments;
    /* Current segment count in use */
    unsigned int segments;
    /* Max CPU threads allowed to use */
    unsigned int max_threads;
    unsigned int mode;
    unsigned char *comp_buffer;
    GThreadPool *comp_pool;
} CompressState;

typedef struct {
    unsigned char *buffer_src_start;
    unsigned char *buffer_dst_start;
    /* extState, or explicit context for compressor to reuse */
    unsigned char *extState;
    size_t src_size;
    size_t buf_size;
    size_t out_size;
} CompressSegmentState;

static QemuSemaphore ft_compression_threads_sem;
/* Global FT compression states */
static CompressState *comp_stat = NULL;
/* Segment states of all threads */
static CompressSegmentState *seg_stats = NULL;
static void ft_compress_worker_func(gpointer seg_state, gpointer comp_state);
static bool ft_update_comp_buffer_size(size_t new_size);
static bool ft_update_segment_size(size_t new_size);
static size_t get_bound_size(size_t req_size);
//ft send buffer
int ft_send_message(FTMessage msg, Error **errp) {
    FT_RETURN_ON_QEMUFILE_INVALID();
    if (msg >= FT_MESSAGE__MAX) {
        error_setg(errp, "%s: Argument msg invalid", __func__);
        return -EINVAL;
    }

#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: %s", __func__, FTMessage_str(msg));
#endif
    qemu_put_be32(to_peer_file, msg);
    qemu_fflush(to_peer_file);

    int ret = qemu_file_get_error(to_peer_file);
    if (ret != 0) {
        error_setg_errno(errp, -ret, "Can't send FT message");
        return ret;
    }
    ft_mark_messaging_active();
    return 0;
}

int ft_send_message_and_value(FTMessage msg, uint64_t value, Error **errp) {
    Error *local_err = NULL;
    int ret;

    ret = ft_send_message(msg, &local_err);
    if (local_err) {
        error_propagate(errp, local_err);
        return ret;
    }
    if (ret != 0) {
        return ret;
    }

    qemu_put_be64(to_peer_file, value);
    qemu_fflush(to_peer_file);

    ret = qemu_file_get_error(to_peer_file);
    if (ret != 0) {
        error_setg_errno(errp, -ret, "Failed to send value for message:%s",
                         FTMessage_str(msg));
    }
    ft_mark_messaging_active();
    return ret;
}

int ft_send_value(uint64_t value, Error **errp) {
    FT_RETURN_ON_QEMUFILE_INVALID();
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: %lu", __func__, value);
#endif
    qemu_put_be64(to_peer_file, value);
    qemu_fflush(to_peer_file);

    int ret = qemu_file_get_error(to_peer_file);
    if (ret != 0) {
        error_setg_errno(errp, -ret, "Failed to send value");
    }
    ft_mark_messaging_active();
    return ret;
}

int ft_send_buffer(const uint8_t *buf, size_t size) {
    FT_RETURN_ON_QEMUFILE_INVALID();
    size_t pending_bytes = size;
    size_t next_send_bytes = buffer_segment_bytes;
    int ret = 0;
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: begin, size=%lu", __func__, size);
#endif

    while (pending_bytes > 0) {
        if (pending_bytes < buffer_segment_bytes) {
            next_send_bytes = pending_bytes;
        }
        qemu_put_buffer(to_peer_file, buf, next_send_bytes);//把buffer資料放到backup(QF)
        qemu_fflush(to_peer_file);
        ret = qemu_file_get_error(to_peer_file);
        if (ret != 0) {
            error_report("%s: sent %lu of %lu, error: %s", __func__,
                         size - pending_bytes, size , strerror(-ret));
            break;
        }

        pending_bytes -= next_send_bytes;
        buf += next_send_bytes;
        ft_mark_messaging_active();
    }
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: finish, size=%lu", __func__, size);
#endif
    return ret;
}

/* ft message must be locked before calling this function, and it must
 * remain locked after exiting this function no matter success or not */
int ft_send_buffer_compressed(uint8_t *buf, size_t size,
                              FTRecordSendType type) {
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: begin, size=%lu", __func__, size);
#endif
    Error *local_err = NULL;
    int ret;
    unsigned int segments = comp_stat->max_threads;
    /* make sure size of every segment is < 1 GiB.
     * some compressor (e.g. lz4) does not accept huge input */
    while (size / segments >= 1 * GiB) {
        segments *= 2;
    }

    ret = ft_send_value(segments, &local_err);
    if (unlikely(local_err || ret != 0)) {
        error_report("%s: fail to send seg val", __func__);
        goto error_out;
    }

    /* unlock message to let keep-alive available;
     * it's safe since receiver will be waiting for message and value here.
     * Backup will re-allocate buffer if needed in this period. */
    ft_message_unlock();

    if (!ft_update_segment_size(segments)) {
        ft_message_lock();
        error_report("%s: ft_update_segment_size(%u) failed",
                     __func__, segments);
        return -ENOMEM;
    }

    size_t quotient = size / segments;
    size_t remainder =  size % segments;
    /* In the worst case, compressed buffer will be bigger than the source;
     * the offset of dst will be slightly bigger than src. */
    size_t src_offset = 0;
    size_t dst_offset = 0;
    int64_t comp_begin, comp_finish;

    /* resize buffer to the desired size of compressor before push works
     * to the pool, or will cause use-after-free (dangling pointers).
     * some compressor might return 0 bound size when single segment size is
     * too large (lz4@2GB), so divide the size first then multiply it back */
    size_t target_buf_size = get_bound_size(size / segments) * segments;
    if (!ft_update_comp_buffer_size(target_buf_size)) {
        ft_message_lock();
        error_report("failing update buffer to %lu bytes", target_buf_size);
        return -ENOMEM;
    }

    /* calculate addr per work then push it to the pool (non-blocking) */
    for (unsigned int i = 0; i < segments; ++i) {
        src_offset = i * quotient;
        seg_stats[i].buffer_src_start = buf + src_offset;
        seg_stats[i].buffer_dst_start = comp_stat->comp_buffer + dst_offset;
        /* Put the remain buffer into the last segment */
        seg_stats[i].src_size = (i == segments - 1) ? quotient+remainder : quotient;
        seg_stats[i].buf_size = get_bound_size(seg_stats[i].src_size);
        dst_offset += seg_stats[i].buf_size;
    }

    comp_begin = qemu_clock_get_ms(QEMU_CLOCK_HOST);
    for (unsigned int i = 0; i < segments; ++i) {
        ret = g_thread_pool_push(comp_stat->comp_pool, &seg_stats[i], NULL);
        if (unlikely(ret != true))
        {
            ft_message_lock();
            error_report("%s: g_thread_pool_push() failed", __func__);
            return -ENOMEM;
        }
    }

    /* wait backup host which is allocating decompress buffer */
    if (target_buf_size > comp_stat->prev_buffer_size) {
        ft_message_lock();
        ft_receive_and_check_message(FT_MESSAGE_JOBS_DONE, &local_err);
        ft_message_unlock();
    }

    /* wait for all compression tasks done. */
    for (unsigned int i = 0; i < segments; ++i) {
        qemu_sem_wait(&ft_compression_threads_sem);
    }
    comp_finish = qemu_clock_get_ms(QEMU_CLOCK_HOST);

    /* regain message lock and send the compressed buffer */
    ft_message_lock();

    /* KeepAlives will be sent by watchdog thread during compression period.
     * This message makes sure Backup can receive the KeepAlive message correctly,
     * instead of receiving it as a value and occasionally break the protocol. */
    ret = ft_send_message(FT_MESSAGE_JOBS_DONE, &local_err);
    FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);

    size_t total_compressed_size = 0;
    for (unsigned int i = 0; i < segments; ++i) {
        if (seg_stats[i].out_size == 0) {
            error_report("Compress failed, in_size=%lu,buf_size=%lu,bound=%lu",
                        seg_stats[i].src_size, seg_stats[i].buf_size,
                        get_bound_size(seg_stats[i].src_size));
            return -EPERM;
        }
        ret = ft_send_value(seg_stats[i].out_size, &local_err);
        FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);

        ret = ft_send_buffer(seg_stats[i].buffer_dst_start, seg_stats[i].out_size);
        FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);

        total_compressed_size += (size_t)seg_stats[i].out_size;
    }

#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: finish, size=%lu", __func__, size);
#endif
    ft_autopilot_fill_send_type_data(type, comp_finish - comp_begin,
                                     total_compressed_size);
    return 0;

error_out:
    if (local_err) {
        error_report_err(local_err);
    }
    if (local_err && ret == 0) {
        error_report("%s: error happened but ret is 0", __func__);
        ret = -ENOTSUP;
    }
    return ret;
}

/* Selectively compress and send the buffer by autopilot and HMP settings,
 * Both sender/receiver should use *_adaptively functions if compression is needed.
 * Pass corresponding FTRecord item to get compress time and size data.
 * Return false if a local_err appears. **NOT** thread-safe.
 */
int ft_send_buffer_adapt(uint8_t *buf, size_t size, FTRecordSendType type)
{
    Error *local_err = NULL;
    int ret;

    if (comp_stat->mode >= FT_COMPRESS_IMPL__MAX) {
        error_report("[Checkpointing] unknown compression impl %d", comp_stat->mode);
        return -ENOTSUP;
    }

    /* send buffer directly if compression not needed */
    if (comp_stat->mode == FT_NO_COMPRESS ||
        !ft_autopilot_should_do_compress(type, size)) {
        ret = ft_send_message(FT_MESSAGE_BUFFER_NORMAL, &local_err);
        FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);

        ret = ft_send_buffer(buf, size);
        FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);

        return ret;
    }
    ret = ft_send_message(FT_MESSAGE_BUFFER_COMPRESSED, &local_err);
    FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);

    ret = ft_send_buffer_compressed(buf, size, type);
    if (unlikely(ret != 0)) {
        goto error_out;
    }
    return ret;

error_out: ;
    error_report("%s: %s buf sending failed because %s\n",
        __func__, FTRecordSendType_str(type),
        (local_err)? error_get_pretty(local_err) : strerror(-ret));
    return ret;
}

/* Send the type and size going to be sent after this function call to peer.
 * When Watchdog is enabled and prev_max > size, the function will wait
 * for the peer to complete memory allocation job.
 * prev_max should be the max buffer size has been sent in this session,
 * and being maintained by main flow. Ensure having same value on both hosts. */
int ft_send_size_and_wait_malloc
        (FTMessage type, size_t size, size_t prev_max, Error **errp) {
    int ret;
    ret = ft_send_message_and_value(type, size, errp);
    FT_MSG_GOTO_ERROROUT_ON(*errp || ret != 0);

    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_WATCHDOG] && size > prev_max) {
        ret = ft_receive_and_check_message(FT_MESSAGE_JOBS_DONE, errp);
    }

error_out:
    return ret;
}

int ft_receive_message(FTMessage *to_recv, Error **errp) {
    FT_RETURN_ON_QEMUFILE_INVALID();
    FTMessage msg = FT_MESSAGE_KEEP_ALIVE;
    int ret;
    /* If keep-alive message received, receive message from QEMUFile again. */
    while (msg == FT_MESSAGE_KEEP_ALIVE) {
        msg = qemu_get_be32(from_peer_file);
        ret = qemu_file_get_error(from_peer_file);
        if (ret != 0) {
            error_setg_errno(errp, -ret, "Can't receive FT message");
            return ret;
        }
        if (msg >= FT_MESSAGE__MAX) {
            error_setg(errp, "%s: Received invalid message - %d", __func__, msg);
            return -EBADMSG;
        }
        /* set TCP_QUICKACK to prevent watchdog timeout by delayed ACKs;
         * TCP_QUICKACK will be reset automatically, see `man tcp` */
        if (msg == FT_MESSAGE_KEEP_ALIVE &&
            cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_WATCHDOG] &&
            cs->watchdog_stage_interval < DEFAULT_FT_WATCHDOG_STAGE_INTERVAL) {
            qemu_file_qio_socket_set_quickack(from_peer_file);
        }
        ft_mark_messaging_active();
        #ifdef FT_DEBUG_MESSAGE
        info_report("[ft-msg] %s: %s", __func__, FTMessage_str(msg));
        #endif
    }

    *to_recv = msg;
    return 0;
}

int ft_receive_and_check_message(FTMessage expect_msg, Error **errp) {
    Error *local_err = NULL;
    int ret;
    FTMessage msg = FT_MESSAGE__MAX;
    ret = ft_receive_message(&msg, &local_err);
    if (local_err || ret != 0) {
        error_report("%s: cannot receive expected FT Message %s",
                     __func__, FTMessage_str(expect_msg));
        error_propagate(errp, local_err);
        return ret;
    }
    if (msg != expect_msg) {
        error_setg(errp, "%s: Unexpected FT message %s(%d), expected %s(%d)", __func__,
                   FTMessage_str(msg), msg, FTMessage_str(expect_msg), expect_msg);
        return -EBADMSG;
    }
    ft_mark_messaging_active();
    return ret;
}

int ft_receive_message_and_value
        (FTMessage expect_msg, uint64_t *to_recv, Error **errp) {
    Error *local_err = NULL;
    int ret;
    ret = ft_receive_and_check_message(expect_msg, &local_err);
    FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);

    uint64_t value = 0;
    ret = ft_receive_value(&value, &local_err);
    FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);

    ft_mark_messaging_active();
    *to_recv = value;

    return ret;
error_out:
    if (local_err) {
        error_propagate(errp, local_err);
    } else {
        error_setg_errno(errp, -ret, "Failed to get value for "
                         "FT message: %s", FTMessage_str(expect_msg));
    }
    return ret;
}

int ft_receive_value(uint64_t *to_recv, Error **errp) {
    FT_RETURN_ON_QEMUFILE_INVALID();
    uint64_t value = qemu_get_be64(from_peer_file);
    int ret = qemu_file_get_error(from_peer_file);
    if (ret != 0) {
        error_setg_errno(errp, -ret, "Failed to get value");
    }
    ft_mark_messaging_active();
    *to_recv = value;
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: %lu", __func__, value);
#endif
    return ret;
}

int ft_receive_buffer(uint8_t *buf, size_t size) {
    FT_RETURN_ON_QEMUFILE_INVALID();
    size_t pending_bytes = size;
    size_t expected_recv_bytes = buffer_segment_bytes;
    size_t current_received_bytes;
    int ret = 0;

#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: begin, size=%lu", __func__, size);
#endif

    while (pending_bytes > 0) {
        if (pending_bytes < buffer_segment_bytes) {
            expected_recv_bytes = pending_bytes;
        }
        current_received_bytes = qemu_get_buffer(from_peer_file, buf, expected_recv_bytes);
        if (current_received_bytes != expected_recv_bytes) {
            ret = qemu_file_get_error(from_peer_file);
            error_report("%s: received %lu of %lu, error: %s\n", __func__,
                 current_received_bytes, expected_recv_bytes, strerror(-ret));
            break;
        }
        pending_bytes -= current_received_bytes;
        buf += current_received_bytes;
        ft_mark_messaging_active();
    }
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: finish, size=%lu", __func__, size - pending_bytes);
#endif
    if (pending_bytes > 0 && ret == 0){
        ret = -EIO;
    }
    return ret;
}

size_t ft_receive_buffer_compressed(uint8_t *buf, size_t size) {
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: begin, size=%lu", __func__, size);
#endif
    Error *local_err = NULL;
    int ret = 0;
    uint64_t segments = 0;

    ret = ft_receive_value(&segments, &local_err);
    FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);
    if (segments == 0) {
        error_report("%s: invalid segment count %lu", __func__, segments);
        return -EINVAL;
    }

    size_t target_buf_size = get_bound_size(size / segments) * segments;
    /* do reverse keepalive if decompress buffer allocation is needed.
     * ft_update_comp_buffer_size() will change current_buffer_size and
     * prev_buffer_size in comp_stat. */
    if (target_buf_size > comp_stat->current_buffer_size) {
        ft_reverse_keepalive_start();
    }
    ft_update_segment_size(segments);
    if (!ft_update_comp_buffer_size(target_buf_size)) {
        error_report("%s: compression buffer update failed (OOM?)", __func__);
        return -ENOMEM;
    }
    if (target_buf_size > comp_stat->prev_buffer_size) {
        ft_reverse_keepalive_finish();
        ret = ft_send_message(FT_MESSAGE_JOBS_DONE, &local_err);
        FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);
    }

    /* This message prevent keep-alives from breaking the protocol */
    ret = ft_receive_and_check_message(FT_MESSAGE_JOBS_DONE, &local_err);
    FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);

    /* build decompression information */
    size_t quotient = size / segments;
    size_t remainder =  size % segments;
    size_t src_offset = 0;
    size_t dst_offset = 0;

    for (uint64_t i = 0; i < segments; ++i) {
        dst_offset = i * quotient;
        seg_stats[i].buffer_src_start = comp_stat->comp_buffer + src_offset;
        seg_stats[i].buffer_dst_start = buf + dst_offset;
        ret = ft_receive_value(&seg_stats[i].src_size, &local_err);
        FT_MSG_GOTO_ERROROUT_ON(local_err || ret != 0);

        seg_stats[i].buf_size = (i == segments - 1) ? quotient+remainder : quotient;
        ret = ft_receive_buffer(seg_stats[i].buffer_src_start,
                                seg_stats[i].src_size);
        FT_MSG_GOTO_ERROROUT_ON(ret != 0);

        if (g_thread_pool_push(comp_stat->comp_pool, &seg_stats[i], NULL) != true) {
            error_report("%s: g_thread_pool_push() failed", __func__);
            return -ENOMEM;
        }
        src_offset += seg_stats[i].src_size;
    }

    /* wait for all compression tasks done. */
    for (unsigned int i = 0; i < segments; ++i) {
        qemu_sem_wait(&ft_compression_threads_sem);
    }
    for (unsigned int i = 0; i < segments; ++i) {
        if (seg_stats[i].out_size != seg_stats[i].buf_size) {
            error_report("%s: decomp failed seg=%u,src_size=%lu,out_size=%lu,expected=%lu",
                __func__, i, seg_stats[i].src_size, seg_stats[i].out_size, seg_stats[i].buf_size);
            return -EINVAL;
        }
    }
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] %s: finish, size=%lu", __func__, size);
#endif
error_out:
    if (local_err) {
        error_report_err(local_err);
    }
    return ret;
}

int ft_receive_buffer_adapt(uint8_t *buf, size_t size) {
    Error *local_err = NULL;
    FTMessage msg = FT_MESSAGE__MAX;
    int ret;

    ret = ft_receive_message(&msg, &local_err);
    if (unlikely(local_err)) {
        return ret;
    }

    switch (msg) {
    case FT_MESSAGE_BUFFER_NORMAL:
        return ft_receive_buffer(buf, size);

    case FT_MESSAGE_BUFFER_COMPRESSED:
        return ft_receive_buffer_compressed(buf, size);

    default:
        error_report("[Checkpointing] %s: get unknown msg=%d", __func__, msg);
        return -ENOTSUP;
    }
}

int ft_receive_size_and_malloc(size_t *to_recv, FTMessage type, size_t prev_max,
            void *qio_channel_buffer, Error **errp) {
    uint64_t value;
    int ret;
    QIOChannelBuffer *qioc_buf = qio_channel_buffer;

    ret = ft_receive_message_and_value(type, &value, errp);
    FT_MSG_GOTO_ERROROUT_ON(*errp || ret != 0);
    *to_recv = value;
    /* no need to do memory alloc if the size is already big enough */
    if (value <= prev_max) {
        return 0;
    }

    ft_reverse_keepalive_start();
    ret = ft_discard_resize_qio_channel_buffer(value, qioc_buf);
    FT_MSG_GOTO_ERROROUT_ON(ret != 0);
    ret = ft_reverse_keepalive_finish();
    FT_MSG_GOTO_ERROROUT_ON(ret != 0);

    if (!qioc_buf->data) {
        error_setg(errp, "%s: Resize QIOChannel buffer failed", __func__);
        return -ENOMEM;
    }

    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_WATCHDOG]) {
        ret = ft_send_message(FT_MESSAGE_JOBS_DONE, errp);
        FT_MSG_GOTO_ERROROUT_ON(*errp || ret != 0);
    }

error_out:
    return ret;
}

// Return active state since last call and reset the active state to false
bool ft_messaging_is_active(void) {
    bool active = atomic_read(&messaging_active);
    atomic_set(&messaging_active, false);
    return active;
}

// Set active state at end of every send/received function inline.
static inline void ft_mark_messaging_active(void) {
    atomic_set(&messaging_active, true);
}

void ft_message_lock(void) {
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] lock");
#endif
    qemu_mutex_lock(&ft_message_mutex);
}

void ft_message_unlock(void) {
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] unlock");
#endif
    qemu_mutex_unlock(&ft_message_mutex);
}

int ft_message_trylock(void) {
#ifdef FT_DEBUG_MESSAGE
    info_report("[ft-msg] trylock");
#endif
    return qemu_mutex_trylock(&ft_message_mutex);
}

bool ft_compression_init(FTCompressor compressor) {
    /* initialize all value to 0 */
    comp_stat = calloc(1, sizeof(CompressState));
    if (!comp_stat) {
        error_report("[Checkpointing] FT Compression init failed because OOM.");
        return false;
    }

    if (compressor == FT_COMPRESSOR_NONE || compressor >= FT_COMPRESSOR__MAX) {
        info_report("[Checkpointing] Compression is disabled, or the settings"
            " are invalid. mode=%s", FTCompressor_str(compressor));
        return true;
    }

    comp_stat->max_threads =
                migrate_get_current()->parameters.ft_compress_threads;
    info_report("[Checkpointing] initializing compressor %s with %u threads",
                FTCompressor_str(compressor), comp_stat->max_threads);

    /* set corresponding compressor and decompressor by FTRole */
    switch (ft_get_assigned_role()) {
    case FT_ROLE_PRIMARY:
        switch (compressor) {
        case FT_COMPRESSOR_LZ4_FAST:
            comp_stat->mode = FT_LZ4_FAST_COMPRESS;
            break;
        case FT_COMPRESSOR_ZSTD_FAST:
            comp_stat->mode = FT_ZSTD_FAST_COMPRESS;
            break;
        default:
            goto fail;
        }
        break;
    case FT_ROLE_BACKUP:
        switch (compressor) {
        case FT_COMPRESSOR_LZ4_FAST:
            comp_stat->mode = FT_LZ4_SAFE_DECOMPRESS;
            break;
        case FT_COMPRESSOR_ZSTD_FAST:
            comp_stat->mode = FT_ZSTD_DECOMPRESS;
            break;
        default:
            goto fail;
        }
        break;
    default:
        goto fail;
    }

    /* Pre-allocate ram_size/4, as same as ft_checkpoint_loop() */
    if (!ft_update_segment_size(comp_stat->max_threads) ||
        !ft_update_comp_buffer_size(ram_size / 4)) {
        error_report("[Checkpointing] Failed to init compression: OOM");
        goto fail;
    }

    comp_stat->comp_pool = g_thread_pool_new(ft_compress_worker_func,
                                comp_stat, comp_stat->max_threads, TRUE, NULL);
    if (g_thread_pool_get_num_threads(comp_stat->comp_pool)
                                    != comp_stat->max_threads) {
        error_report("[Checkpointing] Failed to init compression: thread pool");
        goto fail;
    }
    /* always keep the idle threads */
    g_thread_pool_set_max_idle_time(0);

    qemu_sem_init(&ft_compression_threads_sem, 0);
    return true;

fail:
    ft_compression_cleanup(false);
    return false;
}

void ft_free_segments(void)
{
    if (!seg_stats || !comp_stat) {
        seg_stats = NULL;
        return;
    }
    for (int i = 0; i < comp_stat->segments; ++i) {
        if (!(seg_stats + i)) {
            continue;
        }
        switch (comp_stat->mode) {
        case FT_LZ4_FAST_COMPRESS:
            free(seg_stats[i].extState);
            break;
        case FT_ZSTD_FAST_COMPRESS:
            ZSTD_freeCCtx((ZSTD_CCtx *)seg_stats[i].extState);
            break;
        case FT_ZSTD_DECOMPRESS:
            ZSTD_freeDCtx((ZSTD_DCtx *)seg_stats[i].extState);
            break;
        case FT_LZ4_SAFE_DECOMPRESS:
            /* fallthrough: LZ4 decompress do not have extState */
        default:
            /* nothing to do if nothing allocated */
            break;
        }
        seg_stats[i].extState = NULL;
    }
    free(seg_stats);
    seg_stats = NULL;
}

/* free comp_stat only when final is true to keep current mode accessible */
void ft_compression_cleanup(bool final) {
    if (!comp_stat) {
        return;
    }
    comp_stat->mode = FT_NO_COMPRESS;

    ft_free_segments();
    if (comp_stat->comp_buffer) {
        free(comp_stat->comp_buffer);
        comp_stat->comp_buffer = NULL;
    }
    if (comp_stat->comp_pool) {
        g_thread_pool_free(comp_stat->comp_pool, TRUE, FALSE);
        comp_stat->comp_pool = NULL;
    }
    if (ft_compression_threads_sem.initialized) {
        qemu_sem_destroy(&ft_compression_threads_sem);
    }
    /* comp_stat must be free lastly (e.g. takeover) */
    if (final) {
        free(comp_stat);
        comp_stat = NULL;
    }
}

void ft_compress_worker_func(gpointer seg_state, gpointer comp_state) {
    CompressState *comp_stat = comp_state;
    CompressSegmentState *seg_stat = seg_state;

    int lz4_ret = 0;
    switch (comp_stat->mode) {
    case FT_LZ4_FAST_COMPRESS:
        /* higher lz4 accel value seems not giving better compression ratio. */
        lz4_ret = LZ4_compress_fast_extState((void *)seg_stat->extState,
                                    (char *)seg_stat->buffer_src_start,
                                    (char *)seg_stat->buffer_dst_start,
                                    seg_stat->src_size, seg_stat->buf_size, 1);
        break;
    case FT_LZ4_SAFE_DECOMPRESS:
        lz4_ret = LZ4_decompress_safe((char *)seg_stat->buffer_src_start,
                                    (char *)seg_stat->buffer_dst_start,
                                    seg_stat->src_size, seg_stat->buf_size);
        break;
    case FT_ZSTD_FAST_COMPRESS:
        seg_stat->out_size =
                  ZSTD_compressCCtx((ZSTD_CCtx *)seg_stat->extState,
                        (void *)seg_stat->buffer_dst_start, seg_stat->buf_size,
                        (void *)seg_stat->buffer_src_start, seg_stat->src_size,
                        3);
        break;
    case FT_ZSTD_DECOMPRESS:
        seg_stat->out_size =
                  ZSTD_decompressDCtx((ZSTD_DCtx *)seg_stat->extState,
                        (void *)seg_stat->buffer_dst_start, seg_stat->buf_size,
                        (void *)seg_stat->buffer_src_start, seg_stat->src_size);
        break;
    default:
        error_report("Compression subsystem is in unknown mode\n");
        qemu_sem_post(&ft_compression_threads_sem);
        return;
    }

    /* check for errors and set out_size if needed */
    switch (comp_stat->mode) {
    case FT_LZ4_FAST_COMPRESS:
    case FT_LZ4_SAFE_DECOMPRESS:
        seg_stat->out_size = (size_t)lz4_ret;
        if (lz4_ret < 0) {
            error_report("lz4 mode=%d,error_addr=%d,src_size=%lu,buf_size=%lu,src_start=%lx,"
                         "buf_start=%lx", comp_stat->mode, lz4_ret, seg_stat->src_size,
                         seg_stat->buf_size, (uintptr_t)seg_stat->buffer_src_start,
                         (uintptr_t)seg_stat->buffer_dst_start);
            seg_stat->out_size = 0;
        }
        break;
    case FT_ZSTD_FAST_COMPRESS:
    case FT_ZSTD_DECOMPRESS:
        if (ZSTD_isError(seg_stat->out_size)) {
            error_report("zstd mode=%d,src_size=%lu,buf_size=%lu,src_start=%lx,"
                         "buf_start=%lx", comp_stat->mode, seg_stat->src_size,
                          seg_stat->buf_size, (uintptr_t)seg_stat->buffer_src_start,
                          (uintptr_t)seg_stat->buffer_dst_start);
            seg_stat->out_size = 0;
        }
        break;
    default:
        error_report("Compression done but subsystem is in unknown mode\n");
    }

    qemu_sem_post(&ft_compression_threads_sem);
}

bool ft_update_comp_buffer_size(size_t new_size) {
    if (!comp_stat) {
        return false;
    }
    if (new_size <= comp_stat->current_buffer_size) {
        comp_stat->prev_buffer_size = comp_stat->current_buffer_size;
        return true;
    }
    if (comp_stat->comp_buffer) {
        free(comp_stat->comp_buffer);
    }
    /* Allocate slightly more (128 MB) to prevent reallocation frequently */
    size_t new_buf_size = new_size + (128 * MiB);
    comp_stat->comp_buffer = malloc(new_buf_size);
    if (!comp_stat->comp_buffer) {
        return false;
    }
    comp_stat->prev_buffer_size = comp_stat->current_buffer_size;
    comp_stat->current_buffer_size = new_buf_size;
    return true;
}

/* Re-create seg_stats if current segments is smaller. */
bool ft_update_segment_size(size_t new_size) {
    if (comp_stat->alloc_segments >= new_size) {
        goto complete;
    }

    /* free seg_stats and the extStates inside */
    if (seg_stats) {
        ft_free_segments();
    }
    seg_stats = calloc(new_size, sizeof(CompressSegmentState));
    if (!seg_stats) {
        return false;
    }

    /* init ext states for each compressor/decompressor */
    for (int i = 0; i < new_size; ++i)
    {
        /* If extState alloc needed, check whether the alloc is successful */
        bool need_alloc_check = true;

        switch (comp_stat->mode) {
        case FT_LZ4_FAST_COMPRESS:
            seg_stats[i].extState =
                    (unsigned char *)malloc(LZ4_sizeofState());
            break;
        case FT_ZSTD_FAST_COMPRESS:
            seg_stats[i].extState = (unsigned char *)ZSTD_createCCtx();
            break;
        case FT_ZSTD_DECOMPRESS:
            seg_stats[i].extState = (unsigned char *)ZSTD_createDCtx();
            break;
        case FT_LZ4_SAFE_DECOMPRESS:
            /* fallthrough: LZ4 decompress do not have extState*/
        default:
            seg_stats[i].extState = NULL;
            need_alloc_check = false;
            break;
        }
        if (need_alloc_check && !seg_stats[i].extState) {
            return false;
        }
    }
    comp_stat->alloc_segments = new_size;

complete:
    comp_stat->segments = new_size;
    return true;
}

/* Get the required buffer bound size of current compressor. */
size_t get_bound_size(size_t req_size)
{
    switch (comp_stat->mode) {
    case FT_LZ4_FAST_COMPRESS: /* fallthrough */
    case FT_LZ4_SAFE_DECOMPRESS:
        return (size_t)LZ4_compressBound(req_size);

    case FT_ZSTD_FAST_COMPRESS: /* fallthrough */
    case FT_ZSTD_DECOMPRESS:
        return ZSTD_compressBound(req_size);

    default:
        /* shouldn't arrive here */
        error_report("compressor bound size requested but no mode set");
        return req_size;
    }
}

static void *ft_reverse_keepalive_func(void *opaque) {
    CheckpointingState *cs = checkpointing_get_current();
    Error *local_err = NULL;
    struct timespec current_time;
    /* convert timespec nanoseconds to milliseconds */
    int64_t iter_begin_ms = 0, current_ms = 0;

    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_WATCHDOG] == false) {
        info_report("[Checkpointing] Watchdog disabled, so disabling reverse keepalive.");
        return NULL;
    }

    atomic_set(&ft_reverse_keepalive_thread_started, true);
    while (true) {
        qemu_sem_wait(&reverse_keepalive_request);
        if (atomic_read(&ft_reverse_keepalive_thread_should_stop) == true) {
            break;
        }

        while (qemu_sem_timedwait(&reverse_keepalive_finish_request, 1) != 0) {
            clock_gettime(CLOCK_MONOTONIC, &current_time);
            current_ms = current_time.tv_sec * 1000 + (current_time.tv_nsec / 1000000l);
            if (current_ms - iter_begin_ms < cs->watchdog_stage_interval) {
                continue;
            }

            ft_reverse_keepalive_thread_ret =
                    ft_send_message(FT_MESSAGE_KEEP_ALIVE, &local_err);
            if (local_err || ft_reverse_keepalive_thread_ret != 0)
            {
                error_report("%s: ft_send_message failed with ret=%d - %s",
                             __func__, ft_reverse_keepalive_thread_ret,
                             strerror(ft_reverse_keepalive_thread_ret));
                break;
            }
            iter_begin_ms = current_ms;
        }
        qemu_sem_post(&reverse_keepalive_finished);
    }

    if (local_err && ft_reverse_keepalive_thread_ret == 0) {
        ft_reverse_keepalive_thread_ret = -ENOMSG;
    }
    if (local_err) {
        error_report_err(local_err);
    }
    atomic_set(&ft_reverse_keepalive_thread_started, false);
    return NULL;
}

/* Start a thread sending keepalive to the QEMUfile.
 * Message lock must be granted before calling this function, and any
 * FTMessage sending or receiving must not be called before calling
 * ft_reverse_keepalive_finish().
 * Make sure Primary is receiving messages instead of sending, or the
 * messaging order would be broken. */
void ft_reverse_keepalive_start(void) {
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_WATCHDOG] == false) {
        return;
    }
    if (unlikely(atomic_read(&ft_reverse_keepalive_thread_started) == false)) {
        warn_report("%s: reverse keepalive exited but reverse keepalive "
                    "requested", __func__);
        return;
    }
    qemu_sem_post(&reverse_keepalive_request);
}

/* Stop the thread sending keepalive to the QEMUfile.
 * After this funtion returned, a ft_send_message() must be called to
 * wake the peer up.
 * Note: Calling this function might be blocked by posting internal
 *       semaphore and thread join; NOT MT-Safe. */
int ft_reverse_keepalive_finish(void) {
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_WATCHDOG] == false) {
        return 0;
    }
    if (atomic_read(&ft_reverse_keepalive_thread_started) == true) {
        qemu_sem_post(&reverse_keepalive_finish_request);
    }
    qemu_sem_wait(&reverse_keepalive_finished);
    return ft_reverse_keepalive_thread_ret;
}

int ft_message_init(QEMUFile *to_peer, QEMUFile *from_peer, void *chkpt_state) {
    if (!to_peer || !from_peer || !chkpt_state) {
        return -EINVAL;
    }
    /* qemu_put_*() or qemu_get_*() will assert() if writable property mismatch,
     * check it before the VM being aborted. */
    if (!qemu_file_is_writable(to_peer) || qemu_file_is_writable(from_peer)) {
        error_report("%s: Peer QEMUFile parameter(s) is invalid", __func__);
        return -EINVAL;
    }
    cs = chkpt_state;
    to_peer_file = to_peer;
    from_peer_file = from_peer;

    /* create reverse keepalive thread */
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_WATCHDOG]) {
        qemu_sem_init(&reverse_keepalive_request, 0);
        qemu_sem_init(&reverse_keepalive_finish_request, 0);
        qemu_sem_init(&reverse_keepalive_finished, 0);
        atomic_set(&ft_reverse_keepalive_thread_should_stop, false);
        qemu_thread_create(&ft_reverse_keepalive_thread,
                           "ft_reverse_keepalive", ft_reverse_keepalive_func,
                           NULL, QEMU_THREAD_JOINABLE);
    }

    return 0;
}

void ft_message_cleanup(void) {
    atomic_set(&ft_reverse_keepalive_thread_should_stop, true);
    qemu_sem_post(&reverse_keepalive_request);
    qemu_thread_join(&ft_reverse_keepalive_thread);
    if (ft_reverse_keepalive_thread_ret != 0) {
        error_report("%s: keepalive thread exited with %d - %s",
                     __func__, ft_reverse_keepalive_thread_ret,
                     strerror(ft_reverse_keepalive_thread_ret));
    }
    qemu_sem_destroy(&reverse_keepalive_finish_request);
    qemu_sem_destroy(&reverse_keepalive_request);
    qemu_sem_destroy(&reverse_keepalive_finished);
}

static void __attribute__((constructor)) ft_message_constructor(void) {
    qemu_mutex_init(&ft_message_mutex);
}

