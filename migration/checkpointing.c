#include "qemu/osdep.h"
#include "sysemu/sysemu.h"
#include "qapi/error.h"
#include "migration.h"
#include "migration/ft.h"
#include "qemu-file-channel.h"
#include "qemu-file.h"
#include "qemu/error-report.h"
#include "qemu/thread.h"
#include "qapi/qmp/qdict.h"

#include "monitor/hmp.h"

static void checkpointing_print_enabled_caps(CheckpointingState *cs);

void hmp_checkpointing_set_type(Monitor *mon, const QDict *qdict)
{
    char *in_type = strdup(qdict_get_str(qdict, "type"));
    MigrationState *ms = migrate_get_current();
    CheckpointingState *cs = checkpointing_get_current();
    CheckpointingType out_type = CHECKPOINTING_TYPE_NONE;
    char help_str[2048] = "";

    for (int i = 0; i < CHECKPOINTING_TYPE__MAX; ++i)
    {
        if (strcmp(in_type, CheckpointingType_str(i)) == 0)
        {
            out_type = i;
            break;
        }
    }

    // Set all capabilitie flags to false
    memset(&cs->enabled_capabilities, 0, sizeof(bool) * CHECKPOINTING_CAPABILITY__MAX);

    /* set caps that are not override-able by migration params
     * The params set by MigrationParameters must be set explicitly, while
     * CheckpointState enabled_capabilities will be set to false automatically */
    switch(out_type)
    {
    case CHECKPOINTING_TYPE_FT_EPOCH_BLOCK_REPL:
        cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_BLOCK_REPLICATE] = true;
    /* fallthrough */

    case CHECKPOINTING_TYPE_FT_EPOCH_BASE:
        atomic_set(&ms->parameters.ft_checkpoint_delay,
                   DEFAULT_FT_CHECKPOINT_DELAY);
        atomic_set(&ms->parameters.ft_auto_cd, true);
        atomic_set(&ms->parameters.ft_watchdog_stage_interval,
                   DEFAULT_FT_WATCHDOG_STAGE_INTERVAL);
        atomic_set(&ms->parameters.ft_local_export, true);
        cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_TX_BUFFERING] = true;
        break;

    case CHECKPOINTING_TYPE_DR_FULL_STATES:
        cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_BLOCK_REPLICATE] = true;
        cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_ZSTD] = true;
        cs->compressor = FT_COMPRESSOR_ZSTD_FAST;
        /* placeholder to disable watchdog/export/autopilot */
        atomic_set(&ms->parameters.ft_watchdog_stage_interval, 0);
        atomic_set(&ms->parameters.ft_shared_export, false);
        atomic_set(&ms->parameters.ft_auto_cd, false);
        atomic_set(&ms->parameters.ft_checkpoint_delay, 60000);
        atomic_set(&ms->parameters.ft_local_export, true);
        if (ms->parameters.ft_compress_threads < 2)
        {
            atomic_set(&ms->parameters.ft_compress_threads, 2);
        }
        break;

    default:
        for (int i = 0; i < CHECKPOINTING_TYPE__MAX; ++i)
        {
            sprintf(help_str + strlen(help_str), "%s, ", CheckpointingType_str(i));
        }
        error_report("Available checkpointing types: %s; note that FT params "
                     "has been reset.", help_str);
        return;
    }

    info_report("Checkpointing Type set to %s", CheckpointingType_str(out_type));
    cs->type = out_type;
}

void hmp_dr_set_mode(Monitor *mon, const QDict *qdict)
{
    char *in_mode = strdup(qdict_get_str(qdict, "mode"));
    CheckpointingState *cs = checkpointing_get_current();
    DRMode out_mode = DR_MODE_NONE;

    for (int i = 0; i < DR_MODE__MAX; ++i)
    {
        if (strcmp(in_mode, DRMode_str(i)) == 0)
        {
            out_mode = i;
            break;
        }
    }

    if (out_mode == DR_MODE_NONE || out_mode >= DR_MODE__MAX)
    {
        char help_str[2048] = "";
        for (int i = 0; i < DR_MODE__MAX; ++i)
        {
            sprintf(help_str + strlen(help_str), "%s, ", DRMode_str(i));
        }
        error_report("Available DR Modes: %s", help_str);
        return;
    }

    info_report("DR Mode set to %s", DRMode_str(out_mode));
    cs->dr_mode = out_mode;
}

/* Setup the type assigned by user and its associated capabilities on Primary host.
 * This function is called after live migration command fired. */
bool checkpointing_setup_type(void)
{
    MigrationState *ms = migrate_get_current();
    CheckpointingState *cs = checkpointing_get_current();

    /* set override-able caps or stop if no type is set */
    switch(cs->type)
    {
    case CHECKPOINTING_TYPE_FT_EPOCH_BLOCK_REPL:
    case CHECKPOINTING_TYPE_FT_EPOCH_BASE:
        if (ms->parameters.ft_compress_threads > 0)
        {
            cs->compressor = FT_COMPRESSOR_LZ4_FAST;
            cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_LZ4] = true;
            cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_COMPRESS_POOL] = true;
        }
        else
        {
            cs->compressor = FT_COMPRESSOR_NONE;
        }
        if (ms->parameters.ft_auto_cd == true)
        {
            cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_AUTOPILOT] = true;
        }

        break;
    case CHECKPOINTING_TYPE_DR_FULL_STATES:
        break;

    default:
        error_report("No valid checkpoint type set. "
                     "Execute \"checkpointing_set_type\" in HMP before migrating.");
        info_report("For virsh users, run \"virsh qemu-monitor-command your-vm-name"
                    " --hmp \"checkpointing_set_type ?\" \" for available types.");
        return false;
    }

    /* common override-able caps for every type */
    if (ms->parameters.ft_watchdog_stage_interval > 0)
    {
        cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_WATCHDOG] = true;
        cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_SOCKET_TIMEOUT] = true;
    }
    cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_SHARED_EXPORT]
                    = ms->parameters.ft_shared_export;
    cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_LOCAL_EXPORT]
                    = ms->parameters.ft_local_export;

    info_report("[Checkpointing] Type=%s", CheckpointingType_str(cs->type));
    checkpointing_print_enabled_caps(cs);
    return true;
}

void checkpointing_send_params(QEMUFile *f)
{
    MigrationState *mig = migrate_get_current();
    CheckpointingState *cs = checkpointing_get_current();

    /* send capabilities count to make sure QEMU-FT version */
    qemu_put_be32(f, (uint32_t) CHECKPOINTING_CAPABILITY__MAX);
    /* send boolean caps */
    for (int i = 0; i < CHECKPOINTING_CAPABILITY__MAX; ++i)
    {
        qemu_put_byte(f, (int) cs->enabled_capabilities[i]);
    }

    /* send integer params */
    qemu_put_be32(f, (uint32_t)
        atomic_read(&mig->parameters.ft_watchdog_stage_interval));
    qemu_put_be32(f, (uint32_t)
        atomic_read(&mig->parameters.ft_compress_threads));
    qemu_put_be32(f, (uint32_t)
        atomic_read(&mig->parameters.ft_block_granularity));

    /* send modes */
    qemu_put_be32(f, (uint32_t)cs->compressor);
}

bool checkpointing_recv_params(QEMUFile *f)
{
    MigrationState *mig = migrate_get_current();
    CheckpointingState *cs = checkpointing_get_current();

    /* check checkpointing capabilities count */
    unsigned int recv_be32 = qemu_get_be32(f);
    if (recv_be32 != (uint32_t) CHECKPOINTING_CAPABILITY__MAX)
    {
        error_report("[Checkpointing] recv caps=%u, expected=%d",
                     recv_be32, CHECKPOINTING_CAPABILITY__MAX);
        return false;
    }
    for (int i = 0; i < CHECKPOINTING_CAPABILITY__MAX; ++i)
    {
        cs->enabled_capabilities[i] = qemu_get_byte(f);
    }
    checkpointing_print_enabled_caps(cs);

    /* so it can be memorized after backup takeover */
    atomic_set(&mig->parameters.ft_shared_export, (uint32_t)
        cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_SHARED_EXPORT]);
    atomic_set(&mig->parameters.ft_local_export, (uint32_t)
        cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_LOCAL_EXPORT]);

    /* set integer params */
    atomic_set(&mig->parameters.ft_watchdog_stage_interval,
        (uint32_t) qemu_get_be32(f));
    atomic_set(&mig->parameters.ft_compress_threads,
        (uint32_t) qemu_get_be32(f));
    atomic_set(&mig->parameters.ft_block_granularity,
        (uint32_t) qemu_get_be32(f));

    /* set modes */
    cs->compressor = qemu_get_be32(f);

    return true;
}

static void checkpointing_print_enabled_caps(CheckpointingState *cs)
{
    /* Print enabled types as info */
    char enabled_caps[2048] = "";
    for (int i = 0; i < CHECKPOINTING_CAPABILITY__MAX; ++i)
    {
        if (cs->enabled_capabilities[i] == true)
        {
            sprintf(enabled_caps + strlen(enabled_caps), "%s;",
                    CheckpointingCapability_str(i));
        }
    }
    info_report("[Checkpointing] EnabledCaps=%s", enabled_caps);
}

CheckpointingType checkpointing_get_type(void)
{
    return checkpointing_get_current()->type;
}
