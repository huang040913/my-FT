#include "qemu/osdep.h"
#include "qemu/error-report.h"
#include "qemu/units.h"
#include "monitor/monitor.h"
#include "exec/target_page.h"
#include "migration/migration.h"
#include "migration/ft.h"
#include "ram.h"

static CheckpointingState *cs = NULL;

/* auto checkpoint delay related */
static int64_t max_allowed_delay_ms = 800;
static int64_t moving_average_interval = 2000; // milliseconds

static int64_t checkpoint_time_since_last_cd_update = 0;
static uint64_t checkpoints_count_since_last_cd_update = 0;
static int64_t host_time_of_last_cd_update = 0; // UNIX timestamp
static int64_t xbzrle_pages_at_last_cd_update = 0;
static int64_t xbzrle_bytes_at_last_cd_update = 0;

/* performance metrics related */
static size_t current_send_throughput = 0; // byte per millisecond
static size_t tmp_sent_size = 0; // bytes
static size_t tmp_sent_time = 0; // milliseconds
static uint64_t tmp_sent_count = 0;
static int64_t time_of_last_perf_update = 0; // UNIX timestamp
static struct {
    FTRecord compress_time_rec;
    FTRecord compressed_size_rec;
    FTRecord size_rec;
    /* on-the-fly statistic(compress, transfer) */
    size_t current_compress_throughput; // byte per millisecond
    double current_compress_ratio;
    int64_t current_avg_send_cost; // milliseconds
    size_t current_avg_send_size; // bytes
    /* Temporary variables for average calculation */
    size_t tmp_after_compressed_size_sum; // bytes
    size_t tmp_before_compressed_size_sum; // bytes
    int64_t tmp_compression_time_sum; // millisecond
    size_t tmp_not_compressed_size_sum; // bytes
} send_type_entries[FT_RECORD_SEND_TYPE__MAX];

static inline void send_type_registration(FTRecordSendType, FTRecord,
                                          FTRecord, FTRecord);
static inline void send_type_entry_clear_tmp(FTRecordSendType type);
static inline bool send_type_is_invalid(FTRecordSendType type);
static inline void update_perf_metrics(void);

/* on-the-fly recording related */
static void *record_storage = NULL;
static uint64_t *rec_stor_uint64 = NULL;
static int64_t *rec_stor_int64 = NULL;
static uint64_t recorded_epoch_count = 0;
static bool record_enabled = false;
#ifdef FT_DEBUG_RECORD
static FTRecord last_recorded_item;
static int64_t record_limit = 10000000;
#else
static int64_t record_limit = 1;
#endif
static inline bool is_uint64_item(FTRecord item);
static inline size_t recorded_base(void);
static inline int64_t *cur_record_int64(FTRecord entry);
static inline uint64_t *cur_record_uint64(FTRecord entry);

/* Workaround for saving memory and dealing with different types
 *   (without GObject or Template<>).
 * Functions outputing record results will refer the array to know the type.
 * Currently, only int64_t and uint64_t is allowed, so list uint64s here. */
static const FTRecord uint64_entries[] = {
    FT_RECORD_BLOCK_SIZE, FT_RECORD_BLOCK_COMPRESSED_SIZE,
    FT_RECORD_RAM_SIZE, FT_RECORD_RAM_COMPRESSED_SIZE,
    FT_RECORD_VMSD_SIZE, FT_RECORD_VMSD_COMPRESSED_SIZE
};

static inline size_t recorded_base(void)
{
    return recorded_epoch_count * FT_RECORD__MAX;
}

static inline int64_t *cur_record_int64(FTRecord item)
{
    return (int64_t *)&rec_stor_int64[recorded_epoch_count * FT_RECORD__MAX + item];
}

static inline uint64_t *cur_record_uint64(FTRecord item)
{
    return (uint64_t *)&rec_stor_uint64[recorded_epoch_count * FT_RECORD__MAX + item];
}

/* May need lots of time; should be run within a thread and after first migration */
bool ft_autopilot_init(MigrationState *s)
{
    if (!record_storage)
    {
        record_storage = calloc(record_limit * FT_RECORD__MAX, sizeof(int64_t));
    }
    else
    {
        memset(record_storage, 0, record_limit * FT_RECORD__MAX * sizeof(int64_t));
    }
    cs = checkpointing_get_current();

    recorded_epoch_count = 0;
    rec_stor_int64 = (int64_t *)record_storage;
    rec_stor_uint64 = (uint64_t *)record_storage;
    record_enabled = false;
    cs->max_allowed_delay = max_allowed_delay_ms;
    time_of_last_perf_update = 0;
    xbzrle_pages_at_last_cd_update = xbzrle_counters.pages;
    xbzrle_bytes_at_last_cd_update = xbzrle_counters.bytes;
    send_type_registration(FT_RECORD_SEND_TYPE_RAM, FT_RECORD_RAM_COMPRESS_TIME,
                           FT_RECORD_RAM_COMPRESSED_SIZE, FT_RECORD_RAM_SIZE);
    send_type_registration(FT_RECORD_SEND_TYPE_BLOCK, FT_RECORD_BLOCK_COMPRESS_TIME,
                           FT_RECORD_BLOCK_COMPRESSED_SIZE, FT_RECORD_BLOCK_SIZE);
    send_type_registration(FT_RECORD_SEND_TYPE_VMSD, FT_RECORD_VMSD_COMPRESS_TIME,
                           FT_RECORD_VMSD_COMPRESSED_SIZE, FT_RECORD_VMSD_SIZE);
    return record_storage != NULL;
}

void ft_autopilot_record_uint64(FTRecord item, uint64_t val)
{
#ifdef FT_DEBUG_RECORD
    last_recorded_item = item;
#endif
    rec_stor_uint64[recorded_base() + item] = val;
}

void ft_autopilot_record_int64(FTRecord item, int64_t val)
{
#ifdef FT_DEBUG_RECORD
    last_recorded_item = item;
#endif
    rec_stor_int64[recorded_base() + item] = val;
}

void ft_autopilot_record_host_clock(FTRecord item)
{
#ifdef FT_DEBUG_RECORD
    last_recorded_item = item;
#endif
    rec_stor_int64[recorded_base() + item] = qemu_clock_get_ms(QEMU_CLOCK_HOST);
}

void ft_autopilot_append_int64(FTRecord item, int64_t val)
{
    int64_t orig = *cur_record_int64(item);
    rec_stor_int64[recorded_base() + item] = orig + val;
}

void ft_autopilot_append_uint64(FTRecord item, uint64_t val)
{
    uint64_t orig = *cur_record_uint64(item);
    rec_stor_uint64[recorded_base() + item] = orig + val;
}

static inline bool is_uint64_item(FTRecord item)
{
    for (int i = 0; i < (sizeof(uint64_entries) / sizeof(uint64_entries[0])); ++i)
    {
        if (item == uint64_entries[i])
        {
            return true;
        }
    }
    return false;
}

/* increase recorded epoch count.
 * in-flight record in production should be clean by main FT flow. */
void ft_autopilot_record_epoch_end(void)
{
    update_perf_metrics();
#ifdef FT_DEBUG_RECORD
    if (record_enabled && recorded_epoch_count < record_limit )
    {
        ++recorded_epoch_count;
    }
    else if (record_enabled)
    {
        info_report("FT Record buffer is full");
    }
    memset(record_storage + recorded_base(), 0, FT_RECORD__MAX * sizeof(int64_t));
#else
    /* clear record storage to make append_ functions work */
    memset(record_storage, 0, FT_RECORD__MAX * sizeof(int64_t));
#endif
}

void ft_autopilot_record_start(void)
{
    record_enabled = true;
}

void *ft_autopilot_record_stop_and_save_csv_thread(void *opaque)
{
#ifdef FT_DEBUG_RECORD
    if (!record_enabled || recorded_epoch_count <= 1)
    {
        info_report("start record first; nothing to save.");
        return NULL;
    }

    char *csv_path = opaque;
    record_enabled = false;
    info_report("record stopped, now saving record to %s", csv_path);

    FILE *out = fopen(csv_path, "w");
    if(!out)
    {
        error_report("cannot open file %s to write record, "
                     "try stop recording with correct filename again.", csv_path);
        return NULL;
    }
    /* csv header */
    for (int i = 0; i < FT_RECORD__MAX; ++i)
    {
        fprintf(out, "%s,", FTRecord_str(i));
    }
    fprintf(out, "\n");
    /* csv body
     * skipping last entry since it may have been rewrote by newer entries. */
    for (uint64_t i = 0; i < (recorded_epoch_count - 1); ++i)
    {
        for (int j = 0; j < FT_RECORD__MAX; ++j)
        {
            fprintf(out, (is_uint64_item(j))? "%lu," : "%ld,",
                    (is_uint64_item(j))? rec_stor_uint64[i * FT_RECORD__MAX + j] :
                                         rec_stor_int64[i * FT_RECORD__MAX + j] );
        }
        fprintf(out, "\n");
    }

    fclose(out);

    info_report("record saved to %s", csv_path);
    /* reset recorder */
    recorded_epoch_count = 0;
#else
    error_report("DO NOT call %s in production", __func__);
#endif
    return NULL;
}

/* set delay time by average time spent of the last moving average window.
 * Must only be called from Primary and after FT_RECORD_EPOCH_END is set,
 * and (mostly) only take RAM save/send time into consideration. */
void ft_autopilot_update_checkpoint_delay(MigrationState *migs)
{
    checkpoint_time_since_last_cd_update += (*cur_record_int64(FT_RECORD_EPOCH_END) -
                                             *cur_record_int64(FT_RECORD_EPOCH_BEGIN));
    ++checkpoints_count_since_last_cd_update;

    /* Will not caluclate or set new delay until interval reached */
    if ((*cur_record_int64(FT_RECORD_EPOCH_END) - host_time_of_last_cd_update <
            moving_average_interval) || checkpoints_count_since_last_cd_update == 0)
    {
        return;
    }

    int64_t checkpoint_avg_spends = (int64_t)((double) checkpoint_time_since_last_cd_update /
                                              (double) checkpoints_count_since_last_cd_update);
    int64_t extra_delay = 0;

    /* When xbzrle's LRU cache is large enough and miss rate is low,
     * checkpoint will be too fast to cause guest application performance drop; it usually take
     * longer time if standard compression is utilized, so skip if dirty pages are compressed. */
    if (migs->enabled_capabilities[MIGRATION_CAPABILITY_XBZRLE] &&
        *cur_record_int64(FT_RECORD_RAM_COMPRESSED_SIZE) == 0lu)
    {
        int64_t xbzrle_pages = xbzrle_counters.pages - xbzrle_pages_at_last_cd_update;
        int64_t xbzrle_bytes = xbzrle_counters.bytes - xbzrle_bytes_at_last_cd_update;
        xbzrle_pages_at_last_cd_update = xbzrle_counters.pages;
        xbzrle_bytes_at_last_cd_update = xbzrle_counters.bytes;

        int64_t orig_bytes = xbzrle_pages * (int64_t)qemu_target_page_size();
        int64_t saved_bytes = orig_bytes - xbzrle_bytes;

        /* only add the extra delay when the network is faster than GbE*/
        if (current_send_throughput > 128 * KiB)
        {
            extra_delay += (saved_bytes / current_send_throughput);
        }
    }

    /* shrink the delay caused by sending block(disk) contents,
     * we can slightly prevent hazards caused by IO-heavy workload by doing this. */
    if (cs->enabled_capabilities[CHECKPOINTING_CAPABILITY_BLOCK_REPLICATE])
    {
        extra_delay -= send_type_entries[FT_RECORD_SEND_TYPE_BLOCK].current_avg_send_cost;
    }

    /* require extra_delay to be less than or equal to checkpoint_avg_spends */
    extra_delay = (extra_delay < (checkpoint_avg_spends / 2))?
                        extra_delay : (checkpoint_avg_spends / 2);
    int64_t next_checkpoint_delay = checkpoint_avg_spends + extra_delay;

    /* Set next delay within reasonable range; make sure this is the last step before setting. */
    next_checkpoint_delay = (next_checkpoint_delay > cs->max_allowed_delay)?
                            cs->max_allowed_delay : next_checkpoint_delay;
    next_checkpoint_delay = (next_checkpoint_delay <= 0)? 1 : next_checkpoint_delay;
    atomic_set(&migs->parameters.ft_checkpoint_delay, next_checkpoint_delay);

    checkpoint_time_since_last_cd_update = 0;
    checkpoints_count_since_last_cd_update = 0;
    host_time_of_last_cd_update = rec_stor_int64[recorded_base() + FT_RECORD_EPOCH_END];
}

bool ft_autopilot_should_do_compress(FTRecordSendType type, size_t to_send_byte)
{
    uint64_t compress_throughput = send_type_entries[type].current_compress_throughput;
    double compress_ratio = send_type_entries[type].current_compress_ratio;

    /* force compression if no statistic available, or in DR mode */
    if (current_send_throughput == 0 || compress_throughput == 0 ||
        checkpointing_get_type() == CHECKPOINTING_TYPE_DR_FULL_STATES)
    {
        return true;
    }
    /* calculate estimates */
    int64_t direct_send_ms = to_send_byte / current_send_throughput;
    int64_t compress_ms = to_send_byte / compress_throughput;
    size_t compressed_byte = (size_t)((double)to_send_byte * compress_ratio);
    int64_t send_compressed_ms = (int64_t)compressed_byte / current_send_throughput;

    /* never compress if it will take too much time, or send directly is fast enough */
    if (compress_ms > max_allowed_delay_ms || direct_send_ms < 2)
    {
        return false;
    }

    return direct_send_ms > (compress_ms + send_compressed_ms);
}

void update_perf_metrics(void)
{
    uint64_t sent_bytes = 0;
    int64_t compress_time_sum = 0;

    /* accumulate send size, compressed and original sizes for each send type */
    for (int i = 0; i < FT_RECORD_SEND_TYPE__MAX; ++i)
    {
        uint64_t orig_size = *cur_record_uint64(send_type_entries[i].size_rec);
        uint64_t compressed_size = *cur_record_uint64(send_type_entries[i].compressed_size_rec);
        uint64_t compress_time = *cur_record_uint64(send_type_entries[i].compress_time_rec);

        /* data of this type is not compressed this time,
         * accumulating the buffer size for network throughput computation */
        if (compressed_size == 0lu)
        {
            sent_bytes += orig_size;
            send_type_entries[i].tmp_not_compressed_size_sum += orig_size;
            continue;
        }

        /* throughput can not be properly calculated when compress_time <= 1, ignore it;
         * we might get 1 ms but the real compress runtime is only 50us. */
        if (compress_time > 1)
        {
            sent_bytes += compressed_size;
            send_type_entries[i].tmp_after_compressed_size_sum += compressed_size;
            send_type_entries[i].tmp_before_compressed_size_sum += orig_size;
            send_type_entries[i].tmp_compression_time_sum += compress_time;
            compress_time_sum += compress_time;
        }
    }

    ++tmp_sent_count;
    tmp_sent_size += sent_bytes;
    tmp_sent_time += (*cur_record_int64(FT_RECORD_SEND_END) -
                      *cur_record_int64(FT_RECORD_SEND_BEGIN)) - compress_time_sum;

    /* skip updating until reached moving_average_interval */
    if (unlikely(time_of_last_perf_update == 0))
    {
        time_of_last_perf_update = *cur_record_int64(FT_RECORD_EPOCH_END);
        return;
    }
    if (*cur_record_int64(FT_RECORD_EPOCH_END) - time_of_last_perf_update < moving_average_interval)
    {
        return;
    }

    /* update compression throughput and ratio */
    for (int i = 0; i < FT_RECORD_SEND_TYPE__MAX; ++i)
    {
        uint64_t orig_size = send_type_entries[i].tmp_before_compressed_size_sum;
        uint64_t compressed_size = send_type_entries[i].tmp_after_compressed_size_sum;
        int64_t compress_time = send_type_entries[i].tmp_compression_time_sum;

        if (compressed_size > 0 && compress_time > 0)
        {
            send_type_entries[i].current_compress_throughput = compressed_size / (uint64_t)compress_time;
            // TODO Check in real world
            /* sanity check: force an update if ratio > 1.0 although it may be possible in the worst case */
            double ratio = (double)compressed_size / (double)orig_size;
            send_type_entries[i].current_compress_ratio = (ratio < 1.0)? ratio : 0.0;
        }
    }

    /* update network throughput */
    current_send_throughput =
            tmp_sent_size / (uint64_t)((tmp_sent_time > 0)? tmp_sent_time : 1);



    /* update send cost */
    for (int i = 0; i < FT_RECORD_SEND_TYPE__MAX; ++i)
    {
        size_t type_sent_bytes = send_type_entries[i].tmp_after_compressed_size_sum +
                                 send_type_entries[i].tmp_not_compressed_size_sum;
        send_type_entries[i].current_avg_send_size = type_sent_bytes / tmp_sent_count;
        send_type_entries[i].current_avg_send_cost =
                (int64_t)send_type_entries[i].current_avg_send_size / current_send_throughput;
    }

    /* clear temporary data */
    for (int i = 0; i < FT_RECORD_SEND_TYPE__MAX; ++i)
    {
        send_type_entry_clear_tmp(i);
    }

    time_of_last_perf_update = *cur_record_int64(FT_RECORD_EPOCH_END);
    tmp_sent_size = 0;
    tmp_sent_time = 0;
    tmp_sent_count = 0;
}

void ft_autopilot_print_stat(void)
{
    static int show_title_countdown = 0;
    if (show_title_countdown == 0)
    {
        printf("BLKRAM_SIZE,VMSD_SIZE,BLK+RAM_CMP,SPENT_TIME,SET_DELAY,  TX_QUEUED,+DELAY\n"
               " G  M  K  B,  M  K  B, G  M  K  B,        ms,       ms, G  M  K  B,    ms\n");
        /* most linux terminal can display 24 rows default, but show the hint more times */
        show_title_countdown = 12;
    }
    printf("%11lu,%9lu,%11lu,%10ld,%9ld,%11lu,%6ld\n",
        rec_stor_uint64[recorded_base() + FT_RECORD_RAM_SIZE] +
        rec_stor_uint64[recorded_base() + FT_RECORD_BLOCK_SIZE],
        rec_stor_uint64[recorded_base() + FT_RECORD_VMSD_SIZE],
        rec_stor_uint64[recorded_base() + FT_RECORD_RAM_COMPRESSED_SIZE] +
        rec_stor_uint64[recorded_base() + FT_RECORD_BLOCK_COMPRESSED_SIZE],
        rec_stor_int64[recorded_base() + FT_RECORD_EPOCH_END] -
                    rec_stor_int64[recorded_base() + FT_RECORD_EPOCH_BEGIN],
        rec_stor_int64[recorded_base() + FT_RECORD_CHECKPOINT_DELAY],
        rec_stor_uint64[recorded_base() + FT_RECORD_TX_QUEUE_SIZE],
        rec_stor_int64[recorded_base() + FT_RECORD_EXTRA_DELAY]);
    --show_title_countdown;
}

uint64_t ft_autopilot_record_get_uint64(FTRecord item)
{
    return rec_stor_uint64[recorded_base() + item];
}

int64_t ft_autopilot_record_get_int64(FTRecord item)
{
    return rec_stor_int64[recorded_base() + item];
}

bool send_type_is_invalid(FTRecordSendType type)
{
    if (unlikely(type >= FT_RECORD_SEND_TYPE__MAX))
    {
        error_report("%s: invalid send type %d", __func__, type);
        return true;
    }
    return false;
}

/* register send types during init */
void send_type_registration(FTRecordSendType type,
        FTRecord compress_time, FTRecord compressed_size, FTRecord original_size)
{
    if (send_type_is_invalid(type))
    {
        error_report("%s: invalid send type %d", __func__, type);
        return;
    }
    memset(&send_type_entries[type], 0, sizeof(send_type_entries[type]));
    send_type_entries[type].compress_time_rec = compress_time;
    send_type_entries[type].compressed_size_rec = compressed_size;
    send_type_entries[type].size_rec = original_size;
}

/* helper: backup FTRecords, memset the struct then restore FTRecords */
void send_type_entry_clear_tmp(FTRecordSendType type)
{
    send_type_entries[type].tmp_after_compressed_size_sum = 0;
    send_type_entries[type].tmp_before_compressed_size_sum = 0;
    send_type_entries[type].tmp_compression_time_sum = 0;
    send_type_entries[type].tmp_not_compressed_size_sum = 0;
}

void ft_autopilot_fill_send_type_data(FTRecordSendType type, int64_t time, uint64_t size)
{
    if (send_type_is_invalid(type))
    {
        error_report("%s: invalid send type %d", __func__, type);
        return;
    }
    ft_autopilot_record_int64(send_type_entries[type].compress_time_rec, time);
    ft_autopilot_record_uint64(send_type_entries[type].compressed_size_rec, size);
}

void ft_autopilot_hmp_info_migrate(void *hmp_monitor)
{
    Monitor *mon = hmp_monitor;
    monitor_printf(mon, "FT Network Throughput: %ld mbps\n",
                        current_send_throughput * 1000 / 1024 / 1024 * 8);
    monitor_printf(mon, "FT Average RAM Size: %lu bytes\n",
                        send_type_entries[FT_RECORD_SEND_TYPE_RAM].current_avg_send_size);
    monitor_printf(mon, "FT Average Block Size: %lu bytes\n",
                        send_type_entries[FT_RECORD_SEND_TYPE_BLOCK].current_avg_send_size);
}

#ifdef FT_DEBUG_RECORD
FTRecord ft_get_last_recorded_item(void)
{
    return last_recorded_item;
}
#endif
