/*
 * QEMU I/O channels compress(zstd) driver for migration
 * designed for ft init state (first migration)
 *
 * Copyright (c) 2015 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef QIO_CHANNEL_COMPRESS_H
#define QIO_CHANNEL_COMPRESS_H

#include "io/channel.h"

#define TYPE_QIO_CHANNEL_COMPRESS "qio-channel-compress"
#define QIO_CHANNEL_COMPRESS(obj)                                     \
    OBJECT_CHECK(QIOChannelCompress, (obj), TYPE_QIO_CHANNEL_COMPRESS)

typedef struct QIOChannelCompress QIOChannelCompress;

typedef struct QIOChannelCompressInfo QIOChannelCompressInfo;

/**
 * QIOChannelCompress:
 *
 * The QIOChannelCompress object provides a channel implementation
 * that is able to perform I/O to/from a compress buffer.
 *
 */

struct QIOChannelCompress {
    QIOChannel parent;
    QIOChannel *target;           /* Read or Write target(original) channel */
    QIOChannelCompressInfo *info; /* only in channel-compress.c */
};

enum OperationMode {
    QIOC_ZSTD_COMPRESSION = 1,
    QIOC_ZSTD_UNCOMPRESSION,
    QIOC_COMP_MAX
};

/**
 * qio_channel_compress_new:
 * @target: source or destination channel for (de)compress
 *
 * Init buffer and ZSTD compress resource
 *
 * Returns: the new channel object
 */
QIOChannelCompress* qio_channel_compress_new(QIOChannel *target, int opm);

bool qio_channel_compress_write_flush(QIOChannelCompress *cioc);

extern bool qio_channel_compress_enable;

void qio_channel_compress_parameters(bool set,
                                     int *thread,
                                     int *queue,
                                     int *size);

#endif /* QIO_CHANNEL_COMPRESS_H */
