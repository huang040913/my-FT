
#ifndef QEMU_FT_H
#define QEMU_FT_H

#include "qapi/qapi-types-migration.h"
#include "qapi/qapi-types-run-state.h"

// ft-vmstate tell backup need enable ft
void ft_info_init(void);

bool ft_vmstate_get_info(void);
void ft_vmstate_clear_info(void);

FTRole ft_get_assigned_role(void);
void ft_set_assigned_role(FTRole);
FTRole ft_get_active_role(void);

// common
void ft_vm_start(void);
void ft_vm_stop(RunState);
void ft_vm_stop_still_lock(RunState, bool);
bool ft_is_supported(void);
void ft_set_status(FTStatus, const char *);
void ft_sync_shutdown(const char *);

// primary side
bool migration_in_ft_state(void);
void migrate_start_ft(MigrationState *);
void ft_checkpoint_notify(void *);
void ft_notify_guest_powered_off_pre(void);
void ft_notify_guest_powered_off_post(void);

// backup side
bool migration_incoming_in_ft_state(void);
void *migrate_ft_incoming_thread(void *);

/* common for ft-message
 * Always call ft_message_lock before calling *_message* or *_buffer*.
 * Watchdog keep-alive wont send if the message is being hold by ft thread.
 * QEMUFiles are self-contained, and are assigned by ft_message_init().
 * Return 0 on success, and -errno when failed.
 */
int ft_send_message(FTMessage , Error **);
int ft_send_message_and_value(FTMessage , uint64_t, Error **);
int ft_send_value(uint64_t , Error **);
int ft_send_buffer(const uint8_t *, size_t);
int ft_send_buffer_adapt(uint8_t *, size_t, FTRecordSendType);
int ft_receive_message(FTMessage *, Error **);
int ft_receive_and_check_message(FTMessage, Error **);
int ft_receive_message_and_value(FTMessage, uint64_t *, Error **);
int ft_receive_value(uint64_t *, Error **);
int ft_receive_buffer(uint8_t *, size_t);
int ft_receive_buffer_adapt(uint8_t *, size_t);
int ft_send_size_and_wait_malloc(FTMessage, size_t, size_t, Error **);
int ft_receive_size_and_malloc(size_t *, FTMessage, size_t, void *, Error **);
int ft_message_init(QEMUFile *, QEMUFile *, void *);
void ft_message_cleanup(void);
bool ft_compression_init(FTCompressor);
void ft_compression_cleanup(bool);

// common for ft-net
bool ft_net_init(Error **errp);
bool ft_net_is_supported(void);

/*
 *  PS: I don't want include more header, like: net/net.h, qemu/osdep.h
 *  so use void pointer, it may be danger for pointer type cast
 *  if programmer doesn't know argument usage
 */
bool ft_net_tx_buffering_enabled(void);
ssize_t ft_net_tx_queue_append(void *, const void *const, const int);
ssize_t ft_net_post_savevm(void);  //only use in migration(FT) thread
void ft_net_post_sendvm(void);  //only use in migration(FT) thread
void ft_net_flush_tx(void);  //only use in migration(FT) thread
ssize_t ft_net_failover_flush_tx(void);  //only use in main thread (IO thread) when failover
_Bool ft_tx_queue_is_empty(void); //only use in migration(FT) thread to skip checkpoint

/*  common for FT watchdog. Message lock are used in ft.c to make sure keep-alive function will not
 *   collide with normal FTMessage. Make these interfaces public to lock/unlock less frequently.
 */
#define DEFAULT_FT_WATCHDOG_STAGE_INTERVAL 250
// any FTMessage has been sent/received since last call
bool ft_messaging_is_active(void);
void ft_message_lock(void);
void ft_message_unlock(void);
// ft_message_trylock return 0 when lock is successfully granted, see pthread_mutex_trylock().
int ft_message_trylock(void);
void ft_reverse_keepalive_start(void);
int ft_reverse_keepalive_finish(void);

// ft-export shared. Returning true means minimal required operation succeed.
bool ft_export_init_primary(void);
bool ft_export_init_primary_validate(void);
bool ft_export_init_backup(void);
bool ft_export_init_backup_validate(void);
bool ft_export_put_takeover_lock(void);
bool ft_export_timed_wait_takeover_lock(void);
bool ft_export_exists_takeover_lock(void);
void ft_export_put_status(FTStatus);
FTStatus ft_export_get_status(void);
void ft_export_append_migrated(void);
void ft_export_append_alert(void);
bool ft_export_is_disabled(void);
void ft_disable_shared_export(void);
void ft_export_cleanup_shared(void);

// ft-export local.
bool ft_export_init_local(bool);
void ft_export_cleanup_local(void);
void ft_export_fill_locals(void);
void ft_export_start_locals_polling(void);

// ft-autopilot. functions here works for only Primary.
#define DEFAULT_FT_CHECKPOINT_DELAY 200
bool ft_autopilot_init(MigrationState *);
void ft_autopilot_fill_send_type_data(FTRecordSendType, int64_t, uint64_t);
void ft_autopilot_update_checkpoint_delay(MigrationState *);
bool ft_autopilot_should_do_compress(FTRecordSendType, size_t);
/* Note: use ft_debug_record_* to prevent unnecessary function calls. */
void ft_autopilot_record_uint64(FTRecord, uint64_t);
void ft_autopilot_record_int64(FTRecord, int64_t);
void ft_autopilot_record_host_clock(FTRecord);
void ft_autopilot_append_uint64(FTRecord, uint64_t);
void ft_autopilot_append_int64(FTRecord, int64_t);
void ft_autopilot_record_epoch_end(void);

//int ft_create_channel_file(QIOChannelFile *block_channel,QIOChannelFile *ram_channel,QIOChannelFile *vmsd_channel);
#ifdef FT_DEBUG_RECORD
# define ft_debug_record_uint64(x,y) ft_autopilot_record_uint64(x,y)
# define ft_debug_record_int64(x,y) ft_autopilot_record_int64(x,y)
# define ft_debug_record_host_clock(x) ft_autopilot_record_host_clock(x)
FTRecord ft_get_last_recorded_item(void);
#else
# define ft_debug_record_uint64(x, y)
# define ft_debug_record_int64(x, y)
# define ft_debug_record_host_clock(x)
#endif //FT_DEBUG_RECORD
void ft_autopilot_record_start(void);
void *ft_autopilot_record_stop_and_save_csv_thread(void *);
/* print formatted stat of current epoch */
void ft_autopilot_print_stat(void);
/* return the last record value */
uint64_t ft_autopilot_record_get_uint64(FTRecord);
int64_t ft_autopilot_record_get_int64(FTRecord);
void ft_autopilot_hmp_info_migrate(void *);

/*FIXME separate autopilot skip from main FT logic
 * bool ft_autopilot_epoch_skip_suggested(); */

/* common helper functions */
bool ft_block_replication_enabled(void);
int ft_discard_resize_qio_channel_buffer(size_t new_size, void *opaque);

/* New checkpoint-based init functions for both FT and DR. */
bool checkpointing_setup_type(void);
void checkpointing_send_params(QEMUFile *);
bool checkpointing_recv_params(QEMUFile *);
CheckpointingType checkpointing_get_type(void);

#endif
