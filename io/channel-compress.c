/*
 * QEMU I/O channels compress(zstd) driver for migration
 * designed for ft init state (first migration)
 *
 * Copyright (c) 2015 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "qemu/osdep.h"
#include "io/channel-compress.h"
#include "io/channel-watch.h"
#include "qemu/module.h"
#include "qemu/thread.h"
#include "qemu/queue.h"
#include "qemu/error-report.h"
#include "qemu/timer.h"
#include "qemu/coroutine.h"
#include "qemu/main-loop.h"
#include "qapi/error.h"
#include "qapi/qmp/qdict.h"
#include "monitor/monitor.h"
#include "monitor/hmp.h"
#include "migration/zstd.h"

bool qio_channel_compress_enable = false;

enum TargetIoState {
    TARGET_IO_IDLE,
    TARGET_IO_ACTIVE,
    TARGET_IO_FINISH,
    TARGET_IO_ERROR
};

typedef struct WorkQueue {
    QTAILQ_ENTRY(WorkQueue) entry;
    ZSTD_CCtx *cctx;     /* ZSTD compress API resource struct */
    ZSTD_DCtx *dctx;     /* ZSTD decompress API resource struct */
    uint8_t *ori_buf;    /* original buffer for (de)compress */
    size_t ori_offset;   /* Current offset(position) of ori_buf */
    size_t ori_size;     /* Allocated size of ori_buf */
    size_t ori_usage;    /* Current used size of ori_buf */
    uint8_t *cmped_buf;  /* (de)compressed buffer for target channel */
    size_t cmped_offset; /* Current offset(position) of cmped_buf */
    size_t cmped_size;   /* Allocated size of cmped_buf */
    size_t cmped_usage;  /* Current used size of cmped_buf */
    uint64_t seg_id;     /* segment position number in data stream */
    Error *errp;         /* compress work queue error state */
} WorkQueue;

typedef QTAILQ_HEAD (, WorkQueue) WqHead;

typedef struct TargetIO {
    QIOChannel *ioc;
    QemuThread thread;
    QemuSemaphore notify;
    uint64_t seg_counter;
    int state;
    Error *errp;
} TargetIO;

struct QIOChannelCompressInfo {
    QIOChannelCompress *cioc;
    TargetIO target;
    WqHead ready;
    WqHead comed;
    WqHead trans;
    WqHead error;
    QemuMutex lock_wq;
    QemuSemaphore notify;
    bool waiting;
    bool notified;
    uint32_t mode;
    WorkQueue *current;
    uint64_t seg_counter;
    uint32_t thread_num;
    Coroutine *cort;
    QEMUBH *bh;
    GThreadPool *pool;
};

static inline bool wq_lock(QIOChannelCompressInfo *info) {
    qemu_mutex_lock(&info->lock_wq);
    return true;
}

static inline bool wq_unlock(QIOChannelCompressInfo *info) {
    qemu_mutex_unlock(&info->lock_wq);
    return true;
}

static inline WorkQueue* wq_first(QIOChannelCompressInfo *info, WqHead *head) {
    WorkQueue *ret;
    qemu_mutex_lock(&info->lock_wq);
    ret = ((head)->tqh_first);
    qemu_mutex_unlock(&info->lock_wq);
    return ret;
}

static inline WorkQueue* wq_next(QIOChannelCompressInfo *info, WorkQueue *q) {
    WorkQueue *ret;
    qemu_mutex_lock(&info->lock_wq);
    ret = q->entry.tqe_next;
    qemu_mutex_unlock(&info->lock_wq);
    return ret;
}

#define WQ_EMPTY(head) (                    \
            wq_lock(info) &                 \
            ((head)->tqh_first == NULL) &   \
            wq_unlock(info)                 \
        )

#define WQ_LOOP(var, head, field)                                           \
        for (qemu_mutex_lock(&info->lock_wq),                               \
            (var) = ((head)->tqh_first), qemu_mutex_unlock(&info->lock_wq); \
        (var); )


//#define QIO_CHANNEL_COMPRESS_BUF_SIZE (16<<20)
//#define QIO_CHANNEL_COMPRESS_THRESHOLD (8<<20)
#define QIO_CHANNEL_COMPRESS_BUF_SIZE (8<<20)
#define QIO_CHANNEL_COMPRESS_THRESHOLD (4<<20)

static int qio_channel_compress_thread_num = 8;
static int qio_channel_compress_wq_num = 16;
static int qio_channel_compress_level = 3;
static int qio_channel_compress_buf_size = QIO_CHANNEL_COMPRESS_BUF_SIZE;
static int qio_channel_compress_threshold = QIO_CHANNEL_COMPRESS_THRESHOLD;

static uint64_t cioc_ori_total_size;
static uint64_t cioc_cmp_total_size;
static int64_t cioc_wait_total_us;

static void target_get_buffer(TargetIO *io,
                              uint8_t *buf,
                              size_t size);
static void target_put_buffer(TargetIO *io,
                              uint8_t *buf,
                              size_t size);

static void target_get_uint64(TargetIO *io,
                              uint64_t *ret);
static void target_put_uint64(TargetIO *io,
                              uint64_t val);

static inline void wq_append_safe(QIOChannelCompressInfo *info,
                                  WqHead *head,
                                  WorkQueue *queue);
static inline void wq_remove_safe(QIOChannelCompressInfo *info,
                                  WqHead *head,
                                  WorkQueue *queue);
static inline void wq_change_safe(QIOChannelCompressInfo *info,
                                  WqHead *old,
                                  WqHead *new,
                                  WorkQueue *queue);

static inline bool work_queue_do_compress(WorkQueue *wq)
{
    size_t ret;

    ret = ZSTD_compressCCtx(wq->cctx,
                            wq->cmped_buf,
                            wq->cmped_size,
                            wq->ori_buf,
                            wq->ori_usage,
                            qio_channel_compress_level);
    if(ZSTD_isError(ret)) {
        error_setg(&wq->errp,
                   "ZSTD compress error: %s",
                   ZSTD_getErrorName(ret));
        wq->cmped_usage = 0;
        return false;
    }

    wq->cmped_usage = ret;

    return true;
}

static inline bool work_queue_do_decompress(WorkQueue *wq)
{
    size_t ret;

    ret = ZSTD_decompressDCtx(wq->dctx,
                              wq->ori_buf,
                              wq->ori_size,
                              wq->cmped_buf,
                              wq->cmped_usage);
    if(ZSTD_isError(ret)) {
        error_setg(&wq->errp,
                   "ZSTD decompress error: %s",
                   ZSTD_getErrorName(ret));
        wq->ori_usage = 0;
        return false;
    }

    wq->ori_usage = ret;

    return true;
}

static void work_queue_thread_func(gpointer data, gpointer userdata) {
    QIOChannelCompressInfo *info = (QIOChannelCompressInfo *) userdata;
    WorkQueue *wq = (WorkQueue*) data;

    switch(info->mode) {
        case QIOC_ZSTD_COMPRESSION :
            if(!work_queue_do_compress(wq)) {
                return wq_change_safe(info, &info->comed, &info->error, wq);
            }
            wq_change_safe(info, &info->comed, &info->trans, wq);
            //info_report("channel-compress: compr seg %lu", wq->seg_id);
            if(atomic_read(&info->target.state) == TARGET_IO_IDLE) {
                qemu_sem_post(&info->target.notify);
            }
            break;
        case QIOC_ZSTD_UNCOMPRESSION :
            if(!work_queue_do_decompress(wq)) {
                return wq_change_safe(info, &info->comed, &info->error, wq);
            }
            wq_change_safe(info, &info->comed, &info->ready, wq);
            //info_report("channel-compress: decompr seg %lu", wq->seg_id);
            if(atomic_read(&info->waiting)) {
                if(info->cort) {
                    wq_lock(info);
                    if(!info->notified) {
                        info->notified = true;
                        qemu_bh_schedule(info->bh);
                    }
                    wq_unlock(info);
                } else {
                    qemu_sem_post(&info->notify);
                }
            }
            break;
    }
}

static ssize_t qio_channel_compre_readv(QIOChannel *ioc,
                                        const struct iovec *iov,
                                        size_t niov,
                                        int **fds,
                                        size_t *nfds,
                                        Error **errp)
{
    QIOChannelCompress *cioc = QIO_CHANNEL_COMPRESS(ioc);
    QIOChannelCompressInfo *info = cioc->info;
    WorkQueue *wq = info->current;
    WorkQueue *cur;
    uint8_t *buf;
    size_t size;
    int64_t take_us;

    // caller must use only one iov, like qio_channel_read()
    if(niov > 1) {
        error_setg(errp, "channel-compress: not support multiple IOV read");
        return -1;
    }

    if(!wq) {

        take_us = qemu_clock_get_us(QEMU_CLOCK_REALTIME);

        while(!wq) {

            atomic_set(&info->waiting, false);

            if(info->target.state == TARGET_IO_ERROR) {
                *errp = info->target.errp;
                return -1;
            }
            if(!WQ_EMPTY(&info->error)) return -1;

            WQ_LOOP(cur, &info->ready, entry) {
                if(cur->seg_id == info->seg_counter) {
                    atomic_set(&info->waiting, false);
                    wq = cur;
                    info->current = wq;
                    wq_remove_safe(info, &info->ready, cur);
                    info->seg_counter++;
                    
                    cioc_ori_total_size += wq->ori_usage;
                    cioc_cmp_total_size += wq->cmped_usage;

                    //info_report("channel-compress: find seg %lu", wq->seg_id);
                    break;
                }
                cur = wq_next(info, cur);
            }

            if(!WQ_EMPTY(&info->ready) && WQ_EMPTY(&info->comed)) {
                g_usleep(10);
                continue;
            }
            if(atomic_read(&info->target.state) != TARGET_IO_ACTIVE &&
               WQ_EMPTY(&info->comed)) {
                g_usleep(10);
                continue;
            }
            atomic_set(&info->waiting, true);

            if(info->cort) {
                wq_lock(info);
                info->notified = false;
                wq_unlock(info);
                qemu_coroutine_yield();
            } else {
                qemu_sem_wait(&info->notify);
            }

        }

        take_us = qemu_clock_get_us(QEMU_CLOCK_REALTIME) - take_us;
        cioc_wait_total_us += take_us;
    }

    buf = iov->iov_base;
    size = iov->iov_len;

    if(size > wq->ori_usage - wq->ori_offset) {
        size = wq->ori_usage - wq->ori_offset;
    }

    memcpy(buf, wq->ori_buf + wq->ori_offset, size);
    wq->ori_offset += size;

    if(wq->ori_offset >= wq->ori_usage) {
        wq_append_safe(info, &info->trans, wq);
        if(atomic_read(&info->target.state) == TARGET_IO_IDLE) {
            qemu_sem_post(&info->target.notify);
        }
        info->current = NULL;
    }

    return size;
}

static ssize_t qio_channel_compre_writev(QIOChannel *ioc,
                                         const struct iovec *iov,
                                         size_t niov,
                                         int *fds,
                                         size_t nfds,
                                         Error **errp)
{
    QIOChannelCompress *cioc = QIO_CHANNEL_COMPRESS(ioc);
    QIOChannelCompressInfo *info = cioc->info;
    WorkQueue *wq = info->current;
    TargetIO *io = &info->target;
    size_t i, towrite = 0;
    int64_t take_us;

    if(!wq) {
        take_us = qemu_clock_get_us(QEMU_CLOCK_REALTIME);

        while(WQ_EMPTY(&info->ready)) {
            if(io->state == TARGET_IO_ERROR) {
                *errp = io->errp;
                return -1;
            }
            if(!WQ_EMPTY(&info->error)) return -1;
            //warn_report("No enough queue, wait");
            atomic_set(&info->waiting, true);
            qemu_sem_wait(&info->notify);
            atomic_set(&info->waiting, false);
        }

        take_us = qemu_clock_get_us(QEMU_CLOCK_REALTIME) - take_us;
        cioc_wait_total_us += take_us;

        wq = wq_first(info, &info->ready);
        wq_remove_safe(info, &info->ready, wq);
        info->current = wq;
        wq->seg_id = info->seg_counter;
        wq->ori_usage = 0;
        wq->ori_offset = 0;
        info->seg_counter++;
    }

    // Merge io vector to linear buffer for compress
    for (i = 0; i < niov; i++) {
        memcpy(wq->ori_buf + wq->ori_usage,
               iov[i].iov_base,
               iov[i].iov_len);
        wq->ori_usage += iov[i].iov_len;
        wq->ori_offset += iov[i].iov_len;
        towrite += iov[i].iov_len;
        if(wq->ori_usage >= wq->ori_size) {
            error_setg(errp, "channel-compress: uncompress buffer full");
            return -1;
        }
    }

    // Buffer will extend to threshold, do compress and deliver to target channal.
    // Or flush buffer when work finish.
    if (wq->ori_usage >= qio_channel_compress_threshold ||
        (niov == 0 && wq->ori_usage > 0) )
    {
        if(niov == 0) {
            atomic_set(&info->waiting, true);
        }

        wq_append_safe(info, &info->comed, wq);
        g_thread_pool_push(info->pool, wq, NULL);
        info->current = NULL;

        if(niov == 0) {
            take_us = qemu_clock_get_us(QEMU_CLOCK_REALTIME);

            while(atomic_read(&io->state) != TARGET_IO_ERROR) {
                if(WQ_EMPTY(&info->comed) && WQ_EMPTY(&info->trans)) {
                    break;
                }
                qemu_sem_wait(&info->notify);
            }
            if(atomic_read(&io->state) != TARGET_IO_ERROR) {
                atomic_set(&io->state, TARGET_IO_FINISH);
                qemu_sem_post(&io->notify);
                g_usleep(1000);
                qemu_thread_join(&info->target.thread);
                take_us = qemu_clock_get_us(QEMU_CLOCK_REALTIME) - take_us;
                cioc_wait_total_us += take_us;
            }
            if(atomic_read(&io->state) == TARGET_IO_ERROR) {
                *errp = io->errp;
                return -1;
            }
        }
    }

    return towrite;
}

bool qio_channel_compress_write_flush(QIOChannelCompress *cioc)
{
    Error *err = NULL;
    ssize_t ret;

    if(!cioc->info->current) return true;

    //info_report("channel-compress: flush segment %lu\n", cioc->info->seg_counter-1);

    ret = qio_channel_compre_writev(QIO_CHANNEL(cioc), 0, 0, 0, 0, &err);
    if(ret < 0) {
        if(err) {
            error_reportf_err(err, ", qio_channel_compress_write_flush fail");
        }
        return false;
    }

    //info_report("channel-compress: flush segment done\n");

    return true;
}

static int qio_channel_compress_set_blocking(QIOChannel *ioc G_GNUC_UNUSED,
                                             bool enabled G_GNUC_UNUSED,
                                             Error **errp G_GNUC_UNUSED)
{
    return 0;
}

static off_t qio_channel_compress_seek(QIOChannel *ioc,
                                       off_t offset,
                                       int whence,
                                       Error **errp)
{
    return 0;
}

static bool target_occurred_error(TargetIO *io) {
    return (io->state == TARGET_IO_ERROR);
}

static void target_get_buffer(TargetIO *io,
                              uint8_t *buf,
                              size_t size)
{
    ssize_t ret, done = 0;

    while (done < size) {
        size_t remain = size - (size_t)done;
        if(remain > (512<<10)) remain = 512<<10;
        ret = qio_channel_read(io->ioc, (char *)buf, remain, &io->errp);

        if (ret <= 0) {
            if (ret == QIO_CHANNEL_ERR_BLOCK) {
                if (qemu_in_coroutine()) {
                    qio_channel_yield(io->ioc, G_IO_IN);
                } else {
                    qio_channel_wait(io->ioc, G_IO_IN);
                }
                continue;
            } else {
                io->state = TARGET_IO_ERROR;
                return ;
            }
        }

        buf += ret;
        done += ret;
    }

}

static void target_put_buffer(TargetIO *io,
                              uint8_t *buf,
                              size_t size)
{
    ssize_t ret, done = 0;

    while (done < size) {
        size_t remain = size - (size_t)done;
        if(remain > (512<<10)) remain = 512<<10;
        ret = qio_channel_write(io->ioc, (char *)buf, remain, &io->errp);

        if (ret <= 0) {
            if (ret == QIO_CHANNEL_ERR_BLOCK) {
                if (qemu_in_coroutine()) {
                    qio_channel_yield(io->ioc, G_IO_OUT);
                } else {
                    qio_channel_wait(io->ioc, G_IO_OUT);
                }
                continue;
            } else {
                io->state = TARGET_IO_ERROR;
                return ;
            }
        }

        buf += ret;
        done += ret;
    }

}

static void target_get_uint64(TargetIO *io,
                              uint64_t *ret)
{
    uint8_t buf[8]={0};
    int i;

    target_get_buffer(io, buf, 8);
    if(target_occurred_error(io)) return ;

    *ret = 0;
    for(i=0 ; i<8 ; i++) {
        *ret += ( (uint64_t)(buf[i]) << i*8 );
    }

}

static void target_put_uint64(TargetIO *io,
                              uint64_t val)
{
    uint8_t buf[8]={0};
    int i;

    for(i=0 ; i<8 ; i++) {
        buf[i] = (uint8_t)(val >> i*8) ;
    }

    target_put_buffer(io, buf, 8);

}

static inline void wq_append_safe(QIOChannelCompressInfo *info,
                                  WqHead *head,
                                  WorkQueue *queue) {
    qemu_mutex_lock(&info->lock_wq);

    QTAILQ_INSERT_TAIL(head, queue, entry);

    qemu_mutex_unlock(&info->lock_wq);
}

static inline void wq_remove_safe(QIOChannelCompressInfo *info,
                                  WqHead *head,
                                  WorkQueue *queue) {
    qemu_mutex_lock(&info->lock_wq);

    QTAILQ_REMOVE(head, queue, entry);

    qemu_mutex_unlock(&info->lock_wq);
}

static inline void wq_change_safe(QIOChannelCompressInfo *info,
                                  WqHead *old,
                                  WqHead *new,
                                  WorkQueue *queue) {
    qemu_mutex_lock(&info->lock_wq);

    QTAILQ_REMOVE(old, queue, entry);
    QTAILQ_INSERT_TAIL(new, queue, entry);

    qemu_mutex_unlock(&info->lock_wq);
}

static void* target_input_thread_func(void *opaque) {
    QIOChannelCompressInfo *info = (QIOChannelCompressInfo *) opaque;
    TargetIO *io = &info->target;
    WorkQueue *wq;

    io->state = TARGET_IO_IDLE;

    qio_channel_set_blocking(io->ioc, true, 0);

    while(io->state==TARGET_IO_ACTIVE || io->state==TARGET_IO_IDLE) {

        if(WQ_EMPTY(&info->trans)) {
            atomic_set(&io->state, TARGET_IO_IDLE);
            //warn_report("No enough queue, wait");
            qemu_sem_wait(&io->notify);
            continue;
        }

        atomic_set(&io->state, TARGET_IO_ACTIVE);

        while(!WQ_EMPTY(&info->trans)) {
            wq = wq_first(info, &info->trans);
            //
            target_get_uint64(io, &wq->cmped_usage);
            if(target_occurred_error(io)) break;
            if(wq->cmped_usage == 0) {
                io->state = TARGET_IO_FINISH;
                //info_report("channel-compress: I/O input complete");
                break;
            }

            target_get_buffer(io, wq->cmped_buf, wq->cmped_usage);
            if(target_occurred_error(io)) break;
            //
            //to decompress thread pool
            wq->seg_id = io->seg_counter;
            wq->ori_offset = 0;
            wq_change_safe(info, &info->trans, &info->comed, wq);
            g_thread_pool_push(info->pool, wq, NULL);
            //info_report("channel-compress: receive seg %lu", io->seg_counter);
            io->seg_counter ++ ;
        }

    }

    if(target_occurred_error(io)) {
        if(info->waiting) {
            if(info->cort) {
                qemu_bh_schedule(info->bh);
            } else {
                qemu_sem_post(&info->notify);
            }
        }
        return NULL;
    }

    qio_channel_set_blocking(io->ioc, false, 0);

    //info_report("channel-compress: I/O input thread terminated");
    return NULL;
}

static void* target_output_thread_func(void *opaque) {
    QIOChannelCompressInfo *info = (QIOChannelCompressInfo *) opaque;
    TargetIO *io = &info->target;
    WorkQueue *cur = 0;

    io->state = TARGET_IO_IDLE;

    while(atomic_read(&io->state)==TARGET_IO_IDLE) {

        atomic_set(&io->state, TARGET_IO_ACTIVE);

        WQ_LOOP(cur, &info->trans, entry) {
            if(io->seg_counter == cur->seg_id) {
                target_put_uint64(io, cur->cmped_usage);
                if(target_occurred_error(io)) break;
                target_put_buffer(io, cur->cmped_buf, cur->cmped_usage);
                if(target_occurred_error(io)) break;

                cioc_ori_total_size += cur->ori_usage;
                cioc_cmp_total_size += cur->cmped_usage;

                wq_change_safe(info, &info->trans, &info->ready, cur);
                if(atomic_read(&info->waiting)) qemu_sem_post(&info->notify);

                //info_report("channel-compress: send seg %lu", io->seg_counter);

                io->seg_counter ++;

                cur = wq_first(info, &info->trans);
            } else {
                cur = wq_next(info, cur);
            }
        }

        if(target_occurred_error(io)) break;

        if( !WQ_EMPTY(&info->trans) && WQ_EMPTY(&info->comed) ) {
            atomic_set(&io->state, TARGET_IO_IDLE);
            continue;
        }

        atomic_set(&io->state, TARGET_IO_IDLE);
        if(atomic_read(&info->waiting)) qemu_sem_post(&info->notify);
        qemu_sem_wait(&io->notify);
    }

    if(io->state == TARGET_IO_FINISH) {
        target_put_uint64(io, 0);
        //info_report("channel-compress: I/O output complete");
    }
    if(atomic_read(&info->waiting)) qemu_sem_post(&info->notify);

    //info_report("channel-compress: I/O output thread terminated");
    return NULL;
}

static void* mem_fail_ptr(size_t size) {
    error_report("Glib Memory allocation fail, size: %lu", size);
    return NULL;
}

static void qio_channel_compress_bh_func(void *opaque) {
    QIOChannelCompressInfo *info = opaque;
    if(!info->cort) return ;
    qemu_coroutine_enter(info->cort);
}

QIOChannelCompress* qio_channel_compress_new(QIOChannel *target, int opm)
{
    QIOChannel *ioc;
    QIOChannelCompress *cioc;
    QIOChannelCompressInfo *info;
    WorkQueue *wq;
    int i;

    if(!target) return NULL;

    if(opm <= 0 || opm >= QIOC_COMP_MAX) return NULL;

    cioc = QIO_CHANNEL_COMPRESS(object_new(TYPE_QIO_CHANNEL_COMPRESS));

    cioc->target = target;
    cioc->info = g_try_new0(QIOChannelCompressInfo, 1);
    if(!cioc->info) {
        return mem_fail_ptr(sizeof(QIOChannelCompressInfo));
    }

    // Need user customizable Option
    cioc->info->thread_num = qio_channel_compress_thread_num;

    cioc_ori_total_size = 0;
    cioc_cmp_total_size = 0;
    cioc_wait_total_us = 0;

    info = cioc->info;
    info->cioc = cioc;
    info->mode = opm;
    info->target.ioc = QIO_CHANNEL(target);
    info->waiting = false;

    QTAILQ_INIT(&info->ready);
    QTAILQ_INIT(&info->trans);
    QTAILQ_INIT(&info->comed);
    QTAILQ_INIT(&info->error);

    qemu_mutex_init(&info->lock_wq);
    qemu_sem_init(&info->notify, 0);
    qemu_sem_init(&info->target.notify, 0);

    info->pool = g_thread_pool_new(work_queue_thread_func,
                                   info,
                                   info->thread_num,
                                   TRUE,
                                   NULL);

    if( g_thread_pool_get_num_threads(info->pool) != info->thread_num ) {
        error_report("Glib thread pool create fail");
        return NULL;
    }

    g_thread_pool_set_max_idle_time(0);

    switch(info->mode) {
        case QIOC_ZSTD_COMPRESSION:
            qemu_thread_create(&info->target.thread,
                               "compress_target_output",
                               target_output_thread_func,
                               info,
                               QEMU_THREAD_JOINABLE);
            break;
        case QIOC_ZSTD_UNCOMPRESSION:
            qemu_thread_create(&info->target.thread,
                               "decompress_target_input",
                               target_input_thread_func,
                               info,
                               QEMU_THREAD_JOINABLE);
            break;
    }

    for(i=0 ; i<qio_channel_compress_wq_num ; i++) {
        wq = g_try_malloc0(sizeof(WorkQueue));
        if(!wq) return mem_fail_ptr(sizeof(WorkQueue));

        switch(opm) {
            case QIOC_ZSTD_COMPRESSION:
                wq->cctx = ZSTD_createCCtx();
                if(!wq->cctx) {
                    error_report("Compress: ZSTD_createCCtx fail");
                    return NULL;
                }
                QTAILQ_INSERT_TAIL(&info->ready, wq, entry);
                break;
            case QIOC_ZSTD_UNCOMPRESSION:
                wq->dctx = ZSTD_createDCtx();
                if(!wq->dctx) {
                    error_report("Decompress: ZSTD_createDCtx fail");
                    return NULL;
                }
                QTAILQ_INSERT_TAIL(&info->trans, wq, entry);
        }

        wq->ori_size = qio_channel_compress_buf_size;
        wq->ori_buf = g_try_malloc0(wq->ori_size);
        if(!wq->ori_buf) return mem_fail_ptr(wq->ori_size);

        wq->cmped_size = ZSTD_compressBound(wq->ori_size);
        wq->cmped_buf = g_try_malloc0(wq->cmped_size);
        if(!wq->cmped_buf) return mem_fail_ptr(wq->cmped_size);
    }

    if(qemu_in_coroutine()) {
        info->cort = qemu_coroutine_self();
        info->bh = qemu_bh_new(qio_channel_compress_bh_func, info);
    }

    ioc = QIO_CHANNEL(cioc);
    qio_channel_set_feature(ioc, QIO_CHANNEL_FEATURE_SHUTDOWN);

    return cioc;
}


static int qio_channel_compress_close(QIOChannel *ioc,
                                      Error **errp)
{
    QIOChannelCompress *cioc = QIO_CHANNEL_COMPRESS(ioc);
    QIOChannelCompressInfo *info = cioc->info;
    WorkQueue *cur, *nxt;

    if(!info) return 0;

    printf("\n");
    printf("%sUnCompress Size:%s %lu B\n",
           "\e[96m",
           "\e[0m",
           cioc_ori_total_size);
    printf("%sCompressed Size:%s %lu B\n",
           "\e[96m",
           "\e[0m",
           cioc_cmp_total_size);
    printf("%sWaiting time:%s %ld us\n\n",
           "\e[96m",
           "\e[0m",
           cioc_wait_total_us);

    g_thread_pool_free(info->pool, TRUE, TRUE);

    if(QTAILQ_FIRST(&info->comed)) {
        QTAILQ_FOREACH_SAFE(cur, &info->comed, entry, nxt) {
            QTAILQ_REMOVE(&info->comed, cur, entry);
            QTAILQ_INSERT_TAIL(&info->ready, cur, entry);
        }
    }

    if(QTAILQ_FIRST(&info->trans)) {
        QTAILQ_FOREACH_SAFE(cur, &info->trans, entry, nxt) {
            QTAILQ_REMOVE(&info->trans, cur, entry);
            QTAILQ_INSERT_TAIL(&info->ready, cur, entry);
        }
    }

    if(QTAILQ_FIRST(&info->error)) {
        QTAILQ_FOREACH_SAFE(cur, &info->error, entry, nxt) {
            QTAILQ_REMOVE(&info->error, cur, entry);
            QTAILQ_INSERT_TAIL(&info->ready, cur, entry);
        }
    }

    if(QTAILQ_FIRST(&info->ready)) {
        QTAILQ_FOREACH_SAFE(cur, &info->ready, entry, nxt) {
            if(cur->errp) error_report_err(cur->errp);
            QTAILQ_REMOVE(&info->ready, cur, entry);
            if(cur->ori_buf) g_free(cur->ori_buf);
            if(cur->cmped_buf) g_free(cur->cmped_buf);
            if(cur->cctx) ZSTD_freeCCtx(cur->cctx);
            if(cur->dctx) ZSTD_freeDCtx(cur->dctx);
            g_free(cur);
        }
    }

    if(info->mode == QIOC_ZSTD_UNCOMPRESSION) {
        if(info->target.state != TARGET_IO_FINISH &&
           info->target.state != TARGET_IO_ERROR)
        {
            info->target.state = TARGET_IO_ERROR;
            qio_channel_shutdown(info->target.ioc, 1, 0);
            qemu_sem_post(&info->target.notify);
        }
        qemu_thread_join(&info->target.thread);
    }

    qemu_sem_destroy(&info->target.notify);
    qemu_sem_destroy(&info->notify);
    qemu_mutex_destroy(&info->lock_wq);

    if(qemu_in_coroutine() && info->bh) {
        qemu_bh_delete(info->bh);
    }

    g_free(cioc->info);
    cioc->info = NULL;

    return 0;
}

static int
qio_channel_compress_shutdown(QIOChannel *ioc,
                              QIOChannelShutdown how,
                              Error **errp)
{
    QIOChannelCompress *cioc = QIO_CHANNEL_COMPRESS(ioc);
    TargetIO *io = &cioc->info->target;
    int ret = 0;

    if (qio_channel_has_feature(io->ioc,
                                QIO_CHANNEL_FEATURE_SHUTDOWN)) {
        if (qio_channel_shutdown(io->ioc, how, errp) < 0) {
            ret  = -EIO;
        }
    }

    if(atomic_read(&io->state) == TARGET_IO_IDLE) {
        if(!io->errp) error_setg(&io->errp, "Cancel by monitor");
        atomic_set(&io->state, TARGET_IO_ERROR);
        qemu_sem_post(&io->notify);
    }

    return ret;
}

static void qio_channel_compress_finalize(Object *obj)
{
    QIOChannel *ioc = QIO_CHANNEL(obj);

    qio_channel_compress_close(ioc, NULL);

}

void qio_channel_compress_parameters(bool set,
                                     int *thread,
                                     int *queue,
                                     int *size)
{
    if(set) {
        qio_channel_compress_thread_num = *thread;
        qio_channel_compress_wq_num = *queue;
        qio_channel_compress_buf_size = *size;
        return ;
    }
    *thread = qio_channel_compress_thread_num;
    *queue = qio_channel_compress_wq_num;
    *size = qio_channel_compress_buf_size;
}

void hmp_migrate_channel_compress(Monitor *mon, const QDict *qdict) {
    bool enable = qdict_get_try_bool(qdict, "enable", false);
    bool disable = qdict_get_try_bool(qdict, "disable", false);
    bool set = qdict_get_try_bool(qdict, "set", false);
    int thread = qdict_get_try_int(qdict, "thread", 8);
    int queue = qdict_get_try_int(qdict, "queue", 16);
    int size = qdict_get_try_int(qdict, "size", 4);
    int level = qdict_get_try_int(qdict, "level", 3);

    monitor_printf(mon, "thread:%d, queue:%d, size:%d, level:%d\n",
                   thread, queue, size, level);

    if(thread < 1 || queue < thread || size < 1 || size > 8 ||
       level < 1 || level > 19)
    {
        monitor_printf(mon, "Migrate Channel ZSTD Compress parameters "
                            "is incorrect.\n"
                            "Must: thread >= 1, queue >= thread, "
                            "1 <= size <= 8, 1 <= level <= 19\n");
        return ;
    }

    if(enable) {
        qio_channel_compress_enable = true;
    }

    if(disable) {
        qio_channel_compress_enable = false;
    }

    if(set) {
        qio_channel_compress_thread_num = thread;
        qio_channel_compress_wq_num = queue;
        qio_channel_compress_threshold = size << 20;
        qio_channel_compress_buf_size = qio_channel_compress_threshold + 
                                        (2 << 20);
        qio_channel_compress_level = level;
    }

    const char *status = qio_channel_compress_enable ? "enable" : "disable";
    monitor_printf(mon, "Migrate Channel ZSTD Compress is %s.\n", status);
}

typedef struct QIOChannelCompressSource QIOChannelCompressSource;
struct QIOChannelCompressSource {
    GSource parent;
    QIOChannelCompress *bioc;
    GIOCondition condition;
};

static gboolean
qio_channel_ft_source_prepare(GSource *source,
                              gint *timeout)
{
    QIOChannelCompressSource *bsource = (QIOChannelCompressSource *)source;

    *timeout = -1;

    return (G_IO_IN | G_IO_OUT) & bsource->condition;
}

static gboolean
qio_channel_ft_source_check(GSource *source)
{
    QIOChannelCompressSource *bsource = (QIOChannelCompressSource *)source;

    return (G_IO_IN | G_IO_OUT) & bsource->condition;
}

static gboolean
qio_channel_ft_source_dispatch(GSource *source,
                               GSourceFunc callback,
                               gpointer user_data)
{
    QIOChannelFunc func = (QIOChannelFunc)callback;
    QIOChannelCompressSource *bsource = (QIOChannelCompressSource *)source;

    return (*func)(QIO_CHANNEL(bsource->bioc),
                   ((G_IO_IN | G_IO_OUT) & bsource->condition),
                   user_data);
}

static void
qio_channel_ft_source_finalize(GSource *source)
{
    QIOChannelCompressSource *ssource = (QIOChannelCompressSource *)source;

    object_unref(OBJECT(ssource->bioc));
}

GSourceFuncs qio_channel_compress_source_funcs = {
    qio_channel_ft_source_prepare,
    qio_channel_ft_source_check,
    qio_channel_ft_source_dispatch,
    qio_channel_ft_source_finalize
};

static GSource *qio_channel_compre_create_watch(QIOChannel *ioc,
                                                GIOCondition condition)
{
    QIOChannelCompress *bioc = QIO_CHANNEL_COMPRESS(ioc);
    QIOChannelCompressSource *ssource;
    GSource *source;

    source = g_source_new(&qio_channel_compress_source_funcs,
                          sizeof(QIOChannelCompressSource));
    ssource = (QIOChannelCompressSource *)source;

    ssource->bioc = bioc;
    object_ref(OBJECT(bioc));

    ssource->condition = condition;

    return source;
}


static void qio_channel_compress_class_init(ObjectClass *klass,
                                            void *class_data G_GNUC_UNUSED)
{
    QIOChannelClass *ioc_klass = QIO_CHANNEL_CLASS(klass);

    ioc_klass->io_writev = qio_channel_compre_writev;
    ioc_klass->io_readv = qio_channel_compre_readv;
    ioc_klass->io_set_blocking = qio_channel_compress_set_blocking;
    ioc_klass->io_seek = qio_channel_compress_seek;
    ioc_klass->io_close = qio_channel_compress_close;
    ioc_klass->io_shutdown = qio_channel_compress_shutdown;
    ioc_klass->io_create_watch = qio_channel_compre_create_watch;
}

static const TypeInfo qio_channel_compress_info = {
    .parent = TYPE_QIO_CHANNEL,
    .name = TYPE_QIO_CHANNEL_COMPRESS,
    .instance_size = sizeof(QIOChannelCompress),
    .instance_finalize = qio_channel_compress_finalize,
    .class_init = qio_channel_compress_class_init,
};

static void qio_channel_compress_register_types(void)
{
    type_register_static(&qio_channel_compress_info);
}

type_init(qio_channel_compress_register_types);
