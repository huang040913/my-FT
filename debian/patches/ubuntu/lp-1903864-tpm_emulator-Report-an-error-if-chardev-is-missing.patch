From 88f830745721ba8c9e9d2831c01045a6f130c1a6 Mon Sep 17 00:00:00 2001
From: Stefan Berger <stefanb@linux.vnet.ibm.com>
Date: Fri, 24 Jul 2020 08:57:26 -0400
Subject: [PATCH] tpm_emulator: Report an error if chardev is missing
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

This patch fixes the odd error reporting when trying to send a file
descriptor to the TPM emulator if one has not passed a valid chardev.

$ x86_64-softmmu/qemu-system-x86_64 -tpmdev emulator,id=tpm0
qemu-system-x86_64: -tpmdev emulator,id=tpm0: tpm-emulator: Failed to send CMD_SET_DATAFD: Success
qemu-system-x86_64: -tpmdev emulator,id=tpm0: tpm-emulator: Could not cleanly shutdown the TPM: Success

This is the new error report:

$ x86_64-softmmu/qemu-system-x86_64 -tpmdev emulator,id=tpm0
qemu-system-x86_64: -tpmdev emulator,id=tpm0: tpm-emulator: parameter 'chardev' is missing

This change does not hide the display of supported TPM types if a non-existent type is passed:

$ x86_64-softmmu/qemu-system-x86_64 -tpmdev nonexistent,id=tpm0
qemu-system-x86_64: -tpmdev nonexistent,id=tpm0: Parameter 'type' expects a TPM backend type
Supported TPM types (choose only one):
 passthrough   Passthrough TPM backend driver
    emulator   TPM emulator backend driver

Signed-off-by: Stefan Berger <stefanb@linux.ibm.com>
Reviewed-by: Marc-André Lureau <marcandre.lureau@redhat.com>
Reviewed-by: Markus Armbruster <armbru@redhat.com>

Backport-Notes: path to tpm_emulator.c file was different
Origin: backport, https://git.qemu.org/?p=qemu.git;a=commit;h=88f830745721ba8c9e9d2831c01045a6f130c1a6
Bug-Ubuntu: https://bugs.launchpad.net/bugs/1903864
Last-Update: 2021-01-28

---
 backends/tpm/tpm_emulator.c | 38 ++++++++++++++++++++++---------------
 1 file changed, 23 insertions(+), 15 deletions(-)

diff --git a/backends/tpm/tpm_emulator.c b/backends/tpm/tpm_emulator.c
index 9605339f93..a9b0f55e67 100644
--- a/hw/tpm/tpm_emulator.c
+++ b/hw/tpm/tpm_emulator.c
@@ -549,27 +549,30 @@ err_exit:
 static int tpm_emulator_handle_device_opts(TPMEmulator *tpm_emu, QemuOpts *opts)
 {
     const char *value;
+    Error *err = NULL;
+    Chardev *dev;
 
     value = qemu_opt_get(opts, "chardev");
-    if (value) {
-        Error *err = NULL;
-        Chardev *dev = qemu_chr_find(value);
-
-        if (!dev) {
-            error_report("tpm-emulator: tpm chardev '%s' not found.", value);
-            goto err;
-        }
+    if (!value) {
+        error_report("tpm-emulator: parameter 'chardev' is missing");
+        goto err;
+    }
 
-        if (!qemu_chr_fe_init(&tpm_emu->ctrl_chr, dev, &err)) {
-            error_prepend(&err, "tpm-emulator: No valid chardev found at '%s':",
-                          value);
-            error_report_err(err);
-            goto err;
-        }
+    dev = qemu_chr_find(value);
+    if (!dev) {
+        error_report("tpm-emulator: tpm chardev '%s' not found", value);
+        goto err;
+    }
 
-        tpm_emu->options->chardev = g_strdup(value);
+    if (!qemu_chr_fe_init(&tpm_emu->ctrl_chr, dev, &err)) {
+        error_prepend(&err, "tpm-emulator: No valid chardev found at '%s':",
+                      value);
+        error_report_err(err);
+        goto err;
     }
 
+    tpm_emu->options->chardev = g_strdup(value);
+
     if (tpm_emulator_prepare_data_fd(tpm_emu) < 0) {
         goto err;
     }
@@ -925,6 +928,11 @@ static void tpm_emulator_shutdown(TPMEmulator *tpm_emu)
 {
     ptm_res res;
 
+    if (!tpm_emu->options->chardev) {
+        /* was never properly initialized */
+        return;
+    }
+
     if (tpm_emulator_ctrlcmd(tpm_emu, CMD_SHUTDOWN, &res, 0, sizeof(res)) < 0) {
         error_report("tpm-emulator: Could not cleanly shutdown the TPM: %s",
                      strerror(errno));
-- 
2.30.0

