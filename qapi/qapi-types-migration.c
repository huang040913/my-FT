/* AUTOMATICALLY GENERATED, DO NOT MODIFY */

/*
 * Schema-defined QAPI types
 *
 * Copyright IBM, Corp. 2011
 * Copyright (c) 2013-2018 Red Hat Inc.
 *
 * This work is licensed under the terms of the GNU LGPL, version 2.1 or later.
 * See the COPYING.LIB file in the top-level directory.
 */

#include "qemu/osdep.h"
#include "qapi/dealloc-visitor.h"
#include "qapi-types-migration.h"
#include "qapi-visit-migration.h"

void qapi_free_MigrationStats(MigrationStats *obj)
{
    Visitor *v;

    if (!obj) {
        return;
    }

    v = qapi_dealloc_visitor_new();
    visit_type_MigrationStats(v, NULL, &obj, NULL);
    visit_free(v);
}

void qapi_free_XBZRLECacheStats(XBZRLECacheStats *obj)
{
    Visitor *v;

    if (!obj) {
        return;
    }

    v = qapi_dealloc_visitor_new();
    visit_type_XBZRLECacheStats(v, NULL, &obj, NULL);
    visit_free(v);
}

void qapi_free_CompressionStats(CompressionStats *obj)
{
    Visitor *v;

    if (!obj) {
        return;
    }

    v = qapi_dealloc_visitor_new();
    visit_type_CompressionStats(v, NULL, &obj, NULL);
    visit_free(v);
}

const QEnumLookup MigrationStatus_lookup = {
    .array = (const char *const[]) {
        [MIGRATION_STATUS_NONE] = "none",
        [MIGRATION_STATUS_SETUP] = "setup",
        [MIGRATION_STATUS_CANCELLING] = "cancelling",
        [MIGRATION_STATUS_CANCELLED] = "cancelled",
        [MIGRATION_STATUS_ACTIVE] = "active",
        [MIGRATION_STATUS_POSTCOPY_ACTIVE] = "postcopy-active",
        [MIGRATION_STATUS_POSTCOPY_PAUSED] = "postcopy-paused",
        [MIGRATION_STATUS_POSTCOPY_RECOVER] = "postcopy-recover",
        [MIGRATION_STATUS_COMPLETED] = "completed",
        [MIGRATION_STATUS_FAILED] = "failed",
        [MIGRATION_STATUS_COLO] = "colo",
        [MIGRATION_STATUS_PRE_SWITCHOVER] = "pre-switchover",
        [MIGRATION_STATUS_DEVICE] = "device",
        [MIGRATION_STATUS_WAIT_UNPLUG] = "wait-unplug",
        [MIGRATION_STATUS_FT] = "ft",
    },
    .size = MIGRATION_STATUS__MAX
};

void qapi_free_MigrationInfo(MigrationInfo *obj)
{
    Visitor *v;

    if (!obj) {
        return;
    }

    v = qapi_dealloc_visitor_new();
    visit_type_MigrationInfo(v, NULL, &obj, NULL);
    visit_free(v);
}

const QEnumLookup MigrationCapability_lookup = {
    .array = (const char *const[]) {
        [MIGRATION_CAPABILITY_XBZRLE] = "xbzrle",
        [MIGRATION_CAPABILITY_RDMA_PIN_ALL] = "rdma-pin-all",
        [MIGRATION_CAPABILITY_AUTO_CONVERGE] = "auto-converge",
        [MIGRATION_CAPABILITY_ZERO_BLOCKS] = "zero-blocks",
        [MIGRATION_CAPABILITY_COMPRESS] = "compress",
        [MIGRATION_CAPABILITY_EVENTS] = "events",
        [MIGRATION_CAPABILITY_POSTCOPY_RAM] = "postcopy-ram",
        [MIGRATION_CAPABILITY_X_COLO] = "x-colo",
        [MIGRATION_CAPABILITY_RELEASE_RAM] = "release-ram",
        [MIGRATION_CAPABILITY_BLOCK] = "block",
        [MIGRATION_CAPABILITY_RETURN_PATH] = "return-path",
        [MIGRATION_CAPABILITY_PAUSE_BEFORE_SWITCHOVER] = "pause-before-switchover",
        [MIGRATION_CAPABILITY_MULTIFD] = "multifd",
        [MIGRATION_CAPABILITY_DIRTY_BITMAPS] = "dirty-bitmaps",
        [MIGRATION_CAPABILITY_POSTCOPY_BLOCKTIME] = "postcopy-blocktime",
        [MIGRATION_CAPABILITY_LATE_BLOCK_ACTIVATE] = "late-block-activate",
        [MIGRATION_CAPABILITY_X_IGNORE_SHARED] = "x-ignore-shared",
        [MIGRATION_CAPABILITY_VALIDATE_UUID] = "validate-uuid",
        [MIGRATION_CAPABILITY_FT] = "ft",
    },
    .size = MIGRATION_CAPABILITY__MAX
};

void qapi_free_MigrationCapabilityStatus(MigrationCapabilityStatus *obj)
{
    Visitor *v;

    if (!obj) {
        return;
    }

    v = qapi_dealloc_visitor_new();
    visit_type_MigrationCapabilityStatus(v, NULL, &obj, NULL);
    visit_free(v);
}

void qapi_free_MigrationCapabilityStatusList(MigrationCapabilityStatusList *obj)
{
    Visitor *v;

    if (!obj) {
        return;
    }

    v = qapi_dealloc_visitor_new();
    visit_type_MigrationCapabilityStatusList(v, NULL, &obj, NULL);
    visit_free(v);
}

const QEnumLookup MigrationParameter_lookup = {
    .array = (const char *const[]) {
        [MIGRATION_PARAMETER_ANNOUNCE_INITIAL] = "announce-initial",
        [MIGRATION_PARAMETER_ANNOUNCE_MAX] = "announce-max",
        [MIGRATION_PARAMETER_ANNOUNCE_ROUNDS] = "announce-rounds",
        [MIGRATION_PARAMETER_ANNOUNCE_STEP] = "announce-step",
        [MIGRATION_PARAMETER_COMPRESS_LEVEL] = "compress-level",
        [MIGRATION_PARAMETER_COMPRESS_THREADS] = "compress-threads",
        [MIGRATION_PARAMETER_DECOMPRESS_THREADS] = "decompress-threads",
        [MIGRATION_PARAMETER_COMPRESS_WAIT_THREAD] = "compress-wait-thread",
        [MIGRATION_PARAMETER_CPU_THROTTLE_INITIAL] = "cpu-throttle-initial",
        [MIGRATION_PARAMETER_CPU_THROTTLE_INCREMENT] = "cpu-throttle-increment",
        [MIGRATION_PARAMETER_TLS_CREDS] = "tls-creds",
        [MIGRATION_PARAMETER_TLS_HOSTNAME] = "tls-hostname",
        [MIGRATION_PARAMETER_TLS_AUTHZ] = "tls-authz",
        [MIGRATION_PARAMETER_MAX_BANDWIDTH] = "max-bandwidth",
        [MIGRATION_PARAMETER_DOWNTIME_LIMIT] = "downtime-limit",
        [MIGRATION_PARAMETER_X_CHECKPOINT_DELAY] = "x-checkpoint-delay",
        [MIGRATION_PARAMETER_BLOCK_INCREMENTAL] = "block-incremental",
        [MIGRATION_PARAMETER_MULTIFD_CHANNELS] = "multifd-channels",
        [MIGRATION_PARAMETER_XBZRLE_CACHE_SIZE] = "xbzrle-cache-size",
        [MIGRATION_PARAMETER_MAX_POSTCOPY_BANDWIDTH] = "max-postcopy-bandwidth",
        [MIGRATION_PARAMETER_MAX_CPU_THROTTLE] = "max-cpu-throttle",
        [MIGRATION_PARAMETER_FT_TYPE] = "ft-type",
        [MIGRATION_PARAMETER_FT_CHECKPOINT_DELAY] = "ft-checkpoint-delay",
        [MIGRATION_PARAMETER_FT_SHOW_MSG] = "ft-show-msg",
        [MIGRATION_PARAMETER_FT_AUTO_CD] = "ft-auto-cd",
        [MIGRATION_PARAMETER_FT_WATCHDOG_STAGE_INTERVAL] = "ft-watchdog-stage-interval",
        [MIGRATION_PARAMETER_FT_SHARED_EXPORT] = "ft-shared-export",
        [MIGRATION_PARAMETER_FT_COMPRESS_THREADS] = "ft-compress-threads",
        [MIGRATION_PARAMETER_FT_LOCAL_EXPORT] = "ft-local-export",
        [MIGRATION_PARAMETER_FT_BLOCK_GRANULARITY] = "ft-block-granularity",
    },
    .size = MIGRATION_PARAMETER__MAX
};

void qapi_free_MigrateSetParameters(MigrateSetParameters *obj)
{
    Visitor *v;

    if (!obj) {
        return;
    }

    v = qapi_dealloc_visitor_new();
    visit_type_MigrateSetParameters(v, NULL, &obj, NULL);
    visit_free(v);
}

void qapi_free_MigrationParameters(MigrationParameters *obj)
{
    Visitor *v;

    if (!obj) {
        return;
    }

    v = qapi_dealloc_visitor_new();
    visit_type_MigrationParameters(v, NULL, &obj, NULL);
    visit_free(v);
}

const QEnumLookup COLOMessage_lookup = {
    .array = (const char *const[]) {
        [COLO_MESSAGE_CHECKPOINT_READY] = "checkpoint-ready",
        [COLO_MESSAGE_CHECKPOINT_REQUEST] = "checkpoint-request",
        [COLO_MESSAGE_CHECKPOINT_REPLY] = "checkpoint-reply",
        [COLO_MESSAGE_VMSTATE_SEND] = "vmstate-send",
        [COLO_MESSAGE_VMSTATE_SIZE] = "vmstate-size",
        [COLO_MESSAGE_VMSTATE_RECEIVED] = "vmstate-received",
        [COLO_MESSAGE_VMSTATE_LOADED] = "vmstate-loaded",
    },
    .size = COLO_MESSAGE__MAX
};

const QEnumLookup COLOMode_lookup = {
    .array = (const char *const[]) {
        [COLO_MODE_NONE] = "none",
        [COLO_MODE_PRIMARY] = "primary",
        [COLO_MODE_SECONDARY] = "secondary",
    },
    .size = COLO_MODE__MAX
};

const QEnumLookup FailoverStatus_lookup = {
    .array = (const char *const[]) {
        [FAILOVER_STATUS_NONE] = "none",
        [FAILOVER_STATUS_REQUIRE] = "require",
        [FAILOVER_STATUS_ACTIVE] = "active",
        [FAILOVER_STATUS_COMPLETED] = "completed",
        [FAILOVER_STATUS_RELAUNCH] = "relaunch",
    },
    .size = FAILOVER_STATUS__MAX
};

const QEnumLookup COLOExitReason_lookup = {
    .array = (const char *const[]) {
        [COLO_EXIT_REASON_NONE] = "none",
        [COLO_EXIT_REASON_REQUEST] = "request",
        [COLO_EXIT_REASON_ERROR] = "error",
        [COLO_EXIT_REASON_PROCESSING] = "processing",
    },
    .size = COLO_EXIT_REASON__MAX
};

const QEnumLookup CheckpointingCapability_lookup = {
    .array = (const char *const[]) {
        [CHECKPOINTING_CAPABILITY_TX_BUFFERING] = "tx-buffering",
        [CHECKPOINTING_CAPABILITY_WATCHDOG] = "watchdog",
        [CHECKPOINTING_CAPABILITY_SOCKET_TIMEOUT] = "socket-timeout",
        [CHECKPOINTING_CAPABILITY_SHARED_EXPORT] = "shared-export",
        [CHECKPOINTING_CAPABILITY_AUTOPILOT] = "autopilot",
        [CHECKPOINTING_CAPABILITY_BLOCK_REPLICATE] = "block-replicate",
        [CHECKPOINTING_CAPABILITY_COMPRESSED_MESSAGE] = "compressed-message",
        [CHECKPOINTING_CAPABILITY_LZ4] = "lz4",
        [CHECKPOINTING_CAPABILITY_ZSTD] = "zstd",
        [CHECKPOINTING_CAPABILITY_COMPRESS_POOL] = "compress-pool",
        [CHECKPOINTING_CAPABILITY_SAVE_POOL] = "save-pool",
        [CHECKPOINTING_CAPABILITY_MESSAGE_POOL] = "message-pool",
        [CHECKPOINTING_CAPABILITY_LOCAL_EXPORT] = "local-export",
    },
    .size = CHECKPOINTING_CAPABILITY__MAX
};

const QEnumLookup CheckpointingType_lookup = {
    .array = (const char *const[]) {
        [CHECKPOINTING_TYPE_NONE] = "none",
        [CHECKPOINTING_TYPE_FT_EPOCH_BASE] = "ft-epoch-base",
        [CHECKPOINTING_TYPE_FT_TCP_LOCK_STEPPING] = "ft-tcp-lock-stepping",
        [CHECKPOINTING_TYPE_FT_EPOCH_BLOCK_REPL] = "ft-epoch-block-repl",
        [CHECKPOINTING_TYPE_DR_FULL_STATES] = "dr-full-states",
    },
    .size = CHECKPOINTING_TYPE__MAX
};

const QEnumLookup FTRole_lookup = {
    .array = (const char *const[]) {
        [FT_ROLE_UNKNOWN] = "unknown",
        [FT_ROLE_PRIMARY] = "primary",
        [FT_ROLE_BACKUP] = "backup",
    },
    .size = FT_ROLE__MAX
};

const QEnumLookup FTMessage_lookup = {
    .array = (const char *const[]) {
        [FT_MESSAGE_CHECKPOINT_READY] = "checkpoint-ready",
        [FT_MESSAGE_CHECKPOINT_REQUEST] = "checkpoint-request",
        [FT_MESSAGE_CHECKPOINT_REPLY] = "checkpoint-reply",
        [FT_MESSAGE_VMSTATE_RECEIVED] = "vmstate-received",
        [FT_MESSAGE_VMSTATE_LOADED] = "vmstate-loaded",
        [FT_MESSAGE_NET_RX_READY] = "net-rx-ready",
        [FT_MESSAGE_NET_RX_SIZE] = "net-rx-size",
        [FT_MESSAGE_NET_TX_CHECKSUM_REQUEST] = "net-tx-checksum-request",
        [FT_MESSAGE_NET_TX_CHECKSUM] = "net-tx-checksum",
        [FT_MESSAGE_KEEP_ALIVE] = "keep-alive",
        [FT_MESSAGE_BUFFER_COMPRESSED] = "buffer-compressed",
        [FT_MESSAGE_BUFFER_NORMAL] = "buffer-normal",
        [FT_MESSAGE_BUFFER_SIZE] = "buffer-size",
        [FT_MESSAGE_RAM_FULL_SIZE] = "ram-full-size",
        [FT_MESSAGE_RAM_ITERATE_SIZE] = "ram-iterate-size",
        [FT_MESSAGE_VMSD_FULL_SIZE] = "vmsd-full-size",
        [FT_MESSAGE_BLOCK_FULL_SIZE] = "block-full-size",
        [FT_MESSAGE_BLOCK_ITERATE_SIZE] = "block-iterate-size",
        [FT_MESSAGE_JOBS_DONE] = "jobs-done",
        [FT_MESSAGE_WAIT_FOR_DONE] = "wait-for-done",
        [FT_MESSAGE_NOP] = "nop",
        [FT_MESSAGE_GUEST_POWERED_OFF] = "guest-powered-off",
    },
    .size = FT_MESSAGE__MAX
};

const QEnumLookup FTStatus_lookup = {
    .array = (const char *const[]) {
        [FT_STATUS_UNKNOWN] = "unknown",
        [FT_STATUS_NORMAL] = "normal",
        [FT_STATUS_NET_FRONTEND_NOT_SUPPORTED] = "net-frontend-not-supported",
        [FT_STATUS_NET_BACKEND_NOT_SUPPORTED] = "net-backend-not-supported",
        [FT_STATUS_BLOCK_FRONTEND_NOT_SUPPORTED] = "block-frontend-not-supported",
        [FT_STATUS_NO_UUID] = "no-uuid",
        [FT_STATUS_UUID_IN_MIGRATED_LIST] = "uuid-in-migrated-list",
        [FT_STATUS_DURING_TAKEOVER] = "during-takeover",
        [FT_STATUS_STORAGE_DISCONNECTED] = "storage-disconnected",
        [FT_STATUS_PRIMARY_TAKEOVER] = "primary-takeover",
        [FT_STATUS_BACKUP_TAKEOVER] = "backup-takeover",
        [FT_STATUS_VM_POWERED_OFF] = "vm-powered-off",
        [FT_STATUS_INTERNAL_ERROR] = "internal-error",
        [FT_STATUS_DEVICE_NOT_SUPPORTED] = "device-not-supported",
        [FT_STATUS_VM_CHANGED_DURING_INIT] = "vm-changed-during-init",
    },
    .size = FT_STATUS__MAX
};

const QEnumLookup FTRecord_lookup = {
    .array = (const char *const[]) {
        [FT_RECORD_EPOCH_BEGIN] = "epoch-begin",
        [FT_RECORD_EPOCH_END] = "epoch-end",
        [FT_RECORD_VM_STOP_BEGIN] = "vm-stop-begin",
        [FT_RECORD_VM_STOP_END] = "vm-stop-end",
        [FT_RECORD_VM_START_BEGIN] = "vm-start-begin",
        [FT_RECORD_VM_START_END] = "vm-start-end",
        [FT_RECORD_BLOCK_SAVE_BEGIN] = "block-save-begin",
        [FT_RECORD_BLOCK_SAVE_END] = "block-save-end",
        [FT_RECORD_RAM_SAVE_BEGIN] = "ram-save-begin",
        [FT_RECORD_RAM_SAVE_END] = "ram-save-end",
        [FT_RECORD_VMSD_SAVE_BEGIN] = "vmsd-save-begin",
        [FT_RECORD_VMSD_SAVE_END] = "vmsd-save-end",
        [FT_RECORD_BLOCK_COMPRESS_TIME] = "block-compress-time",
        [FT_RECORD_RAM_COMPRESS_TIME] = "ram-compress-time",
        [FT_RECORD_VMSD_COMPRESS_TIME] = "vmsd-compress-time",
        [FT_RECORD_SEND_BEGIN] = "send-begin",
        [FT_RECORD_SEND_END] = "send-end",
        [FT_RECORD_LOADED_ACK_BEGIN] = "loaded-ack-begin",
        [FT_RECORD_LOADED_ACK_END] = "loaded-ack-end",
        [FT_RECORD_BLOCK_SIZE] = "block-size",
        [FT_RECORD_BLOCK_COMPRESSED_SIZE] = "block-compressed-size",
        [FT_RECORD_RAM_SIZE] = "ram-size",
        [FT_RECORD_RAM_COMPRESSED_SIZE] = "ram-compressed-size",
        [FT_RECORD_VMSD_SIZE] = "vmsd-size",
        [FT_RECORD_VMSD_COMPRESSED_SIZE] = "vmsd-compressed-size",
        [FT_RECORD_TX_QUEUE_SIZE] = "tx-queue-size",
        [FT_RECORD_CHECKPOINT_DELAY] = "checkpoint-delay",
        [FT_RECORD_EXTRA_DELAY] = "extra-delay",
    },
    .size = FT_RECORD__MAX
};

const QEnumLookup FTRecordSendType_lookup = {
    .array = (const char *const[]) {
        [FT_RECORD_SEND_TYPE_RAM] = "ram",
        [FT_RECORD_SEND_TYPE_BLOCK] = "block",
        [FT_RECORD_SEND_TYPE_VMSD] = "vmsd",
    },
    .size = FT_RECORD_SEND_TYPE__MAX
};

const QEnumLookup DRMode_lookup = {
    .array = (const char *const[]) {
        [DR_MODE_NONE] = "none",
        [DR_MODE_CONTINUOUS] = "continuous",
        [DR_MODE_DELAYED] = "delayed",
        [DR_MODE_AUTO] = "auto",
        [DR_MODE_MANUAL] = "manual",
    },
    .size = DR_MODE__MAX
};

const QEnumLookup FTExportLocal_lookup = {
    .array = (const char *const[]) {
        [FT_EXPORT_LOCAL_SESSION_CURRENT] = "session-current",
        [FT_EXPORT_LOCAL_SESSION_INPUT] = "session-input",
        [FT_EXPORT_LOCAL_TAKEOVER] = "takeover",
        [FT_EXPORT_LOCAL_DESTROY] = "destroy",
        [FT_EXPORT_LOCAL_LAST_SYNCED] = "last-synced",
        [FT_EXPORT_LOCAL_STATUS] = "status",
        [FT_EXPORT_LOCAL_ROLE] = "role",
        [FT_EXPORT_LOCAL_RUNSTATE] = "runstate",
        [FT_EXPORT_LOCAL_RPO] = "rpo",
        [FT_EXPORT_LOCAL_RPO_REQUEST] = "rpo-request",
        [FT_EXPORT_LOCAL_CHECKPOINT_NOW] = "checkpoint-now",
        [FT_EXPORT_LOCAL_DR_MODE] = "dr-mode",
        [FT_EXPORT_LOCAL_DR_MODE_REQUEST] = "dr-mode-request",
        [FT_EXPORT_LOCAL_CHECKPOINTING_TYPE] = "checkpointing-type",
    },
    .size = FT_EXPORT_LOCAL__MAX
};

const QEnumLookup FTCompressor_lookup = {
    .array = (const char *const[]) {
        [FT_COMPRESSOR_NONE] = "none",
        [FT_COMPRESSOR_LZ4_FAST] = "lz4_fast",
        [FT_COMPRESSOR_ZSTD_FAST] = "zstd_fast",
    },
    .size = FT_COMPRESSOR__MAX
};

#if defined(CONFIG_REPLICATION)
void qapi_free_ReplicationStatus(ReplicationStatus *obj)
{
    Visitor *v;

    if (!obj) {
        return;
    }

    v = qapi_dealloc_visitor_new();
    visit_type_ReplicationStatus(v, NULL, &obj, NULL);
    visit_free(v);
}
#endif /* defined(CONFIG_REPLICATION) */

void qapi_free_COLOStatus(COLOStatus *obj)
{
    Visitor *v;

    if (!obj) {
        return;
    }

    v = qapi_dealloc_visitor_new();
    visit_type_COLOStatus(v, NULL, &obj, NULL);
    visit_free(v);
}

/* Dummy declaration to prevent empty .o file */
char qapi_dummy_qapi_types_migration_c;
