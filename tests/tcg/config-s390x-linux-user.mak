# Automatically generated by configure - do not modify
TARGET_NAME=s390x
CONFIG_USER_ONLY=y
QEMU=$(BUILD_DIR)/s390x-linux-user/qemu-s390x
CROSS_CC_GUEST_CFLAGS=-m64
DOCKER_IMAGE=debian-s390x-cross
DOCKER_CROSS_CC_GUEST=s390x-linux-gnu-gcc
